# class MyButtonCell < FXFormBaseCell -> empty, where you add your own items
class HousePriceCell < FXFormBaseCell
  def setUp
    super
    rmq(self).apply_style :house_price_cell
    rmq(self.contentView).tap do |q|
      @house_price = q.append(UILabel, :house_price).get
    end
  end

  def update
    super # optional
    # self.separatorStyle = UITableViewCellSeparatorStyleNone

    rmq(self).all.reapply_styles
    @house_price.text = self.field.value
  end
end