# class MyButtonCell < FXFormBaseCell -> empty, where you add your own items
class RatingCell < FXFormTextFieldCell
  attr_accessor :beds, :baths, :sqft
  def setUp
    super
    ratingView = AXRatingView.alloc.init
    ratingView.setStepInterval(0.5)
    ratingView.addTarget(self, action: :changeRate, forControlEvents:UIControlEventValueChanged)
    self.accessoryView = ratingView
    self.accessoryView.sizeToFit
    self.textField.hidden = true
  end

  def update
    super # optional
    # self.separatorStyle = UITableViewCellSeparatorStyleNone
    self.accessoryView.value = self.field.value.to_f
  end

  def changeRate
    self.textField.text = self.accessoryView.value.to_s
    self.field.value = self.accessoryView.value.to_s
  end

  # def didSelectWithTableView(tableView, controller:controller)
  #   # super # optional
  #   # open IntroScreen.new
  #   self.field.action.call
  # end

end