# class MyButtonCell < FXFormBaseCell -> empty, where you add your own items
class UserInfoCell < FXFormBaseCell
  def setUp
    super
    rmq(self).apply_style :user_info_cell
    rmq(self.contentView).tap do |q|
      @user_profile_pic   = q.append(UIImageView, :user_info_cell_profile_pic).get
      @user_verfied       = q.append(UILabel, :user_info_cell_verfied).get
      @user_checkins      = q.append(UILabel, :user_info_cell_checkins).get
      @user_estimates     = q.append(UILabel, :user_info_cell_estimates).get
      @user_accurate      = q.append(UILabel, :user_info_cell_accurate).get
      @user_sold_listings = q.append(UILabel, :user_info_cell_sold_listings).get
    end
  end

  def update
    super # optional
    self.separatorStyle = UITableViewCellSeparatorStyleNone

    rmq(@user_profile_pic).all.reapply_styles
    rmq(@user_verfied).all.reapply_styles
    rmq(@user_checkins).all.reapply_styles
    rmq(@user_estimates).all.reapply_styles
    rmq(@user_accurate).all.reapply_styles
    rmq(@user_sold_listings).all.reapply_styles
    p self.field.value
    p self.field.value.fetch(:profile_pic, nil)
    @user_profile_pic.image = get_photos(self.field.value.fetch(:profile_pic, nil))

    if self.field.value.fetch(:verified, false)
      @user_verfied.text = "Verified User"
    else
      @user_verfied.text = "Unverified User"
    end

    @user_checkins.text = self.field.value.fetch(:estimates_count, 0).to_s + " Check-Ins"
    @user_estimates.text = self.field.value.fetch(:estimates_count, 0).to_s + " Estimates"
    @user_accurate.text = self.field.value.fetch(:accurate, 0.0).to_s + "% Accurate"
    @user_sold_listings.text = self.field.value.fetch(:sold_listings, 0).to_s + " Sold Listings"
  end

  def get_photos(photo)
    if photo.nil?
      rmq.image.resource("icon-512")
    else
      image = rmq.image.resource("icon-512")
      JMImageCache.sharedCache.imageForURL(photo.to_s.to_url , completionBlock: lambda do |downloadedImage|
        image = downloadedImage
      end)
      image
    end
  end
end