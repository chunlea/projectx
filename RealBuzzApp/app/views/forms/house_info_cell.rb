# class MyButtonCell < FXFormBaseCell -> empty, where you add your own items
class HouseInfoCell < FXFormBaseCell
  attr_accessor :beds, :baths, :sqft
  def setUp
    super
    rmq(self).apply_style :house_info_cell
    rmq(self.contentView).tap do |q|
      @house_info_beds  = q.append(UILabel, :house_info_beds).get
      @house_info_baths = q.append(UILabel, :house_info_baths).get
      @house_info_sqft  = q.append(UILabel, :house_info_sqft).get
      q.append(UILabel, :house_info_beds_label)
      q.append(UILabel, :house_info_baths_label)
      q.append(UILabel, :house_info_sqft_label)
    end
  end

  def update
    super # optional
    # self.separatorStyle = UITableViewCellSeparatorStyleNone
    # Overwrite the title specified in the form hash
    # so we can test that the custom cell updated properly
    # self.textField.text = "Cell Updated"
    rmq(@house_info_cell).all.reapply_styles
    # @house_info_beds.text = self.field.formController.beds
    # p self.field.formController.beds
    # @fb_login_button.delegate = self.field.formController.delegate
    @value = self.field.value
    @house_info_beds.text  = @value[:beds].to_s
    @house_info_baths.text = @value[:baths].to_s
    @house_info_sqft.text  = @value[:acres].round(3).to_s
  end

  # def didSelectWithTableView(tableView, controller:controller)
  #   # super # optional
  #   # open IntroScreen.new
  #   self.field.action.call
  # end

end