# class MyButtonCell < FXFormBaseCell -> empty, where you add your own items
class PagedImagesCell < FXFormBaseCell
  def setUp
    super
    rmq(self).apply_style :paged_images_cell
    rmq(self.contentView).tap do |q|
      @scorll_view = q.append(UIScrollView, :paged_images_cell_scroll_view).get
      @page_control = q.append(UIPageControl, :paged_images_cell_page_control).get
    end
  end

  def update
    super # optional
    rmq(self).all.reapply_styles
    # self.separatorStyle = UITableViewCellSeparatorStyleNone
    self.field.value = [] unless self.field.value.is_a?(Array)
    page_size = self.field.value.first(10).size
    @page_control.numberOfPages = page_size
    @scorll_view.delegate       = self
    @scorll_view.contentSize    = CGSizeMake(@scorll_view.frame.size.width * page_size, @scorll_view.frame.size.height)
    self.field.value.first(10).each_with_index do |image, index|
      if image
        image_view             = UIImageView.alloc.initWithImage(image)
        image_view.contentMode = UIViewContentModeScaleAspectFill
        frame                  = @scorll_view.frame
        frame.origin.x         = frame.size.width * index
        frame.origin.y         = 0
        image_view.frame       = frame
        @scorll_view.addSubview(image_view)
      end
    end
  end

  def scrollViewDidScroll(scrollView)
    pageWidth = scrollView.frame.size.width
    page = ((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
    @page_control.currentPage = page
  end

  # def didSelectWithTableView(tableView, controller:controller)
  #   # super # optional
  #   # open IntroScreen.new
  #   self.field.action.call
  # end

end