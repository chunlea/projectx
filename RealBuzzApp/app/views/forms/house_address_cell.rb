# class MyButtonCell < FXFormBaseCell -> empty, where you add your own items
class HouseAddressCell < FXFormBaseCell
  def setUp
    super
    rmq(self).apply_style :house_address_cell
    rmq(self.contentView).tap do |q|
      @house_address = q.append(UILabel, :house_address).get
    end
  end

  def update
    super # optional
    self.separatorStyle = UITableViewCellSeparatorStyleNone

    rmq(@house_address).all.reapply_styles
    @house_address.text = self.field.value
  end
end