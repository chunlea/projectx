class Profile < CDQManagedObject
  def initialize(params = {})
  end

  def update(params = {})
    self
  end

  def delete
  end

  def save
    true
  end

  class << self
    def create(params = {})
      Profile.new(params).tap do |profile|
        profile.save
      end
    end

    def find(id_or_params)
    end

    def all
      profile.find(:all)
    end
  end
end
