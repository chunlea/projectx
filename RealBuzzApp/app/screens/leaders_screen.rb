class LeadersScreen < PM::TableScreen
  title "Leaderboard"
  stylesheet PreviousScreenStylesheet
  status_bar :light
  # searchable placeholder: "Search your previous estimate"

  def on_init
    init_tab_bar

    @segmented_toolbar = UISegmentedControl.alloc.initWithItems(["By Accuracy", "By Estimates Amounts"])
    @segmented_toolbar.selectedSegmentIndex = 0
    set_toolbar_items [{
        system_item: :flexible_space
      }, {
        custom_view: @segmented_toolbar
      }, {
        system_item: :flexible_space
      }]
  end
  def on_load
    init_nav
    self.edgesForExtendedLayout = UIRectEdgeBottom
    table_view.contentInset     = UIEdgeInsetsMake(0, 0.0, 48, 0.0)
    table_view.addPullToRefreshWithActionHandler -> { reload_data }
    table_view.addInfiniteScrollingWithActionHandler -> { load_more }
    @page = 1
    @data = []
    @url = "estimates/leaders"

    reload_data

    find(@segmented_toolbar).on(:value_changed) do |sender|
      get_items(sender.selectedSegmentIndex)
      reload_data
    end

  end
  def get_items(type_id)
    case type_id
    when 0
      @url = "estimates/leaders"
    else
      @url = "estimates/leaders_by_amounts"
    end
  end

  def will_appear
    self.navigationController.setToolbarHidden(false, animated:false)
  end
  def will_disappear
    self.navigationController.setToolbarHidden(true, animated:false)
  end

  def table_data
    [{
      cells: @data.map do |data|
        {
          title: "#{data[:name].empty?? data[:email]:data[:name]}",
          subtitle: "#{data[:estimates_count]} Estimates,  #{(data[:accurate]*100).round(2)}% Accuracy",
          # search_text: "",
          action: :open_profile_screen,
          arguments: { user: data },
          height: 50,
          # remote_image: {  # remote image, requires JMImageCache CocoaPod
          #   url: listing[:photos].first.to_s,
          #   placeholder: "icon-512", # NOTE: this is required!
          #   size: 50,
          #   radius: 0,
          #   content_mode: :scale_aspect_fill
          # }
        }
      end
    }]
  end
  def open_profile_screen(item)
    if item[:user].fetch(:role) == "agent"
      open MyProfileScreen.new(nav_bar: true, user: item[:user]), hide_tab_bar: true
    else
      open ProfileScreen.new(nav_bar: true, user: item[:user]), hide_tab_bar: true
    end
  end
  def load_data
    Api.get @url, {page: @page} do |result, error|
      if result
        p result
        if @page == 1
          @data = result
        else
          @data += result
        end
        update_table_data
        table_view.pullToRefreshView.stopAnimating
        table_view.infiniteScrollingView.stopAnimating
      end
      # if error
      #   App.alert(error)
      # end
    end
  end

  def reload_data
    @page = 1
    load_data
  end

  def load_more
    @page += 1
    load_data
  end

  def get_money(money)
    nf = NSNumberFormatter.alloc.init
    nf.setNumberStyle(NSNumberFormatterCurrencyStyle)
    nf.setMaximumFractionDigits(0)
    nf.stringFromNumber(money)
  end

  def init_tab_bar
    tab_bar_icon_selected   = FAKIonIcons.iosPaperIconWithSize(30)
    tab_bar_icon_unselected = FAKIonIcons.iosPaperOutlineIconWithSize(30)
    tab_bar_icon_selected.addAttribute(NSForegroundColorAttributeName, value:rmq.color.tint)
    tab_bar_icon_unselected.addAttribute(NSForegroundColorAttributeName, value:rmq.color.gray)
    set_tab_bar_item item: { selected: tab_bar_icon_selected.imageWithSize(CGSizeMake(30, 30)), unselected: tab_bar_icon_unselected.imageWithSize(CGSizeMake(30, 30)) }, title: "Leaders"
  end

  def init_nav
    toggleOutlineIcon = FAKIonIcons.iosToggleOutlineIconWithSize(25)
    # naviconRoundIcon = FAKIonIcons.naviconRoundIconWithSize(25)
    set_nav_bar_button :left,  image: toggleOutlineIcon.imageWithSize(CGSizeMake(25, 25)), action: :nav_left_button
    # set_nav_bar_button :right, image: naviconRoundIcon.imageWithSize(CGSizeMake(25, 25)),  action: :nav_right_button
    # self.navigationItem.rightBarButtonItem.enabled = false
  end

  def nav_left_button
    app_delegate.menu.toggle_left
  end

  def nav_right_button
    # open ListScreen.new(region: map.region)
  end

  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
    layout_top_uitoolbar
  end
end
