class UpdateProfileScreen < PM::FormScreen
  title "Update Profile"
  stylesheet WelcomeScreenStylesheet
  status_bar :light

  def on_load
    @profile = {}
    reload_data
  end

  def form_data
    [{
      title: "Profile Information",
      footer: "",
      cells: [{
        name: "bio",
        title: "Bio",
        type: :longtext,
        value: "#{@profile.fetch(:bio, "")}"
      }, {
        name: "college",
        title: "College (optional)",
        type: :longtext,
        value: "#{@profile.fetch(:college, "")}"
      }, {
        name: "degree",
        title: "Degree (optional)",
        type: :longtext,
        value: "#{@profile.fetch(:degree, "")}"
        }, {
        name: "yr_graduated",
        title: "Year Graduated (optional)",
        type: :number,
        value: "#{@profile.fetch(:yr_graduated, "")}"
        }, {
        name: "other_school",
        title: "Other School (optional)",
        type: :longtext,
        value: "#{@profile.fetch(:other_school, "")}"
        }, {
        name: "designations",
        title: "Designations (separate with commas)",
        type: :longtext,
        value: "#{@profile.fetch(:designations, "")}"
        }, {
        name: "linked_in",
        title: "LinkedIn URL",
        type: :longtext,
        value: "#{@profile.fetch(:linked_in, "")}"
        }, {
        name: "website",
        title: "Your Website URL",
        type: :longtext,
        value: "#{@profile.fetch(:website, "")}"
        }, {
        name: "agent_id",
        title: "Your MLS ID",
        type: :text,
        value: "#{@profile.fetch(:agent_id, "")}"
        }, {
        name: "phone",
        title: "Phone Number",
        type: :text,
        value: "#{@profile.fetch(:phone, "")}"
        }, {
        name: "photos",
       class: NSArray,
       sortable: true,
       inline: true,
       template: { type: :image },
       value: [],
          }]
    },{
      title: "",
      # footer: "Some help text",
      cells: [{
        name: :submit,
        title: "Update Profile",
        type: :button,
        "textLabel.color" => rmq.color.tint,
        "textLabel.textAlignment" => UITextAlignmentCenter,
        action: "update_profile"
      }]
    }]
  end

  def scaleImage(image)
    if image.size.width < 1080
      newimg = image
    else
      scaleBy = 1080.0/image.size.width
      size = CGSizeMake(image.size.width * scaleBy, image.size.height * scaleBy)

      UIGraphicsBeginImageContext(size)
      context = UIGraphicsGetCurrentContext()
      transform = CGAffineTransformIdentity

      transform = CGAffineTransformScale(transform, scaleBy, scaleBy)
      CGContextConcatCTM(context, transform)

      image.drawAtPoint(CGPointMake(0, 0))
      newimg = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
    end
    UIImageJPEGRepresentation(newimg, 0.8)
  end

  def reload_data
    @data = {user_id: Auth.id.to_i}
    Api.get "profiles", @data do |result, error|
      if result
        @profile = result
        update_form_data
      end
    end
  end

  def update_profile
    data = render_form
    photos = data[:photos]
    @data = {profile: {user_id: Auth.id, bio: data[:bio], degree: data[:degree], yr_graduated: data[:yr_graduated], college: data[:college], other_school: data[:other_school], designations: data[:designations], linked_in: data[:linked_in], website: data[:website], agent_id: data[:agent_id], phone: data[:phone]}}

    SVProgressHUD.show
    Api.post "/profiles/#{@profile[:id]}", @data do |result, error|
      if result
        p result
        Profile.create(bio:result[:bio], degree:result[:degree], yr_graduated:result[:yr_graduated], college:result[:college], other_school:result[:other_school], designations:result[:designations], linked_in:result[:linked_in], website:result[:website], profile_pic:result[:profile_pic], verified:result[:verified], user_id:result[:id], agent_id:result[:agent_id])
        profile_id = result[:id]
        photos.each do |photo|
          photo_data = scaleImage(photo)
          AFMotion::SessionClient.shared.multipart_post("profiles/#{profile_id}/upload") do |result, form_data|
            if form_data
              if photo_data
                form_data.appendPartWithFileData(photo_data, name: "photo", fileName:"photo.jpg", mimeType: "image/jpeg")
              end
            elsif result.success?
              p result
            else
              p "Upload Error"
            end
          end
        end
        SVProgressHUD.showSuccessWithStatus("")
        app_delegate.open_main_screens
      end
      if error
        SVProgressHUD.showErrorWithStatus("")
        App.alert(error)
      end
    end
  end
  # Remove the following if you're only using portrait

  # You don't have to reapply styles to all UIViews, if you want to optimize, another way to do it
  # is tag the views you need to restyle in your stylesheet, then only reapply the tagged views, like so:
  #   def logo(st)
  #     st.frame = {t: 10, w: 200, h: 96}
  #     st.centered = :horizontal
  #     st.image = image.resource('logo')
  #     st.tag(:reapply_style)
  #   end
  #
  # Then in will_animate_rotate
  #   find(:reapply_style).reapply_styles#
  def will_animate_rotate(orientation, duration)
    reapply_styles
  end
end
