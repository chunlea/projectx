class RequestDetailScreen < PM::FormScreen
  title "Request Estimate"
  stylesheet DetailScreenStylesheet
  status_bar :light
  attr_accessor :listing
  include MotionSocial::Sharing

  def on_load
    init_nav
    reload_data

  end

  def check_in
    open_modal EstimateScreen.new(nav_bar: true, listing: @listing)
  end

  def will_appear
    # self.tableView.setContentInset UIEdgeInsetsMake(-32, 0.0, 0.0, 0.0)
  end
  def will_disappear
    # self.navigationController.setToolbarHidden(true, animated:true)
  end

  def form_data
    [{
      title: "Address",
      footer: "",
      cells: [{
        name: "address",
        title: "",
        type: :longtext,
        placeholder: "The address of this Listing",
        value: @listing[:address]
      }, {
        name: "zip",
        title: "Zipcode",
        type: :text,
        placeholder: @listing[:zip]
      }]
    }, {
      title: "Lisitng Information",
      footer: "",
      cells: [{
        name: "type_of_home",
        title: "Type of home",
        type: :option,
        options: ["Single Family", "Multifamily", "Condo", "Townhome"],
        value: @listing[:type_of_home]
      }]
    }, {
      title: "Listing Information",
      footer: "",
      cells: [{
        name: "square",
        title: "Square footage",
        type: :float,
        value: @listing[:square].to_s
      }, {
        name: "bedrooms",
        title: "Number of bedrooms",
        type: :integer,
        cell: FXFormStepperCell,
        value: @listing[:bedrooms].to_s
      }, {
        name: "bathrooms",
        title: "Number of bathrooms",
        type: :integer,
        cell: FXFormStepperCell,
        value: @listing[:bathrooms].to_s
      }]
    }, {
      title: "",
      footer: "",
      cells: [{
        name: "condition",
        title: "Condition",
        type: :longtext,
        value: @listing[:condition]
      }, {
        name: "notes",
        title: "Notes",
        type: :longtext,
        value: @listing[:notes]
      }]
    }]
  end
  def get_photos(photos)
    if photos.empty?
      [rmq.image.resource("icon-512")]
    else
      photos.map { |photo|
        image = rmq.image.resource("icon-512")
        JMImageCache.sharedCache.imageForURL(photo.to_s.to_url , completionBlock: lambda do |downloadedImage|
          image = downloadedImage
        end)
        image
      }
    end
  end

  def get_days_from_now(date)
    date_formatter = NSDateFormatter.alloc.init
    date_formatter.dateFormat = "yyyy-MM-dd"
    sec = date_formatter.dateFromString(date).timeIntervalSinceNow
    (sec.to_i*(-1)/(3600*24)).to_s
  end

  def get_money(money)
    nf = NSNumberFormatter.alloc.init
    nf.setNumberStyle(NSNumberFormatterCurrencyStyle)
    nf.setMaximumFractionDigits(0)
    nf.stringFromNumber(money)
  end

  def init_nav
  end

  def on_return(args={})
    if args[:updated]
      reload_data
    end
  end

  def reload_data
    # A bug with custom cell, so just give up this function for now.
    # Api.get "listings/#{listing[:id]}" do |result, error|
    #   if result
    #     @listing = result
    #     update_form_data
    #     find.all.reapply_styles
    #   end
    # end
  end


  def sharing_message
    "Check out #{@listing[:address]} on the ProjectX app! #ProjectX"
  end

  def sharing_url
    "https://itunes.apple.com/us/app/ProjectX/id968479855?mt=8"
  end

  def sharing_image
    nil
  end

  def controller
    self # This is so that we can present the dialogs.
  end

  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
