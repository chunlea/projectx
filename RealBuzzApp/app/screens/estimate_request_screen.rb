class EstimateRequestScreen < PM::FormScreen
  title "Request Estimate"
  stylesheet WelcomeScreenStylesheet
  status_bar :light


  def on_load
  end

  def form_data
    [{
      title: "Address",
      footer: "",
      cells: [{
        name: "address",
        title: "",
        type: :longtext,
        placeholder: "The address of this Listing"
      }, {
        name: "zip",
        title: "Zipcode",
        type: :text,
        placeholder: "00000"
      }]
    }, {
      title: "Lisitng Information",
      footer: "",
      cells: [{
        name: "type_of_home",
        title: "Type of home",
        type: :option,
        options: ["Single Family", "Multifamily", "Condo", "Townhome"],
        value: "Single Family"
      }]
    }, {
      title: "Photos of this Listing",
      footer: "",
      cells: [{
         name: "photos",
         class: NSArray,
         sortable: true,
         inline: true,
         template: { type: :image },
         value: [],
       }]
    }, {
      title: "Listing Information",
      footer: "",
      cells: [{
        name: "square",
        title: "Square footage",
        type: :float,
        value: "0"
      }, {
        name: "bedrooms",
        title: "Number of bedrooms",
        type: :integer,
        cell: FXFormStepperCell,
        value: "0"
      }, {
        name: "bathrooms",
        title: "Number of bathrooms",
        type: :integer,
        cell: FXFormStepperCell,
        value: "0"
      }]
    }, {
      title: "",
      footer: "",
      cells: [{
        name: "condition",
        title: "Condition",
        type: :longtext,
        value: ""
      }, {
        name: "notes",
        title: "Notes",
        type: :longtext,
        value: ""
      }]
    }, {
      title: "",
      footer: "",
      cells: [{
        name: :submit,
        title: "Request Estimate",
        type: :button,
        "textLabel.color" => rmq.color.tint,
        "textLabel.textAlignment" => UITextAlignmentCenter,
        action: "add_new_estimate"
      }]
    }, {
      title: "",
      footer: "",
      cells: []
    }]
  end

  def add_new_estimate
    data = render_form
    SVProgressHUD.show

    photos = data[:photos]

    Api.post "request_listings", {listing: data} do |result, error|
      if result
        listing_id = result[:id]
        photos.each do |photo|
          photo_data = scaleImage(photo)
          AFMotion::SessionClient.shared.multipart_post("request_listings/#{listing_id}/upload") do |result, form_data|
            if form_data
              if photo_data
                form_data.appendPartWithFileData(photo_data, name: "photo", fileName:"photo.jpg", mimeType: "image/jpeg")
              end
            elsif result.success?
              p result
            else
              p "Upload Error"
            end
          end
        end
        SVProgressHUD.showSuccessWithStatus("")
        App.alert("Your Request had been send successfully.")
        app_delegate.open_main_screens
      end
      if error
        SVProgressHUD.showErrorWithStatus("")
        App.alert(error)
      end
    end
  end
  # Remove the following if you're only using portrait

  # You don't have to reapply styles to all UIViews, if you want to optimize, another way to do it
  # is tag the views you need to restyle in your stylesheet, then only reapply the tagged views, like so:
  #   def logo(st)
  #     st.frame = {t: 10, w: 200, h: 96}
  #     st.centered = :horizontal
  #     st.image = image.resource('logo')
  #     st.tag(:reapply_style)
  #   end
  #
  # Then in will_animate_rotate
  #   find(:reapply_style).reapply_styles#
  def will_animate_rotate(orientation, duration)
    reapply_styles
  end
end
