class ProfileScreen < PM::Screen
  # title "Your title here"
  stylesheet ProfileScreenStylesheet
  status_bar :light
  attr_accessor :user

  def on_load
    if @user
      self.title = @user[:name]
    end

    @username         = append!(UILabel, :username_label)
    @username.text    = @user[:name]
    @info             = append!(UILabel, :info_label)
    @info.text        = "#{@user[:estimates_count]} Estimates,  #{(@user[:accurate]*100).round(2)}% Accuracy"
    @user_role        = append!(UILabel, :user_role_label)
    @user_role.text   = @user[:role].to_s.capitalize
    append(UIButton, :message_btn).on(:touch) { message_to_user }
    @follow_btn       = append!(UIButton, :follow_btn)
    @follow_btn.on(:touch) { follow_user }
    append(UILabel, :estimates_label)
    @listing = UserEstimatesScreen.new(user: @user)
    @listing.delegate = self
    append(@listing.view, :listing_table)
    if @user[:followed]
      find(@follow_btn).apply_style(:unfollow_btn)
    else
      find(@follow_btn).apply_style(:follow_btn)
    end
  end

  def message_to_user
    open_modal MessageToUserScreen.new(nav_bar: true, user: @user)
  end

  def follow_user
    Api.get "users/#{@user[:id]}/toggle_follow" do |result, error|
      if result
        if result[:followed]
          find(@follow_btn).apply_style(:unfollow_btn)
        else
          find(@follow_btn).apply_style(:follow_btn)
        end
      end
      if error
        App.alert(error)
      end
    end
  end

  def open_detail_screen(listing)
    open DetailScreen.new(nav_bar: true, listing: listing), hide_tab_bar: true
  end


  # Remove the following if you're only using portrait

  # You don't have to reapply styles to all UIViews, if you want to optimize, another way to do it
  # is tag the views you need to restyle in your stylesheet, then only reapply the tagged views, like so:
  #   def logo(st)
  #     st.frame = {t: 10, w: 200, h: 96}
  #     st.centered = :horizontal
  #     st.image = image.resource('logo')
  #     st.tag(:reapply_style)
  #   end
  #
  # Then in will_animate_rotate
  #   find(:reapply_style).reapply_styles#
  def will_animate_rotate(orientation, duration)
    find.all.reapply_styles
  end
end
