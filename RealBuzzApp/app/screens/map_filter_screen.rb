class MapFilterScreen < PM::FormScreen
  title "Listing Filter"
  stylesheet MapFilterScreenStylesheet
  status_bar :light

  def on_load
    init_nav
  end

  def form_data
    [{
      title: "Price Filter",
      # footer: "Some help text",
      cells: [{
        name: "price_begin",
        title: "Min Price",
        type: :float,
        value: "0"
      }, {
        name: "price_end",
        title: "Max Price",
        type: :float,
      }]
    }, {
      title: "Other Filter",
      # footer: "Some help text",
      cells: [{
        name: "min_beds",
        title: "Min. Beds",
        type: :integer,
        value: "0",
        cell_class: FXFormStepperCell,
      }, {
        name: "min_baths",
        title: "Min. Baths",
        type: :integer,
        value: "0",
        cell_class: FXFormStepperCell,
      }]
    }]
  end

  def init_nav
    set_nav_bar_button :left, title: "Clear",  action: :nav_left_button
    set_nav_bar_button :right, title: "Set Filter",  action: :nav_right_button
  end

  def nav_left_button
    close_modal_screen(filter: nil)
  end
  def nav_right_button
    data = render_form
    close_modal_screen(filter: data)
  end

  # Remove the following if you're only using portrait

  # You don't have to reapply styles to all UIViews, if you want to optimize, another way to do it
  # is tag the views you need to restyle in your stylesheet, then only reapply the tagged views, like so:
  #   def logo(st)
  #     st.frame = {t: 10, w: 200, h: 96}
  #     st.centered = :horizontal
  #     st.image = image.resource('logo')
  #     st.tag(:reapply_style)
  #   end
  #
  # Then in will_animate_rotate
  #   find(:reapply_style).reapply_styles#
  def will_animate_rotate(orientation, duration)
    find.all.reapply_styles
  end
end
