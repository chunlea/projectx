class ListScreen < PM::TableScreen
  title "Listings Near Here"
  stylesheet ListScreenStylesheet
  status_bar :light
  attr_accessor :region
  searchable placeholder: "Search Listing around here"

  def on_load
    self.edgesForExtendedLayout = UIRectEdgeBottom
    table_view.addPullToRefreshWithActionHandler -> { reload_data }
    table_view.addInfiniteScrollingWithActionHandler -> { load_more }
    @page = 1
    @data = []

    reload_data
  end

  def table_data
    [{
      cells: @data.map do |listing|
        {
          title: get_money(listing[:price]),
          subtitle: "#{listing[:bedrooms]} Beds #{listing[:baths]} Baths",
          search_text: "#{listing[:address]}",
          action: :open_detail_screen,
          arguments: { listing: listing },
          height: 50,
          remote_image: {  # remote image, requires JMImageCache CocoaPod
            url: listing[:photos].first.to_s,
            placeholder: "icon-512", # NOTE: this is required!
            size: 50,
            radius: 0,
            content_mode: :scale_aspect_fill
          }
        }
      end
    }]
  end

  def open_detail_screen(item)
    open DetailScreen.new(nav_bar: true, listing: item[:listing]), hide_tab_bar: true
  end

  def load_data
    params = {page: @page, lat: region.center.latitude, lon: region.center.longitude, radius: region.span.latitudeDelta/2*111}

    Api.get "listings/nearests", params do |result, error|
      if result
        if @page == 1
          @data = result
        else
          @data += result
        end
        update_table_data
        table_view.pullToRefreshView.stopAnimating
        table_view.infiniteScrollingView.stopAnimating
      end
      # if error
      #   App.alert(error)
      # end
    end
  end

  def reload_data
    @page = 1
    load_data
  end

  def load_more
    @page += 1
    load_data
  end

  def get_money(money)
    nf = NSNumberFormatter.alloc.init
    nf.setNumberStyle(NSNumberFormatterCurrencyStyle)
    nf.setMaximumFractionDigits(0)
    nf.stringFromNumber(money)
  end

  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
