class SignInScreen < PM::FormScreen
  title "Sign in"
  stylesheet WelcomeScreenStylesheet
  status_bar :light

  def on_load
    set_nav_bar_button :right, title: "Close", action: :close_screen
  end

  def close_screen
    close_modal_screen
  end

  def form_data
    [{
      title: "Account Information",
      footer: "",
      cells: [{
        name: "email",
        title: "Email",
        type: :email,
      }, {
        name: "password",
        title: "Password",
        type: :password,
      }]
    },{
      title: "",
      # footer: "Some help text",
      cells: [{
        name: :submit,
        title: "Login",
        type: :button,
        "textLabel.color" => rmq.color.tint,
        "textLabel.textAlignment" => UITextAlignmentCenter,
        action: "login"
      }]
    }, {
      title: "Forget your password",
      # footer: "Some help text",
      cells: [{
        name: :submit,
        title: "Forget password",
        type: :button,
        "textLabel.color" => rmq.color.tint,
        action: "open_forget_password"
      }]
    }, {
      title: "Join us now",
      # footer: "Some help text",
      cells: [{
        name: :submit,
        title: "Sign up",
        type: :button,
        "textLabel.color" => rmq.color.tint,
        action: "open_register_screen"
      }]
    }]
  end


  def login
    data = render_form
    if data[:email].empty? or data[:password].empty?
      App.alert("Email and password can't be blank")
      return false
    end

    data[:device_id]  = App::Persistence['token']
    data[:model]      = UIDevice.currentDevice.model
    data[:name]       = UIDevice.currentDevice.name
    data[:os_name]    = UIDevice.currentDevice.systemName
    data[:os_version] = UIDevice.currentDevice.systemVersion

    SVProgressHUD.show
    Api.post "auth/login", data do |result, error|
      if result
        Auth.set id: result[:user][:id], email: result[:user][:email], token: result[:token], name: result[:user][:name], zip_code: result[:user][:zip_code], role: result[:user][:role], facebook_id: result[:user][:facebook_id], confirmed: result[:confirmed]
        App::Persistence['user'] = result[:user][:id]

        if Auth.role == "agent"
          # get profile
          @data = {user_id: Auth.id.to_i}
          Api.get "profiles", @data do |result, error|
            if result
              Profile.each do |x|
                x.destroy
              end
              Profile.create(bio:result[:bio], degree:result[:degree], yr_graduated:result[:yr_graduated], college:result[:college], other_school:result[:other_school], designations:result[:designations], linked_in:result[:linked_in], website:result[:website], profile_pic:result[:profile_pic], verified:result[:verified], user_id:result[:id], agent_id:result[:agent_id])
              app_delegate.open_main_screens
            end
            if error
              open NewProfileScreen.new
            end
          end
        end
        # end profile stuff

        SVProgressHUD.showSuccessWithStatus("")
        # app_delegate.open_main_screens
      end
      if error
        SVProgressHUD.showErrorWithStatus("")
        App.alert(error)
      end
    end
  end

  def open_register_screen
    open SignUpScreen.new(nav_bar: true)
  end

  def open_forget_password
    # open ForgetPasswordScreen.new(nav_bar: true)
    open WebScreen.new(nav_bar: true, link: "#{BASE_HOST}users/password/new".to_url, screen_title: "Forget Password")
  end


  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
