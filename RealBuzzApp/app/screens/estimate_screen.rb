class EstimateScreen < PM::FormScreen
  title "Check In and Estimate"
  stylesheet EstimateScreenStylesheet
  status_bar :light
  attr_accessor :listing

  def on_load
    set_nav_bar_button :right, title: "Close", action: :close_screen
    @estimate = {}
    fetch_estimate_from_server
  end

  def close_screen
    close_modal_screen
  end

  def form_data
    [{
      title: "Your Review and Estimate",
      footer: "",
      cells: [{
        name: "location_rating",
        title: "Location",
        "textField.enabled" => false,
        cell_class: RatingCell,
        value: @estimate.fetch(:location_rating, nil).to_s
      }, {
        name: "condition_rating",
        title: "Condition",
        "textField.enabled" => false,
        cell_class: RatingCell,
        value: @estimate.fetch(:condition_rating, nil).to_s
      }]
    }, {
      title: "Estimate your price",
      footer: "",
      cells: [{
        name: "price",
        title: "Price(US$)",
        type: :float,
        value: @estimate.fetch(:price, nil).to_s
      }, {
        name: "reason",
        title: "Reason",
        type: :longtext,
        value: @estimate.fetch(:reason, nil).to_s
      }]
    }, {
      title: "",
      # footer: "",
      cells: [{
        title: (@estimate.empty?? "Check In and Estimate":"Update my previous Estimate"),
        type: :button,
        action: "estimate",
        "textLabel.color" => rmq.color.tint,
      }]
    }]
  end

  def fetch_estimate_from_server
    Api.get "listings/#{listing[:id]}/estimates" do |result, error|
      if result
        @estimate = result.first if result.first
        update_form_data
      end
    end
  end

  def estimate
    data = render_form
    if data[:location_rating].empty? or data[:condition_rating].empty?
      App.alert("Please give your rating of Location and Condition of this Listing.")
      return false
    end
    if data[:price] == "" or data[:price] == false or data[:reason].empty?
      App.alert("Please estimate your price and the reason first.")
      return false
    end
    data[:price] = data[:price].floatValue.to_f

    if @estimate.fetch(:id, nil)
      Api.put "listings/#{listing[:id]}/estimates/#{@estimate.fetch(:id, nil)}", {estimate: data} do |result, error|
        if result
          @estimate = result
          update_form_data
          App.alert("Your estimate has been saved.")
          close_modal_screen(updated: true)
        end
        if error
          App.alert(error)
        end
      end
    else
      Api.post "listings/#{listing[:id]}/estimates", {estimate: data} do |result, error|
        if result
          @estimate = result
          update_form_data
          App.alert("Your estimate has been saved.")
          close_modal_screen(updated: true)
        end
        if error
          App.alert(error)
        end
      end
    end
  end

  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
