class NewOfferScreen < PM::FormScreen
  title "New Offer"
  stylesheet WelcomeScreenStylesheet
  status_bar :light
  attr_accessor :listing

  def on_load
    set_nav_bar_button :right, title: "Close", action: :close_screen
    p @listing
  end

  def close_screen
    close_modal_screen
  end

  def form_data
    [{
      title: "Let's make and offer!",
      footer: "",
      cells: [{
        name: "price",
        title: "Offer Price",
        type: :text,
      }, {
        name: "escrow_period",
        title: "Escrow Period (in days)",
        type: :text,
      }, {
        name: "due_dill",
        title: "Due Dilligence (in days)",
        type: :text,
      }, {
        name: "as_is",
        title: "As Is?",
        type: :boolean,
      }, {
        name: "cont_finance",
        title: "Contigent on Financing?",
        type: :boolean,
      }, {
        name: "cont_inspect",
        title: "Contigent on Inspection?",
        type: :boolean,
      }, {
        name: "prequalified",
        title: "Prequalified?",
        type: :boolean,
      }, {
        name: "notes",
        title: "Offer Notes",
        type: :text,
      }]
    },{
      title: "",
      # footer: "Some help text",
      cells: [{
        name: :submit,
        title: "Submit Offer",
        type: :button,
        "textLabel.color" => rmq.color.tint,
        "textLabel.textAlignment" => UITextAlignmentCenter,
        action: "submit_offer"
      }]
    }]
  end


  def submit_offer
    data = render_form
    @price = data[:price].to_i
    @escrow = data[:escrow_period].to_i
    @due_dill = data[:due_dill].to_i
    if data[:as_is] == true
      @as_is = "Yes"
    else
      @as_is = "No"
    end

    if data[:cont_finance] == true
      @cont_finance = "Yes"
    else
      @cont_finance = "No"
    end

    if data[:cont_inspect] == true
      @cont_inspect = "Yes"
    else
      @cont_inspect = "No"
    end

    if data[:prequalified] == true
      @prequalified = "Yes"
    else
      @prequalified = "No"
    end
    
    @notes = data[:notes]

    SVProgressHUD.show
    data = {offer: {price:data[:price], escrow_period:data[:escrow_period], due_dill:data[:due_dill], as_is:data[:as_is], cont_finance:data[:cont_finance], cont_inspect:data[:cont_inspect], prequalified:data[:prequalified], notes:data[:notes], user_id:Auth.id, listing_id:@listing[:id]}}
    Api.post "offers", data do |result, error|
      if result
        p result
       
        send_message
        SVProgressHUD.showSuccessWithStatus("")
        App.alert("Offer Submitted")
        close_screen
      end
      if error
        SVProgressHUD.showErrorWithStatus("")
        App.alert(error)
      end
    end
  end

  def send_message
    #data = render_form

    data = {body:"New Offer!\n\n\nPrice: $#{@price}\nEscrow Period: #{@escrow}\nDue Dilligence Period: #{@due_dill}\nAs-Is?: #{@as_is}\nContingent on Financing: #{@cont_finance}\nContingent on Inspection: #{@cont_inspect}\nPrequalified?: #{@prequalified}\nNotes: #{@notes}", user_id: @listing[:user][:id]}



    Api.post "messages", data do |result, error|
      if result
        # App.alert("Your message has been sent")
        #`close_modal_screen(updated: true)
      end
      if error
        App.alert(error)
      end
    end
  end
end







