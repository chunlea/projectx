class MenuScreen < PM::TableScreen
  title "Menu"
  stylesheet MenuScreenStylesheet
  status_bar :light

  def on_load
    view.tableHeaderView = create!(UIView, :table_header).tap do |q|
      @pic = q.append(UIImageView, :user_profile_pic).get
      if Profile.first
        @pic.image = get_photos(Profile.first.profile_pic)
      else
        @pic.image = rmq.image.resource("icon-512")
      end
      q.append(UILabel, :username_label)
    end
    view.tableFooterView = create!(UIView)

    find(view.tableHeaderView).on(:tap) do |sender|
      open_menu MyProfileScreen.new(nav_bar: true)
    end

    set_toolbar_items [{
        system_item: :flexible_space
      }, {
        title: "Logout",
        action: :logout,
        # image: FAKIonIcons.mapIconWithSize(40).addAttribute(NSForegroundColorAttributeName, value:rmq.color.tint).imageWithSize(CGSizeMake(40, 40))
        # custom_view:  create!(UIView, :logout_btn).tap do |q|
        #                 # q.append(UILabel, :hello_world)
        #               end
      }, {
        system_item: :flexible_space
      }]
  end
  def get_photos(photo)
    image = rmq.image.resource("icon-512")
    JMImageCache.sharedCache.imageForURL(photo.to_s.to_url , completionBlock: lambda do |downloadedImage|
      image = downloadedImage
    end)
    image
  end
  def logout
    app_delegate.logout
  end
  def table_data
    [{
      cells: [
        {
          title: "Home",
          action: "open_home_screen",
          height: 80,
          properties: { # (Edge change, use `style:` in ProMotion 2.0.x)
            background_color: rmq.color.clear, # Creates a UIView for the backgroundView
            text_color: rmq.color.white,
            selection_style: UITableViewCellSelectionStyleGray,
          },
          image: {
            image: FAKIonIcons.iosHomeIconWithSize(40).addAttribute(NSForegroundColorAttributeName, value:rmq.color.white).imageWithSize(CGSizeMake(40, 40)), # PM will do `UIImage.imageNamed("something")` for you
          },
        }, {
          title: "Messages",
          action: "open_messages_screen",
          height: 80,
          properties: { # (Edge change, use `style:` in ProMotion 2.0.x)
            background_color: rmq.color.clear, # Creates a UIView for the backgroundView
            text_color: rmq.color.white,
            selection_style: UITableViewCellSelectionStyleGray,
          },
          image: {
            image: FAKIonIcons.iosEmailIconWithSize(40).addAttribute(NSForegroundColorAttributeName, value:rmq.color.white).imageWithSize(CGSizeMake(40, 40))
          },
        }, {
          title: "Favorites",
          action: "open_favorites_screen",
          height: 80,
          properties: { # (Edge change, use `style:` in ProMotion 2.0.x)
            background_color: rmq.color.clear, # Creates a UIView for the backgroundView
            text_color: rmq.color.white,
            selection_style: UITableViewCellSelectionStyleGray,
          },
          image: {
            image: FAKIonIcons.iosStarIconWithSize(40).addAttribute(NSForegroundColorAttributeName, value:rmq.color.white).imageWithSize(CGSizeMake(40, 40))
          },
        }, {
          title: "Your Listings",
          action: "open_my_listings_screen",
          height: 80,
          properties: { # (Edge change, use `style:` in ProMotion 2.0.x)
            background_color: rmq.color.clear, # Creates a UIView for the backgroundView
            text_color: rmq.color.white,
            selection_style: UITableViewCellSelectionStyleGray,
          },
          image: {
            image: FAKIonIcons.earthIconWithSize(40).addAttribute(NSForegroundColorAttributeName, value:rmq.color.white).imageWithSize(CGSizeMake(40, 40))
          },
        }, {
          title: "Settings",
          action: "open_settings_screen",
          height: 80,
          properties: { # (Edge change, use `style:` in ProMotion 2.0.x)
            background_color: rmq.color.clear, # Creates a UIView for the backgroundView
            text_color: rmq.color.white,
            selection_style: UITableViewCellSelectionStyleGray,
          },
          image: {
            image: FAKIonIcons.iosCogIconWithSize(40).addAttribute(NSForegroundColorAttributeName, value:rmq.color.white).imageWithSize(CGSizeMake(40, 40)), # PM will do `UIImage.imageNamed("something")` for you
            radius: 15, # radius is optional
          },
        }
      ]
    }]
  end

  def open_menu(menu)
    app_delegate.menu.center_controller = menu
    app_delegate.menu.toggle_left
  end

  def open_home_screen;      open_menu app_delegate.main_screens;          end
  def open_settings_screen;  open_menu SettingsScreen.new(nav_bar: true);  end
  def open_favorites_screen; open_menu FavoritesScreen.new(nav_bar: true); end
  def open_messages_screen;  open_menu MessagesScreen.new(nav_bar: true);  end
  def open_my_listings_screen;  open_menu MyListingsScreen.new(nav_bar: true);  end


  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
