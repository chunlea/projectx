class SettingsScreen < PM::FormScreen
  title "Settings"
  stylesheet WelcomeScreenStylesheet
  status_bar :light

  def on_load
    init_nav
  end
  def init_nav
    toggleOutlineIcon = FAKIonIcons.iosToggleOutlineIconWithSize(25)
    set_nav_bar_button :left,  image: toggleOutlineIcon.imageWithSize(CGSizeMake(25, 25)), action: :nav_left_button
  end

  def nav_left_button
    app_delegate.menu.toggle_left
  end

  def form_data
    # if Auth.role == "agent"
    #   agent = {
    #   title: "Update your agent profile",
    #   footer: "",
    #   cells: [{
    #     name: :submit,
    #     title: "Update your agent profile",
    #     type: :button,
    #     action: "open_update_profile",
    #     properties: {
    #       "accessoryType" => UITableViewCellAccessoryDisclosureIndicator,
    #       "textLabel.color" => rmq.color.tint,
    #       # "accessoryView" => CustomAccessory.new,
    #       # "backgroundColor"      => color.red,
    #       # "detailTextLabel.font" => UIFont.fontWithName("MyFont", size:20),
    #     }
    #   }]
    # }
    # else
      agent = {}
    # end
    [{
      title: "Account Information",
      footer: "",
      cells: [{
        name: "name",
        title: "Name",
        type: :text,
        value: Auth.name
      }, {
        name: "email",
        title: "Email",
        type: :email,
        value: Auth.email
      }, {
        name: "type",
        title: "Agent",
        type: :boolean,
        value: (Auth.role == "agent")? "1":"0"
      }, {
        name: "zip_code",
        title: "Zip Code",
        type: :text,
        value: Auth.zip_code
      }]
    }, {
      title: "",
      footer: "",
      cells: [{
        name: :submit,
        title: "Extra Zip Codes",
        type: :button,
        action: "open_extra_zip_codes",
        properties: {
          "accessoryType" => UITableViewCellAccessoryDisclosureIndicator,
          "textLabel.color" => rmq.color.tint,
          # "accessoryView" => CustomAccessory.new,
          # "backgroundColor"      => color.red,
          # "detailTextLabel.font" => UIFont.fontWithName("MyFont", size:20),
        }
      }]
    }, agent , {
      title: "Change Password",
      footer: "",
      cells: [{
        name: "current_password",
        title: "Current Password",
        type: :password,
        footer: "Leave it blank if you don't want to change password.",
      }, {
        name: "password",
        title: "New Password",
        type: :password,
      }, {
        name: "password_confirmation",
        title: "New Password Confirmation",
        type: :password,
      }]
    }, {
      title: "",
      # footer: "Some help text",
      cells: [{
        name: :submit,
        title: "Update",
        type: :button,
        "textLabel.color" => rmq.color.tint,
        action: "update"
      }]
    }]
  end

  def update
    data = render_form
    if data[:type]=="" or data[:type].boolValue == false
      data[:role] = "buyer"
    else
      data[:role] = "agent"
    end

    if data[:current_password]==""
      data.delete(:current_password)
      data.delete(:password)
      data.delete(:password_confirmation)
    end

    if data[:zip_code].empty?
      App.alert("You need to enter your zip code.")
      return false
    end
    p data

    Api.put "users/#{Auth.id}", {user: data } do |result, error|
      if result
        p result
        Auth.update id: result[:id], email: result[:email], name: result[:name], zip_code: result[:zip_code], role: result[:role], facebook_id: result[:facebook_id], confirmed: result[:confirmed]
        App.alert("Your information has been updated")
      end
      if error
        App.alert(error)
      end
    end
  end

  def open_extra_zip_codes
    open ExtraZipCodesScreen.new(nav_bar: true)
  end

  def open_update_profile
    open UpdateProfileScreen.new(nav_bar: true)
  end

  def open_profile
    App.alert(Auth.name)
  end

  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
