class SignUpScreen < PM::FormScreen
  title "Sign up"
  stylesheet WelcomeScreenStylesheet
  status_bar :light

  def on_load
    set_nav_bar_button :right, title: "Close", action: :close_screen
  end

  def close_screen
    close_modal_screen
  end

  def form_data
    [{
      title: "Account Information",
      footer: "",
      cells: [{
        name: "name",
        title: "Name",
        type: :text,
      }, {
        name: "email",
        title: "Email",
        type: :email,
      }, {
        name: "type",
        title: "Agent",
        type: :boolean,
        value: false
      }, {
        name: "password",
        title: "Password",
        type: :password,
      }, {
        name: "password_confirmation",
        title: "Password Confirmation",
        type: :password,
      }, {
        name: "zip_code",
        title: "Zip Code",
        type: :text,
      }]
    },{
      title: "",
      # footer: "Some help text",
      cells: [{
        name: :submit,
        title: "Sign Up",
        type: :button,
        "textLabel.color" => rmq.color.tint,
        action: "signup"
      }]
    }]
  end

  def open_login
    open_root_screen LoginScreen.new(nav_bar: true)
  end

  def signup
    data = render_form
    if data[:type]=="" or data[:type].boolValue == false
      data[:role] = "buyer"
    else
      data[:role] = "agent"
    end
    if data[:email].empty? or data[:password].empty?
      App.alert("Email and password can't be blank.")
      return false
    end
    if data[:zip_code].empty?
      App.alert("You need to enter your zip code.")
      return false
    end
    SVProgressHUD.show
    Api.post "auth/register", data do |result, error|
      if result
        Auth.set id: result[:user][:id], email: result[:user][:email], token: result[:token], name: result[:user][:name], zip_code: result[:user][:zip_code], role: result[:user][:role], facebook_id: result[:user][:facebook_id], confirmed: result[:confirmed]
        SVProgressHUD.showSuccessWithStatus("")
        #app_delegate.open_main_screens
        if data[:role] == "agent"
          open NewProfileScreen.new
        else
          app_delegate.open_main_screens
        end
      end
      if error
        SVProgressHUD.showErrorWithStatus("")
        App.alert(error)
      end
    end
  end


  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
