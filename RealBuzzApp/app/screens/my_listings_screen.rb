class MyListingsScreen < PM::TableScreen
  title "Your Listings (tap to edit)"
  stylesheet ListScreenStylesheet
  status_bar :light
  searchable placeholder: "Search Your Listings"

  def on_load
    init_nav
    self.edgesForExtendedLayout = UIRectEdgeBottom
    table_view.addPullToRefreshWithActionHandler -> { reload_data }
    table_view.addInfiniteScrollingWithActionHandler -> { load_more }
    @page = 1
    @data = []

    reload_data
  end
  
  def table_data
    [{
      cells: @data.map do |listing|
        {
          title: "#{listing[:address]}",
          subtitle: "#{listing[:bedrooms]} Beds #{listing[:baths]} Baths",
          search_text: "#{listing[:address]}",
          action: :open_detail_screen,
          arguments: { listing: listing },
          height: 50,
          remote_image: {  # remote image, requires JMImageCache CocoaPod
            url: listing[:photos].first.to_s,
            placeholder: "icon-512", # NOTE: this is required!
            size: 50,
            radius: 0,
            content_mode: :scale_aspect_fill
          }
        }
      end
    }]
  end

  def open_detail_screen(item)
    open EditListingScreen.new(nav_bar: true, listing: item[:listing]), hide_tab_bar: true
  end

  def load_data
    params = {page: @page, user_id: Auth.id}

    Api.get "own_listings", params do |result, error|
      if result
        if @page == 1
          @data = result
        else
          @data += result
        end
        update_table_data
        table_view.pullToRefreshView.stopAnimating
        table_view.infiniteScrollingView.stopAnimating
      end
      # if error
      #   App.alert(error)
      # end
    end
  end

  def reload_data
    @page = 1
    load_data
  end

  def load_more
    @page += 1
    load_data
  end

  def get_money(money)
    nf = NSNumberFormatter.alloc.init
    nf.setNumberStyle(NSNumberFormatterCurrencyStyle)
    nf.setMaximumFractionDigits(0)
    nf.stringFromNumber(money)
  end

  def init_nav
    toggleOutlineIcon = FAKIonIcons.iosToggleOutlineIconWithSize(25)
    naviconRoundIcon = FAKIonIcons.naviconRoundIconWithSize(25)
    set_nav_bar_button :left,  image: toggleOutlineIcon.imageWithSize(CGSizeMake(25, 25)), action: :nav_left_button
  end

  def nav_left_button
    app_delegate.menu.toggle_left
  end

  def will_animate_rotate(orientation, duration)
    reapply_styles
  end
end
