class WelcomeScreen < PM::Screen
  title "ProjectX"
  stylesheet WelcomeScreenStylesheet
  status_bar :light

  def on_load
    init_intro
    append(UIButton, :sign_up_btn).on(:touch) { open_sign_up_screen }
    append(UIButton, :sign_in_btn).on(:touch) { open_sign_in_screen }

    # @fb_login_button                 = append!(FBLoginView.new, :fb_login_button)
    @fb_login_button                 = append!(app_delegate.fb_loginview, :fb_login_button)
    @fb_login_button.readPermissions = ["public_profile", "email"]
    @fb_login_button.delegate        = self
    find(@fb_login_button).layout fb: 80
  end

  def will_disappear
    @fb_login_button.delegate = nil
  end
  def on_appear
    @fb_login_button.delegate = self
  end

  def open_sign_up_screen
    open_modal SignUpScreen.new(nav_bar: true)
  end

  def open_sign_in_screen
    open_modal SignInScreen.new(nav_bar: true)
  end

  def init_intro
    page1                    = EAIntroPage.page
    page1.titleIconView      = UIImageView.alloc.initWithImage(rmq.image.resource("icon-76"))
    page1.titleIconPositionY = 120
    page1.bgImage            = rmq.image.resource("bg1")
    #page1.title              = "ProjectX"
    page1.titleFont          = rmq.font.large
    # page1.titleColor       = rmq.color.red
    page1.titlePositionY     = 380
    #page1.desc               = "Real Estate Everywhere"
    page1.descFont           = rmq.font.large
    # page1.descColor        = rmq.color.red
    # page1.descWidth        = 300
    page1.descPositionY      = 300

    page2                    = EAIntroPage.page
    page2.titleIconView      = UIImageView.alloc.initWithImage(rmq.image.resource("icon-76"))
    page2.titleIconPositionY = 120
    page2.bgImage            = rmq.image.resource("bg2")
    page2.title              = "ProjectX 2"
    page2.titleFont          = rmq.font.large
    # page2.titleColor       = rmq.color.red
    page2.titlePositionY     = 380
    page2.desc               = "Real Estate Everywhere"
    page2.descFont           = rmq.font.large
    # page2.descColor        = rmq.color.red
    # page2.descWidth        = 300
    page2.descPositionY      = 300

    page3                    = EAIntroPage.page
    page3.titleIconView      = UIImageView.alloc.initWithImage(rmq.image.resource("icon-76"))
    page3.titleIconPositionY = 120
    page3.bgImage            = rmq.image.resource("bg3")
    page3.title              = "ProjectX 3"
    page3.titleFont          = rmq.font.large
    # page3.titleColor       = rmq.color.red
    page3.titlePositionY     = 380
    page3.desc               = "Real Estate Everywhere"
    page3.descFont           = rmq.font.large
    # page3.descColor        = rmq.color.red
    # page3.descWidth        = 300
    page3.descPositionY      = 300

    page4                    = EAIntroPage.page
    page4.titleIconView      = UIImageView.alloc.initWithImage(rmq.image.resource("icon-76"))
    page4.titleIconPositionY = 120
    page4.bgImage            = rmq.image.resource("bg4")
    page4.title              = "ProjectX 4"
    page4.titleFont          = rmq.font.large
    # page4.titleColor       = rmq.color.red
    page4.titlePositionY     = 380
    page4.desc               = "Real Estate Everywhere"
    page4.descFont           = rmq.font.large
    # page4.descColor        = rmq.color.red
    # page4.descWidth        = 300
    page4.descPositionY      = 300

    intro = EAIntroView.alloc.initWithFrame(self.view.bounds,
                        andPages: [page1, page2, page3, page4])

    intro.pageControlY         = 100
    # intro.titleView          = UIImageView.alloc.initWithImage(rmq.image.resource("icon-76"))
    # intro.titleViewY         = 100
    intro.swipeToExit          = false
    # intro.tapToNext          = true
    intro.hideOffscreenPages   = true
    intro.easeOutCrossDisolves = true
    intro.useMotionEffects     = true

    intro.setDelegate self
    intro.showInView(self.view, animateDuration:0.1)
  end

  def introDidFinish(introView)
    open_sign_in_screen
  end

  def loginViewFetchedUserInfo(_, user: user)
    user[:device_id]  = App::Persistence['token']
    user[:model]      = UIDevice.currentDevice.model
    user[:name]       = UIDevice.currentDevice.name
    user[:os_name]    = UIDevice.currentDevice.systemName
    user[:os_version] = UIDevice.currentDevice.systemVersion

    Api.fblogin user do |result, error|
      if result
        p result
        Auth.set id: result[:user][:id], email: result[:user][:email], token: result[:token], name: result[:user][:name], zip_code: result[:user][:zip_code], role: result[:user][:role], facebook_id: result[:user][:facebook_id], confirmed: result[:confirmed]
        SVProgressHUD.showSuccessWithStatus("")
        app_delegate.open_main_screens
      end
      if error
        App.alert(error)
      end
    end
  end

  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
