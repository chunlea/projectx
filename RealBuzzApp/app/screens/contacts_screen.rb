class ContactsScreen < PM::FormScreen
  title "Contact The Seller or Agent"
  stylesheet ContactsScreenStylesheet
  status_bar :light
  attr_accessor :listing

  def on_load
    set_nav_bar_button :right, title: "Cancel", action: :close_screen
  end

  def close_screen
    close_modal_screen
  end

  def form_data
    [{
      title: "Send message to The Seller or Agent",
      footer: "",
      cells: [{
        name: "body",
        title: "Message",
        type: :longtext,
      }]
    },{
      title: "",
      # footer: "Some help text",
      cells: [{
        name: :submit,
        title: "Send Message",
        type: :button,
        "textLabel.color" => rmq.color.tint,
        "textLabel.textAlignment" => UITextAlignmentCenter,
        action: "send_message"
      }]
    }]
  end

  def send_message
    data = render_form
    if data[:body].empty?
      App.alert("You can't send empty messages.")
      return false
    end
    data[:listing_id] = listing[:id]

    p data

    Api.post "messages", data do |result, error|
      if result
        App.alert("Your message has been sent.")
        close_modal_screen(updated: true)
      end
      if error
        App.alert(error)
      end
    end
  end

  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
