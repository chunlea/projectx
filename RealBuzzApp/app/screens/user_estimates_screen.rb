class UserEstimatesScreen < PM::TableScreen
  title "Estimates"
  stylesheet UserEstimatesScreenStylesheet
  attr_accessor :user, :delegate

  def on_load
    self.edgesForExtendedLayout = UIRectEdgeBottom
    table_view.addPullToRefreshWithActionHandler -> { reload_data }
    table_view.addInfiniteScrollingWithActionHandler -> { load_more }
    @page = 1
    @data = []

    reload_data
  end

  def table_data
    [{
      cells: @data.map do |estimate|
        {
          title: "#{estimate[:listing][:address]}",
          subtitle: "Last Sold Price: #{get_money(estimate[:listing][:last_sold_price])}",
          search_text: "#{estimate[:listing][:address]}",
          action: :open_detail_screen,
          arguments: { listing: estimate[:listing] },
          height: 50,
          remote_image: {  # remote image, requires JMImageCache CocoaPod
            url: estimate[:listing][:photos].first,
            placeholder: "icon-512", # NOTE: this is required!
            size: 50,
            radius: 0,
            content_mode: :scale_aspect_fill
          }
        }
      end
    }]
  end

  def open_detail_screen(item)
    @delegate.open_detail_screen(item[:listing])
  end

  def load_data
    params = {page: @page}
    unless @user
      @user = Auth.user
    end

    Api.get "users/#{@user[:id]}/estimates", params do |result, error|
      if result
        p result
        if @page == 1
          @data = result
        else
          @data += result
        end
        update_table_data
        table_view.pullToRefreshView.stopAnimating
        table_view.infiniteScrollingView.stopAnimating
      end
      # if error
      #   App.alert(error)
      # end
    end
  end

  def reload_data
    @page = 1
    load_data
  end

  def load_more
    @page += 1
    load_data
  end

  def get_money(money)
    nf = NSNumberFormatter.alloc.init
    nf.setNumberStyle(NSNumberFormatterCurrencyStyle)
    nf.setMaximumFractionDigits(0)
    nf.stringFromNumber(money)
  end


  # Remove the following if you're only using portrait

  # You don't have to reapply styles to all UIViews, if you want to optimize, another way to do it
  # is tag the views you need to restyle in your stylesheet, then only reapply the tagged views, like so:
  #   def logo(st)
  #     st.frame = {t: 10, w: 200, h: 96}
  #     st.centered = :horizontal
  #     st.image = image.resource('logo')
  #     st.tag(:reapply_style)
  #   end
  #
  # Then in will_animate_rotate
  #   find(:reapply_style).reapply_styles#
  def will_animate_rotate(orientation, duration)
    find.all.reapply_styles
  end
end
