class MessagesScreen < PM::TableScreen
  title_view UISegmentedControl.alloc.initWithItems(["Unread", "All Messages"]) #, "Archived"
  stylesheet MessagesScreenStylesheet
  status_bar :light
  attr_accessor :read_message

  def on_load
    init_nav
    self.edgesForExtendedLayout = UIRectEdgeBottom
    @page = 1
    @url = "messages/unread"

    table_view.addPullToRefreshWithActionHandler -> { reload_data }
    table_view.addInfiniteScrollingWithActionHandler -> { load_more }
    @messages = []

    reload_data
  end

  def table_data
    [{
      cells: @messages.map do |message|
        {
          title: "From #{message[:sender][:name]} (#{message[:created_at]})",
          subtitle: message[:body],
          action: :read_message,
          # height: 80,
          arguments: { message: message },
          # properties: { # (Edge change, use `style:` in ProMotion 2.0.x)
          #   masks_to_bounds: true,
          #   background_color: UIColor.whiteColor, # Creates a UIView for the backgroundView
          # },
        }
      end
    }]
  end

  def read_message(item)
    if @url == "messages/unread"
      Api.get "messages/#{item[:message][:id]}/read" do |result, error|
      end
    end
    open MessageDetailScreen.new(nav_bar: true, message: item[:message])
  end

  def will_appear
    find(self.navigationItem.titleView).off(:value_changed)
    find(self.navigationItem.titleView).on(:value_changed) do |sender|
      case sender.selectedSegmentIndex
      when 1
        get_all_messages
      when 2
        get_archived_messages
      else
        get_unread_messages
      end
    end
  end

  def get_all_messages
    @url = "messages"
    reload_data
  end
  def get_archived_messages
    @url = "messages/archived"
    reload_data
  end
  def get_unread_messages
    @url = "messages/unread"
    reload_data
  end

  def load_data
    params = {page: @page}

    Api.get @url, params do |result, error|
      if result
        if @page == 1
          @messages = result
        else
          @messages += result
        end
        update_table_data
        table_view.pullToRefreshView.stopAnimating
        table_view.infiniteScrollingView.stopAnimating
      end
      if error
        App.alert(error)
      end
    end
  end

  def reload_data
    @page = 1
    load_data
  end

  def load_more
    @page += 1
    load_data
  end

  def on_return(args={})
    @user = args[:to_user]
    if @user
      open_modal MessageToUserScreen.new(nav_bar: true, user: @user)
    end
  end

  def init_nav
    self.navigationItem.titleView.segmentedControlStyle = UISegmentedControlStyleBordered
    self.navigationItem.titleView.sizeToFit
    self.navigationItem.titleView.selectedSegmentIndex = 0

    toggleOutlineIcon = FAKIonIcons.iosToggleOutlineIconWithSize(25)
    set_nav_bar_button :left,  image: toggleOutlineIcon.imageWithSize(CGSizeMake(25, 25)), action: :nav_left_button
    set_nav_bar_button :right, system_item: :compose,  action: :nav_right_button
  end

  def nav_left_button
    app_delegate.menu.toggle_left
  end

  def nav_right_button
    open_modal FriendsScreen.new(nav_bar: true, message_to_user: true)
  end

  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
