class WebScreen < PM::WebScreen
  # title "Your title here"
  stylesheet WebScreenStylesheet
  attr_accessor :link, :screen_title

  def on_load
    if @screen_title
      self.title = @screen_title.to_s
    end
  end


  def content
    @link
    # You can return:
    #  1. A reference to a file placed in your resources directory
    #  2. An instance of NSURL
    #  3. An arbitrary HTML string
    # "AboutView.html"
  end

  def load_started
    # Optional
    # Called when the request starts to load
  end

  def load_finished
    # Optional
    # Called when the request is finished
  end

  def load_failed(error)
    # Optional
    # "error" is an instance of NSError
  end

  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
