class FriendsScreen < PM::TableScreen
  title_view UISegmentedControl.alloc.initWithItems(["Following", "Followers"])
  stylesheet FriendsScreenStylesheet
  status_bar :light
  attr_accessor :message_to_user

  def on_init
    init_tab_bar
  end
  def on_load
    init_nav
    self.edgesForExtendedLayout = UIRectEdgeBottom
    table_view.addPullToRefreshWithActionHandler -> { reload_data }
    table_view.addInfiniteScrollingWithActionHandler -> { load_more }
    @page = 1
    @data = []

    get_following
  end

  def table_data
    [{
      cells: @data.map do |data|
        {
          title: "#{data[:name].empty?? data[:email]:data[:name]}",
          # subtitle: "#{data[:estimates_count]} Estimates,  #{(data[:accurate]*100).round(2)}% Accuracy",
          # search_text: "",
          action: :open_profile_screen,
          arguments: { user: data },
          height: 50,
          # remote_image: {  # remote image, requires JMImageCache CocoaPod
          #   url: listing[:photos].first.to_s,
          #   placeholder: "icon-512", # NOTE: this is required!
          #   size: 50,
          #   radius: 0,
          #   content_mode: :scale_aspect_fill
          # }
        }
      end
    }]
  end

  def will_appear
    find(self.navigationItem.titleView).off(:value_changed)
    find(self.navigationItem.titleView).on(:value_changed) do |sender|
      case sender.selectedSegmentIndex
      when 1
        get_followers
      else
        get_following
      end
    end
  end

  def get_followers
    @url = "users/followers"
    reload_data
  end
  def get_following
    @url = "users/following"
    reload_data
  end

  def open_profile_screen(item)
    if @message_to_user
      close_modal_screen(to_user: item[:user])
    else
      open ProfileScreen.new(nav_bar: true, user: item[:user]), hide_tab_bar: true
    end
  end

  def load_data
    Api.get @url, {page: @page} do |result, error|
      if result
        p result
        if @page == 1
          @data = result
        else
          @data += result
        end
        update_table_data
        table_view.pullToRefreshView.stopAnimating
        table_view.infiniteScrollingView.stopAnimating
      end
      # if error
      #   App.alert(error)
      # end
    end
  end

  def reload_data
    @page = 1
    load_data
  end

  def load_more
    @page += 1
    load_data
  end

  def get_money(money)
    nf = NSNumberFormatter.alloc.init
    nf.setNumberStyle(NSNumberFormatterCurrencyStyle)
    nf.setMaximumFractionDigits(0)
    nf.stringFromNumber(money)
  end

  def init_tab_bar
    tab_bar_icon_selected   = FAKIonIcons.iosPeopleIconWithSize(30)
    tab_bar_icon_unselected = FAKIonIcons.iosPeopleOutlineIconWithSize(30)
    tab_bar_icon_selected.addAttribute(NSForegroundColorAttributeName, value:rmq.color.tint)
    tab_bar_icon_unselected.addAttribute(NSForegroundColorAttributeName, value:rmq.color.gray)
    set_tab_bar_item item: { selected: tab_bar_icon_selected.imageWithSize(CGSizeMake(30, 30)), unselected: tab_bar_icon_unselected.imageWithSize(CGSizeMake(30, 30)) }, title: "My Friends"
  end

  def init_nav
    self.navigationItem.titleView.segmentedControlStyle = UISegmentedControlStyleBordered
    self.navigationItem.titleView.sizeToFit
    self.navigationItem.titleView.selectedSegmentIndex = 0

    toggleOutlineIcon = FAKIonIcons.iosToggleOutlineIconWithSize(25)
    # naviconRoundIcon = FAKIonIcons.naviconRoundIconWithSize(25)
    if modal?
      set_nav_bar_button :right, title: "Cancel", action: :close_screen
    else
      set_nav_bar_button :left,  image: toggleOutlineIcon.imageWithSize(CGSizeMake(25, 25)), action: :nav_left_button
    end
    # set_nav_bar_button :right, image: naviconRoundIcon.imageWithSize(CGSizeMake(25, 25)),  action: :nav_right_button
    # self.navigationItem.rightBarButtonItem.enabled = false
  end

  def nav_left_button
    app_delegate.menu.toggle_left
  end

  def close_screen
    close_modal_screen
  end

  # Remove the following if you're only using portrait

  # You don't have to reapply styles to all UIViews, if you want to optimize, another way to do it
  # is tag the views you need to restyle in your stylesheet, then only reapply the tagged views, like so:
  #   def logo(st)
  #     st.frame = {t: 10, w: 200, h: 96}
  #     st.centered = :horizontal
  #     st.image = image.resource('logo')
  #     st.tag(:reapply_style)
  #   end
  #
  # Then in will_animate_rotate
  #   find(:reapply_style).reapply_styles#
  def will_animate_rotate(orientation, duration)
    find.all.reapply_styles
  end
end
