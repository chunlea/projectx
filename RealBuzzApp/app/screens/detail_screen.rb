class DetailScreen < PM::FormScreen
  title "Listing"
  stylesheet DetailScreenStylesheet
  status_bar :light
  attr_accessor :listing
  include MotionSocial::Sharing

  def on_load
    init_nav
    reload_data

    p @listing
    if @listing[:estimates]
      if @listing[:estimates].count == 0
        @estimate = "No Estimates Yet"
      else
        # added = @listing[:estimates].inject{|sum,x| sum + x }
        added = @listing[:estimates].map{|item| item["price"].to_f}.inject{|sum,x| sum + x }
        count = @listing[:estimates].count
        @estimate = "#{get_money(added / count)}"
      end
    end

    if calculate_distance(App::Persistence['userlat'], App::Persistence['userlon'], @listing[:lat], @listing[:lon]) < 350
      set_toolbar_items [{
          system_item: :flexible_space
        }, {
          title: "Check In and Estimate",
          action: :check_in
        }, {
          system_item: :flexible_space
        }]
    end

  end

  def calculate_distance(userlat, userlon, lat, lon)
    userLocation = CLLocation.alloc.initWithLatitude(userlat, longitude:userlon)
    placeLocation = CLLocation.alloc.initWithLatitude(lat, longitude:lon)
    meters = userLocation.distanceFromLocation(placeLocation)
  end

  def check_in
    open_modal EstimateScreen.new(nav_bar: true, listing: @listing)
  end

  def will_appear
    if calculate_distance(App::Persistence['userlat'], App::Persistence['userlon'], @listing[:lat], @listing[:lon]) < 350
      self.navigationController.setToolbarHidden(false, animated:true)
    end
    self.tableView.setContentInset UIEdgeInsetsMake(-32, 0.0, 0.0, 0.0)
  end
  def will_disappear
    self.navigationController.setToolbarHidden(true, animated:true)
  end

  def form_data
    # user_rated = {
    #   title: "La La La Previous Reviews",
    #   # footer: "",
    #   cells: [{
    #     name: :comments,
    #     title: "Previous Reviews (#{@listing[:comments_count]})",
    #     type: :button,
    #     "textLabel.color" => rmq.color.tint,
    #     action: "open_comments_screen",
    #   }]
    # }
    user_rated = {}
    unless @listing[:previous_reviews].empty?
      user_rated = {
        title: "Your Previous Reviews",
        cells: [{
          name: "location_rating",
          title: "Location",
          "textField.enabled" => false,
          cell_class: RatingCell,
          value: @listing[:previous_reviews].first.fetch(:location_rating, nil).to_s
        }, {
          name: "condition_rating",
          title: "Condition",
          "textField.enabled" => false,
          cell_class: RatingCell,
          value: @listing[:previous_reviews].first.fetch(:condition_rating, nil).to_s
        }, {
          name: "price",
          title: "Estimate Price(US$)",
          type: :float,
          value: @listing[:previous_reviews].first.fetch(:price, nil).to_s
        }, {
          name: "reason",
          title: "Estimate Reason",
          type: :longtext,
          value: @listing[:previous_reviews].first.fetch(:reason, nil).to_s
        }]
      }
    end
    [{
      # title: "Finish",
      # footer: "",
      cells: [{
        name: "photos",
        title: "Photos",
        # value: [rmq.image.resource("icon-512"),rmq.image.resource("icon"),rmq.image.resource("icon"),rmq.image.resource("icon")],
        value: get_photos(@listing[:photos]),
        cell_class: PagedImagesCell,
      }, {
        name: "house_address",
        title: "Address",
        value: @listing[:street],
        cell_class: HouseAddressCell,
      }, {
        title: "Price",
        # value: "Test Custom Class",
        cell_class: HousePriceCell,
        value: get_money(@listing[:price])
      }, {
        title: "Estimated Value",
        # value: "Test Custom Class",
        value: "#{@estimate}"
      }, {
        name: "location_rating",
        title: "Location",
        "textField.enabled" => false,
        "accessoryView.userInteractionEnabled" => false,
        value: @listing[:location_rating].to_s,
        cell_class: RatingCell,
      }, {
        name: "condition_rating",
        title: "Condition",
        "textField.enabled" => false,
        "accessoryView.userInteractionEnabled" => false,
        value: @listing[:condition_rating].to_s,
        cell_class: RatingCell,
      }, {
        title: "House Info",
        # value: "Test Custom Class",
        cell_class: HouseInfoCell,
        value: {beds: @listing[:bedrooms], baths: @listing[:baths], acres: @listing[:acres]}
      }]
    }, {
      # title: "",
      # footer: "",
      cells: [{
        name: "city",
        title: "City",
        "textField.enabled" => false,
        value: @listing[:city]
        }, {
        name: "state",
        title: "State",
        "textField.enabled" => false,
        value: @listing[:state]
        }, {
        name: "days_on_market",
        title: "Days on Market",
        type: :number,
        "textField.enabled" => false,
        value: get_days_from_now(@listing[:list_on_market])
      }, {
        name: "last_sold_price",
        title: "Last Sold Price",
        type: :email,
        "textField.enabled" => false,
        value: get_money(@listing[:last_sold_price])
      }, {
        name: "year_built",
        title: "Year Built",
        type: :email,
        "textField.enabled" => false,
        value: @listing[:year_built].to_s
      }]#, {
      #   name: "recorded_mortgage",
      #   title: "Recorded Mortgage",
      #   type: :email,
      #   "textField.enabled" => false,
      #   value: get_money(@listing[:recorded_mortgage]),
      #   enabled: false
      # }, {
      #   name: "assesed_value",
      #   title: "Assesed Value",
      #   type: :email,
      #   "textField.enabled" => false,
      #   value: get_money(@listing[:assesed_value])
      # }, {
      #   name: "current_taxes",
      #   title: "Current Taxes",
      #   type: :unsigned,
      #   "textField.enabled" => false,
      #   value: get_money(@listing[:current_taxes])
      # }]
    }, user_rated , {
      title: "",
      # footer: "",
      cells: [{
        name: :reviews,
        title: "Remarks",
        type: :longtext,
        value: @listing[:remarks].to_s,
      },
      {
        name: :facebook,
        title: "Share on Facebook",
        type: :button,
        #"textLabelcolor" => rmq.color.tint,
        action: "post_to_facebook"
        }, {
        name: :twitter,
        title: "Share on Twitter",
        type: :button,
        #textLabelcolor" => rmq.color.tint,
        action: "post_to_twitter"
          }]
    }, {
      title: "Contact The Seller",
      # footer: "",
      cells: [{
        name: :contact,
        title: "Contact The Seller or Agent",
        type: :button,
        "textLabel.color" => rmq.color.tint,
        action: "open_contacts_screen",
      }, {
        name: :offer,
        title: "Make an Offer",
        type: :button,
        "textLabel.color" => rmq.color.tint,
        action: "open_offer_screen"
        }]
    }]
  end
  def get_photos(photos)
    if photos.empty?
      [rmq.image.resource("icon-512")]
    else
      photos.map { |photo|
        image = rmq.image.resource("icon-512")
        JMImageCache.sharedCache.imageForURL(photo.to_s.to_url , completionBlock: lambda do |downloadedImage|
          image = downloadedImage
        end)
        image
      }
    end
  end

  def open_contacts_screen
    if @listing[:user]
      open_modal ContactsScreen.new(nav_bar: true, listing: @listing)
    else
      if @listing[:agent_phone]
        BW::UIAlertView.new({
          title: 'Call Agent',
          message: "Would like to call #{@listing[:agent_phone]} to get in touch with the agent?",
          buttons: ['Not now', 'Call now'],
          cancel_button_index: 0
        }) do |alert|
          unless alert.clicked_button.cancel?
            UIApplication.sharedApplication.openURL(NSURL.URLWithString("telprompt://#{@listing[:agent_phone]}"))
          end
        end.show
      else
        App.alert("Sorry!", {cancel_button_title: "Got it!", message: "This listing doesn't have contact info."})
      end
    end
  end

  def get_days_from_now(date)
    date_formatter = NSDateFormatter.alloc.init
    date_formatter.dateFormat = "yyyy-MM-dd"
    sec = date_formatter.dateFromString(date).timeIntervalSinceNow
    (sec.to_i*(-1)/(3600*24)).to_s
  end

  def get_money(money)
    nf = NSNumberFormatter.alloc.init
    nf.setNumberStyle(NSNumberFormatterCurrencyStyle)
    nf.setMaximumFractionDigits(0)
    nf.stringFromNumber(money)
  end

  def init_nav
    starIcon        = FAKIonIcons.iosStarIconWithSize(25)
    starOutlineIcon = FAKIonIcons.iosStarOutlineIconWithSize(25)
    p @listing[:favorited]
    if @listing[:favorited]
      set_nav_bar_button :right, image: starIcon.imageWithSize(CGSizeMake(25, 25)),  action: :nav_right_button
    else
      set_nav_bar_button :right, image: starOutlineIcon.imageWithSize(CGSizeMake(25, 25)),  action: :nav_right_button
    end
  end

  def nav_right_button
    starIcon        = FAKIonIcons.iosStarIconWithSize(25)
    starOutlineIcon = FAKIonIcons.iosStarOutlineIconWithSize(25)

    Api.get "listings/#{listing[:id]}/favorite" do |result, error|
      if result
        @listing[:favorited] = result[:status]
        if @listing[:favorited]
          set_nav_bar_button :right, image: starIcon.imageWithSize(CGSizeMake(25, 25)),  action: :nav_right_button
        else
          set_nav_bar_button :right, image: starOutlineIcon.imageWithSize(CGSizeMake(25, 25)),  action: :nav_right_button
        end
      end
      if error
        App.alert(error)
      end
    end
  end

  def on_return(args={})
    if args[:updated]
      reload_data
    end
  end

  def reload_data
    # A bug with custom cell, so just give up this function for now.
    # Api.get "listings/#{listing[:id]}" do |result, error|
    #   if result
    #     @listing = result
    #     update_form_data
    #     find.all.reapply_styles
    #   end
    # end
  end

  def open_offer_screen
    if @listing[:user]
      open_modal NewOfferScreen.new(nav_bar: true, listing: @listing)
    else
      App.alert("No Contact Info", {cancel_button_title: "Got it!", message: "We don't have a contact email for this seller or listing agent. Maybe you should let them know about ProjectX!"})
    end
  end

  def sharing_message
    "Check out #{@listing[:address]} on the ProjectX app! #ProjectX"
  end

  def sharing_url
    "https://itunes.apple.com/us/app/ProjectX/id968479855?mt=8"
  end

  def sharing_image
    nil
  end

  def controller
    self # This is so that we can present the dialogs.
  end

  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
