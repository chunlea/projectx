class ExtraZipScreen < PM::FormScreen
  include PM::IAP

  title "Your Extra Zip Code"
  stylesheet ExtraZipScreenStylesheet
  status_bar :light
  attr_accessor :editable, :zip

  def on_load
    @zip ||= {}
    @products = []

    SVProgressHUD.showWithStatus("Fetching the products...")
    Api.get "products", {} do |result, error|
      if result
        p result
        retrieve_iaps result[:products] do |products, error|
          p products
          @products = products
          update_form_data
          SVProgressHUD.showSuccessWithStatus("Fetched")
        end
      end
      if error
        SVProgressHUD.showErrorWithStatus("")
        App.alert(error)
      end
    end
  end
  def form_data
    [{
      title: "Your Extra Zip Code",
      footer: "#{@zip.fetch("expired_at", "")}",
      cells: [{
        name: "zip",
        title: "",
        type: :number,
        value: @zip.fetch("zip_code", ""),
        properties: {
          "textField.textAlignment" => NSTextAlignmentCenter,
          "textField.enabled" => @editable? false:true,
          # "accessoryType" => UITableViewCellAccessoryDisclosureIndicator,
          # "textLabel.al" => rmq.color.tint,
          # "accessoryView" => CustomAccessory.new,
          # "backgroundColor"      => color.red,
          # "detailTextLabel.font" => UIFont.fontWithName("MyFont", size:20),
        }
      }]
    }, {
      title: "",
      footer: "",
      cells: @products.map do |product|
        {
          name: "#{product[:product_id]}",
          title: "#{@editable? "Extend":"Buy New"} #{product[:title]} (#{product[:formatted_price]})               ",
          type: :button,
          value: product[:product_id].to_s,
          action: "process_iap:",
          "textLabel.color" => rmq.color.tint,
        }
      end
    }]
  end

  def process_iap(cell)
    product_id = cell.field.value
    # # p @editable
    zip_code = render_form[:zip]
    # p zip_code

    if zip_code.to_s.empty?
      App.alert("The Zip Code can't be empty!")
      return
    end

    purchase_iaps [ product_id.to_s ], username: Auth.id.to_s do |status, transaction|
      case status
      when :in_progress
        SVProgressHUD.showWithStatus("Buying...")
        # Usually do nothing, maybe a spinner
      when :deferred
        SVProgressHUD.showWithStatus("Buying...")
        # Waiting on a prompt to the user
      when :purchased
        SVProgressHUD.showSuccessWithStatus("Purchased!")
        # Pass product_id to the Backend, to set different type.
        if @editable
          SVProgressHUD.showWithStatus("Extending your extra zip code...")
          Api.put "extra_zips/#{@zip.fetch("id", "")}", {product: transaction[:product_id]} do |result, error|
            if result
              close({})
              SVProgressHUD.showSuccessWithStatus("Extra Zip code extend successful!")
            end
            if error
              SVProgressHUD.showErrorWithStatus("")
              App.alert(error)
            end
          end
        else
          # "Add New"
          SVProgressHUD.showWithStatus("Adding your extra zip code...")
          Api.post "extra_zips", {zip_code: zip_code, product: transaction[:product_id]} do |result, error|
            if result
              close
              SVProgressHUD.showSuccessWithStatus("Extra Zip code added successfully!")
            end
            if error
              SVProgressHUD.showErrorWithStatus("")
              App.alert(error)
            end
          end
        end
      when :canceled
        SVProgressHUD.showErrorWithStatus("Canceled!")
        # They just canceled, no big deal.
      when :error
        # Failed to purchase
        SVProgressHUD.showErrorWithStatus(transaction[:error].localizedDescription)
      end
    end
  end

  # Remove the following if you're only using portrait

  # You don't have to reapply styles to all UIViews, if you want to optimize, another way to do it
  # is tag the views you need to restyle in your stylesheet, then only reapply the tagged views, like so:
  #   def logo(st)
  #     st.frame = {t: 10, w: 200, h: 96}
  #     st.centered = :horizontal
  #     st.image = image.resource('logo')
  #     st.tag(:reapply_style)
  #   end
  #
  # Then in will_animate_rotate
  #   find(:reapply_style).reapply_styles#
  def will_animate_rotate(orientation, duration)
    reapply_styles
  end
end
