class EditListingScreen < PM::FormScreen
  title "Edit Listing"
  stylesheet NewListingScreenStylesheet
  status_bar :light
  attr_accessor :listing

  def on_init
    init_tab_bar
    p listing
  end
  def on_load
    init_nav
  end

  def form_data
    [{
      title: "Address",
      footer: "",
      cells: [{
        name: "address",
        title: "",
        type: :longtext,
        value: "#{listing[:address]}"
      }, {
        name: "zip",
        title: "Zipcode",
        type: :text,
        value: "#{listing[:zip]}"
      }]
    }, {
      title: "Lisitng Information",
      footer: "",
      cells: [{
        name: "status",
        title: "Status",
        type: :option,
        inline: true,
        options: ["Active"],
        value: "Active"
      }, {
        name: "for_sale",
        title: "For Sale?",
        type: :boolean,
        value: "1"
      }, {
        name: "listing_id",
        title: "Listing ID (Do not change this)",
        value: "#{listing[:id]}",
        type: :number
        }]
    }, {
      title: "Photos of this Listing",
      footer: "",
      cells: [{
         name: "photos",
         class: NSArray,
         sortable: true,
         inline: true,
         template: { type: :image },
         value: [],
       }]
    }, {
      title: "Listing Information",
      footer: "",
      cells: [{
        name: "fireplaces",
        title: "Fireplaces",
        type: :integer,
        cell: FXFormStepperCell,
        value: "#{listing[:fireplaces]}"
      }, {
        name: "bedrooms",
        title: "Bedrooms",
        type: :integer,
        cell: FXFormStepperCell,
        value: "#{listing[:bedrooms]}"
      }, {
        name: "optional_bedrooms",
        title: "Bonus Rooms",
        type: :integer,
        cell: FXFormStepperCell,
        value: "0"
      }, {
        name: "bathshalf",
        title: "Half Baths",
        type: :integer,
        cell: FXFormStepperCell,
        value: "0"
      }, {
        name: "baths",
        title: "Full Baths",
        type: :integer,
        cell: FXFormStepperCell,
        value: "#{listing[:baths]}"
      }]
    }, {
      title: "Listing",
      footer: "",
      cells: [{
        name: "acres",
        title: "Acres",
        type: :float,
        value: "#{listing[:acres]}"
      }, {
        name: "price",
        title: "Price (US$)",
        type: :float,
        value: "#{listing[:price]}"
      }]
    }, {
      title: "Other Information",
      footer: "",
      cells: [{
        name: "last_sold_price",
        title: "Last Sold Price (US$)",
        type: :float,
      }, {
        name: "year_built",
        title: "Year Built",
        type: :text,
        value: "#{listing[:year_built]}"
      }, {
        name: "recorded_mortgage",
        title: "Recorded Mortgage (US$)",
        type: :float,
      }, {
        name: "assesed_value",
        title: "Assesed Value (US$)",
        type: :float,
      }, {
        name: "current_taxes",
        title: "Current Taxes (US$)",
        type: :float,
      }, {
        name: "remarks",
        title: "Remarks",
        type: :longtext,
        value: "#{listing[:remarks]}"
      }]
    }, {
      title: "All extra fees",
      footer: "",
      cells: [{
        name: "home_owner_fees",
        title: "Home Owner Fees (US$)",
        type: :float,
      }, {
        name: "home_owner_total_fees",
        title: "Home Owner Assessments (US$)",
        type: :float,
      }, {
        name: "other_fees",
        title: "Other Fees (US$)",
        type: :float,
      }, {
        name: "mello_roos_fee",
        title: "Other Fees (US$)",
        type: :float,
      }, {
        name: "parkng_non_garaged_spaces",
        title: "Parkng Non Garaged Spaces",
        type: :float,
      }, {
        name: "estimated_square_feet",
        title: "Estimated Square Feet",
        type: :float,
      }, {
        name: "lot_sqft_approx",
        title: "Lot Sqft Approx",
        type: :float,
      }, {
        name: "garage",
        title: "Garage Spaces (number)",
        type: :float,
      }, {
        name: "parking_spaces_total",
        title: "Parking Spaces Total",
        type: :float,
      }, {
        name: "monthly_total_fees",
        title: "Monthly Total Fees (US$)",
        type: :float,
      }]
    }, {
      title: "",
      footer: "",
      cells: [{
        name: :submit,
        title: "Save Listing",
        type: :button,
        "textLabel.color" => rmq.color.tint,
        "textLabel.textAlignment" => UITextAlignmentCenter,
        action: "add_new_listing"
      }]
    }, {
      title: "",
      footer: "",
      cells: []
    }]
  end

  def scaleImage(image)
    if image.size.width < 1080
      newimg = image
    else
      scaleBy = 1080.0/image.size.width
      size = CGSizeMake(image.size.width * scaleBy, image.size.height * scaleBy)

      UIGraphicsBeginImageContext(size)
      context = UIGraphicsGetCurrentContext()
      transform = CGAffineTransformIdentity

      transform = CGAffineTransformScale(transform, scaleBy, scaleBy)
      CGContextConcatCTM(context, transform)

      image.drawAtPoint(CGPointMake(0, 0))
      newimg = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
    end
    UIImageJPEGRepresentation(newimg, 0.8)
  end

  def add_new_listing
    data = render_form
    SVProgressHUD.show

    data[:listing_class] = get_listing_class(data[:listing_class])
    data[:listing_class_type] = get_listing_class_type(data[:listing_class_type])
    data[:status] = get_status(data[:status])
    data[:lat] = App::Persistence['userlat']
    data[:lon] = App::Persistence['userlon']

    photos = data[:photos]

    if data[:for_sale]=="1" or data[:type].boolValue == true
      data[:for_sale] = true
    else
      data[:for_sale] = false
    end

    Api.post "listings", {listing: data} do |result, error|
      if result
        listing_id = result[:id]
        photos.each do |photo|
          photo_data = scaleImage(photo)
          AFMotion::SessionClient.shared.multipart_post("listings/#{listing_id}/upload") do |result, form_data|
            if form_data
              if photo_data
                form_data.appendPartWithFileData(photo_data, name: "photo", fileName:"photo.jpg", mimeType: "image/jpeg")
              end
            elsif result.success?
              p result
            else
              p "Upload Error"
            end
          end
        end
        SVProgressHUD.showSuccessWithStatus("")
        App.alert("Your listing has been added to our database successfully.")
        app_delegate.open_main_screens
      end
      if error
        SVProgressHUD.showErrorWithStatus("")
        App.alert(error)
      end
    end
  end

  def get_listing_class(value)
    val = ""
    case value
    when "2 4 Units"
      val = "2_4_units"
    when "Lot Land"
      val = "lot_land"
    when "Residential Rental"
      val = "residential_rental"
    when "Fractional Ownership"
      val = "fractional_ownership"
    when "Commercial Res Income"
      val = "commercial_res_income"
    when "Commercial Hotel Motel"
      val = "commercial_hotel_motel"
    when "Commercial Mobhmpark"
      val = "commercial_mobhmpark"
    when "Commercial Off Rtl Ind"
      val = "commercial_off_rtl_ind"
    when "Commercial Busop"
      val = "commercial_busop"
    when "Commercial Lnd Rn Grv"
      val = "commercial_lnd_rn_grv"
    else
      val = "residential"
    end
    val
  end
  def get_listing_class_type(value)
    val = ""
    case value
    when "Detached"
      val = "detached"
    when "Manufactured Home"
      val = "manufactured_home"
    when "Modular Home"
      val = "modular_home"
    when "Mobile Home"
      val = "mobile_home"
    when "Rowhome"
      val = "rowhome"
    when "Townhome"
      val = "townhome"
    when "Twinhome"
      val = "twinhome"
    when "Lots Land"
      val = "lots_land"
    else
      val = "all_other_attached"
    end
    val
  end
  def get_status(value)
    val = ""
    case value
    when "Sold"
      val = "sold"
    when "Pending"
      val = "pending"
    when "Expired"
      val = "expired"
    when "Off Market"
      val = "off_market"
    when "Rented"
      val = "rented"
    else
      val = "active"
    end
    val
  end

  def init_tab_bar
    tab_bar_icon_selected   = FAKIonIcons.iosPlusIconWithSize(30)
    tab_bar_icon_unselected = FAKIonIcons.iosPlusOutlineIconWithSize(30)
    tab_bar_icon_selected.addAttribute(NSForegroundColorAttributeName, value:rmq.color.tint)
    tab_bar_icon_unselected.addAttribute(NSForegroundColorAttributeName, value:rmq.color.gray)
    set_tab_bar_item item: { selected: tab_bar_icon_selected.imageWithSize(CGSizeMake(30, 30)), unselected: tab_bar_icon_unselected.imageWithSize(CGSizeMake(30, 30)) }, title: "Add New Listing"
  end

  def init_nav
    toggleOutlineIcon = FAKIonIcons.iosToggleOutlineIconWithSize(25)
    set_nav_bar_button :left,  image: toggleOutlineIcon.imageWithSize(CGSizeMake(25, 25)), action: :nav_left_button
  end

  def nav_left_button
    app_delegate.menu.toggle_left
  end

  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
