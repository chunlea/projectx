class ExtraZipCodesScreen < PM::TableScreen
  title "Extra Zip Codes"
  stylesheet ExtraZipCodesScreenStylesheet
  status_bar :light

  def on_load
    init_nav
    @page = 1
    @zips_available = []
    @zips_expired = []

    reload_data
  end
  def init_nav
    set_nav_bar_button :right, title: "Add", action: :nav_right_button
  end

  def table_data
    [
      {
        title: "Current Extra Zip Codes",
        cells: @zips_available.map do |zip|
          {
            title: zip.fetch("zip_code", ""),
            subtitle: "Will exprie on #{zip.fetch("expired_at", "")}",
            accessory_type: :disclosure_indicator,
            action: :edit_extra_zip,
            arguments: { zip: zip }
          }
        end
      }, {
        title: "Expired Extra Zip Codes",
        cells: @zips_expired.map do |zip|
          {
            title: zip.fetch("zip_code", ""),
            subtitle: "Expried on #{zip.fetch("expired_at", "")}",
            accessory_type: :disclosure_indicator,
            action: :edit_extra_zip,
            arguments: { zip: zip }
          }
        end
      }
    ]
  end

  def load_data
    SVProgressHUD.show
    Api.get "extra_zips" do |result, error|
      if result
        if @page == 1
          @zips_available = []
          @zips_expired = []
        end
        result.each do |zip|
          if zip[:expired]
            @zips_expired << zip
          else
            @zips_available << zip
          end
        end
        update_table_data
        SVProgressHUD.showSuccessWithStatus("")
      end
      if error
        SVProgressHUD.showErrorWithStatus("")
        App.alert(error)
      end
    end
  end

  def reload_data
    @page = 1
    load_data
  end

  def load_more
    @page += 1
    load_data
  end

  def edit_extra_zip(args={})
    open ExtraZipScreen.new(nav_bar: true, editable: true, zip: args[:zip])
  end

  def nav_right_button
    open ExtraZipScreen.new(nav_bar: true, editable: false)
  end

  def on_return(args={})
    reload_data
  end


  # Remove the following if you're only using portrait

  # You don't have to reapply styles to all UIViews, if you want to optimize, another way to do it
  # is tag the views you need to restyle in your stylesheet, then only reapply the tagged views, like so:
  #   def logo(st)
  #     st.frame = {t: 10, w: 200, h: 96}
  #     st.centered = :horizontal
  #     st.image = image.resource('logo')
  #     st.tag(:reapply_style)
  #   end
  #
  # Then in will_animate_rotate
  #   find(:reapply_style).reapply_styles#
  def will_animate_rotate(orientation, duration)
    reapply_styles
  end
end
