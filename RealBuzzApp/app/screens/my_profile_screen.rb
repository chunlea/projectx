class MyProfileScreen < PM::FormScreen
  stylesheet ProfileScreenStylesheet
  status_bar :light
  attr_accessor :user

  def on_load
    init_nav
    if @user
      self.title = @user[:name]
    else
      @user = {id: Auth.id, name: Auth.name}
      self.title = @user[:name]
    end

    reload_data

    # @username         = append!(UILabel, :username_label)
    # @username.text    = Auth.name
    # #@info             = append!(UILabel, :info_label)
    # #@info.text        = "#{Auth.estimates} Estimates,  #{(@user[:accurate]*100).round(2)}% Accuracy"
    # #@user_role        = append!(UILabel, :user_role_label)
    # #@user_role.text   = @user[:role].to_s.capitalize
    # append(UIButton, :message_btn).on(:touch) { message_to_user }
    # @follow_btn       = append!(UIButton, :follow_btn)
    # @follow_btn.on(:touch) { follow_user }
    # append(UILabel, :estimates_label)
    # @listing = UserEstimatesScreen.new(user: @user)
    # @listing.delegate = self
    # append(@listing.view, :listing_table)
    # Need::TODO
    # if @user[:followed]
    #   find(@follow_btn).apply_style(:unfollow_btn)
    # else
    #   find(@follow_btn).apply_style(:follow_btn)
    # end
  end

  def form_data
    [{
       title: "",
       footer: "",
       cells: [{
        name: "house_address",
        title: "Address",
        value: @user,
        cell_class: UserInfoCell,
      },{
        name: :points,
        title: "Buzz Points",
        type: :label,
        value: @user.fetch(:points, "").to_s,
       },{
        name: :followers,
        title: "Followers",
        type: :label,
        value: @user.fetch(:followers, "123").to_s,
       },{
        name: :following,
        title: "following",
        type: :label,
        value: @user.fetch(:followings, "123").to_s,
       }]
     },{
       title: "Bio",
       footer: "",
       cells: [{
        name: :bio,
        title: "",
        type: :longtext,
        value: @user.fetch(:bio, "").to_s,
       }]
     },{
       title: "Education",
       footer: "",
       cells: [{
        name: :dducation,
        title: "",
        type: :longtext,
        value: @user.fetch(:educations, "").to_s,
       }]
     },{
       title: "Designations",
       footer: "",
       cells: [{
        name: :designations,
        title: "",
        type: :label,
        value: @user.fetch(:designations, "").to_s,
       }]
     },{
       title: "",
       footer: "",
       cells: [{
          name: :contact,
          title: "View My LinkedIn Profile",
          type: :button,
          "textLabel.color" => rmq.color.tint,
          action: "open_linkedin_profile",
        }, {
          name: :offer,
          title: "View My Website",
          type: :button,
          "textLabel.color" => rmq.color.tint,
          action: "open_website"
        }]
     },{
       title: "",
       footer: "",
       cells: [{
          name: :contact,
          title: "View My Listings",
          type: :button,
          "textLabel.color" => rmq.color.tint,
          action: "open_my_listings_screen",
        }, {
          name: :offer,
          title: "Contact Me",
          type: :button,
          "textLabel.color" => rmq.color.tint,
          action: "message_to_user"
        }]
     }
   ]
  end

  def open_linkedin_profile
    UIApplication.sharedApplication.openURL(@user.fetch(:linked_in, "https://www.linkedin.com").to_s.to_url)
  end
  def open_website
    UIApplication.sharedApplication.openURL(@user.fetch(:website, "https://www.linkedin.com").to_s.to_url)
  end
  def open_my_listings_screen
    # open WebScreen.new(nav_bar: true, link: "#{BASE_HOST}users/password/new".to_url, screen_title: "Forget Password")
  end

  def reload_data
    Api.get "users/#{@user[:id]}" do |result, error|
      if result
        @user = result
        update_form_data
        find.all.reapply_styles
      end
    end
  end

  def message_to_user
    open_modal MessageToUserScreen.new(nav_bar: true, user: @user)
  end

  def follow_user
    Api.get "users/#{@user[:id]}/toggle_follow" do |result, error|
      if result
        if result[:followed]
          find(@follow_btn).apply_style(:unfollow_btn)
        else
          find(@follow_btn).apply_style(:follow_btn)
        end
      end
      if error
        App.alert(error)
      end
    end
  end

  def will_appear
    self.tableView.setContentInset UIEdgeInsetsMake(-32, 0.0, 0.0, 0.0)
  end

  def open_detail_screen(listing)
    open DetailScreen.new(nav_bar: true, listing: listing), hide_tab_bar: true
  end

  def init_nav
    toggleOutlineIcon = FAKIonIcons.iosToggleOutlineIconWithSize(25)
    # naviconRoundIcon = FAKIonIcons.naviconRoundIconWithSize(25)
    set_nav_bar_button :left,  image: toggleOutlineIcon.imageWithSize(CGSizeMake(25, 25)), action: :nav_left_button
    # set_nav_bar_button :right, image: naviconRoundIcon.imageWithSize(CGSizeMake(25, 25)),  action: :nav_right_button
    # self.navigationItem.rightBarButtonItem.enabled = false
  end

  def nav_left_button
    app_delegate.menu.toggle_left
  end

  def nav_right_button
    # open ListScreen.new(region: map.region)
  end


  # Remove the following if you're only using portrait

  # You don't have to reapply styles to all UIViews, if you want to optimize, another way to do it
  # is tag the views you need to restyle in your stylesheet, then only reapply the tagged views, like so:
  #   def logo(st)
  #     st.frame = {t: 10, w: 200, h: 96}
  #     st.centered = :horizontal
  #     st.image = image.resource('logo')
  #     st.tag(:reapply_style)
  #   end
  #
  # Then in will_animate_rotate
  #   find(:reapply_style).reapply_styles#
  def will_animate_rotate(orientation, duration)
    find.all.reapply_styles
  end
end
