class PreviousScreen < PM::TableScreen
  title "Previous Estimates"
  stylesheet PreviousScreenStylesheet
  status_bar :light
  searchable placeholder: "Search your previous estimate"

  def on_init
    init_tab_bar
  end
  def on_load
    init_nav
    self.edgesForExtendedLayout = UIRectEdgeBottom
    table_view.addPullToRefreshWithActionHandler -> { reload_data }
    table_view.addInfiniteScrollingWithActionHandler -> { load_more }
    @page = 1
    @data = []

    if AFNetworkReachabilityManager.sharedManager.reachable?
      reload_data
    end
  end

  def will_appear
    reload_data
  end

  def table_data
    [{
      cells: @data.map do |estimate|
        {
          title: "#{estimate[:listing][:address]}",
          subtitle: "Last Sold Price: #{get_money(estimate[:listing][:last_sold_price])}",
          search_text: "#{estimate[:listing][:address]}",
          action: :open_detail_screen,
          arguments: { listing: estimate[:listing] },
          height: 50,
          remote_image: {  # remote image, requires JMImageCache CocoaPod
            url: estimate[:listing][:photos].first,
            placeholder: "icon-512", # NOTE: this is required!
            size: 50,
            radius: 0,
            content_mode: :scale_aspect_fill
          }
        }
      end
    }]
  end
  def open_detail_screen(item)
    open DetailScreen.new(nav_bar: true, listing: item[:listing]), hide_tab_bar: true
  end
  def load_data
    Api.get "estimates/previous", {page: @page} do |result, error|
      if result
        if @page == 1
          @data = result
        else
          @data += result
        end
        update_table_data
        table_view.pullToRefreshView.stopAnimating
        table_view.infiniteScrollingView.stopAnimating
      end
      # if error
      #   App.alert(error)
      # end
    end
  end

  def reload_data
    @page = 1
    load_data
  end

  def load_more
    @page += 1
    load_data
  end

  def get_money(money)
    nf = NSNumberFormatter.alloc.init
    nf.setNumberStyle(NSNumberFormatterCurrencyStyle)
    nf.setMaximumFractionDigits(0)
    nf.stringFromNumber(money)
  end

  def init_tab_bar
    tab_bar_icon_selected   = FAKIonIcons.iosClockIconWithSize(30)
    tab_bar_icon_unselected = FAKIonIcons.iosClockOutlineIconWithSize(30)
    tab_bar_icon_selected.addAttribute(NSForegroundColorAttributeName, value:rmq.color.tint)
    tab_bar_icon_unselected.addAttribute(NSForegroundColorAttributeName, value:rmq.color.gray)
    set_tab_bar_item item: { selected: tab_bar_icon_selected.imageWithSize(CGSizeMake(30, 30)), unselected: tab_bar_icon_unselected.imageWithSize(CGSizeMake(30, 30)) }, title: "Previous"
  end

  def init_nav
    toggleOutlineIcon = FAKIonIcons.iosToggleOutlineIconWithSize(25)
    # naviconRoundIcon = FAKIonIcons.naviconRoundIconWithSize(25)
    set_nav_bar_button :left,  image: toggleOutlineIcon.imageWithSize(CGSizeMake(25, 25)), action: :nav_left_button
    # set_nav_bar_button :right, image: naviconRoundIcon.imageWithSize(CGSizeMake(25, 25)),  action: :nav_right_button
    # self.navigationItem.rightBarButtonItem.enabled = false
  end

  def nav_left_button
    app_delegate.menu.toggle_left
  end

  def nav_right_button
    # open ListScreen.new(region: map.region)
  end

  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
