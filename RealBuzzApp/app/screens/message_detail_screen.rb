class MessageDetailScreen < PM::FormScreen
  title "Read and reply message"
  stylesheet MessageDetailScreenStylesheet
  status_bar :light
  attr_accessor :message

  def on_load
    p message
  end

  def form_data
    [{
      title: "",
      footer: "",
      cells: [{
        name: "message",
        title: "Message sent to you",
        type: :longtext,
        value: @message[:body]
      }]
    },{
      title: "Reply this message",
      # footer: "Some help text",
      cells: [{
        name: "body",
        title: "Reply",
        type: :longtext,

      }, {
        name: :submit,
        title: "Send Reply",
        type: :button,
        "textLabel.color" => rmq.color.tint,
        "textLabel.textAlignment" => UITextAlignmentCenter,
        action: "send_message"
      }]
    }]
  end

  def send_message
    data = render_form
    data[:reply_id] = @message[:id]
    if data[:body].empty?
      App.alert("You can't reply with empty text.")
      return false
    end
    data[:listing_id] = @message[:listing_id]

    Api.post "messages", data do |result, error|
      if result
        App.alert("Your message has been sent successfully.")
        # close_modal_screen(updated: true)
        app_delegate.menu.center_controller = app_delegate.main_screens
      end
      if error
        App.alert(error)
      end
    end
  end

  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
