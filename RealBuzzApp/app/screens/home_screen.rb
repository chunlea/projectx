class HomeScreen < PM::MapScreen
  title "ProjectX"
  stylesheet HomeStylesheet
  status_bar :light

  def on_init

  end

  def on_load
    update_zip_code
    init_tab_bar
    init_nav

    @filter = nil
    @flag = true
    @flag2 = true

    set_toolbar_items [{
      system_item: :flexible_space
    }, {
      title: "Listing Filters",
      action: :open_filters,
      # image: FAKIonIcons.mapIconWithSize(40).addAttribute(NSForegroundColorAttributeName, value:rmq.color.tint).imageWithSize(CGSizeMake(40, 40))
      # custom_view:  create!(UIView, :logout_btn).tap do |q|
      #                 # q.append(UILabel, :hello_world)
      #               end
    }, {
      system_item: :flexible_space
    }]
  end

  def open_filters
    open_modal MapFilterScreen.new(nav_bar: true)
  end

  def left_image(images)
    image_view = UIImageView.alloc.initWithImage(rmq.image.resource("icon-60"))
    if images.first
      JMImageCache.sharedCache.imageForURL(NSURL.URLWithString(images.first), completionBlock: -> downloadedImage {
          image_view.image = downloadedImage
      })
    end
    image_view
  end

  def annotation_data
    @data ||= []
  end
  def open_detail_screen
    annotation = selected_annotations.first
    if annotation
      open DetailScreen.new(nav_bar: true, listing: annotation.params[:listing]), hide_tab_bar: true
    end
  end

  def will_appear
    if @flag2
      show_user_location
      @flag2 = false
      @flag = true
    end
  end
  def on_user_location(location)
    if @flag
      zoom_to_user(0.01, true)
      @flag = false
    end
  end
  def mapView(map_view, regionDidChangeAnimated:animated)
    if user_location
      App::Persistence['userlat'] = user_location.latitude
      App::Persistence['userlon'] = user_location.longitude
    else
      App::Persistence['userlat'] = 0.0
      App::Persistence['userlon'] = 0.0
    end
    load_async(map.region)
  end

  def load_async(region)
    if AFNetworkReachabilityManager.sharedManager.reachable?
      App::Persistence['lat'] = region.center.latitude
      App::Persistence['lon'] = region.center.longitude
      params = {lat: region.center.latitude, lon: region.center.longitude, radius: region.span.latitudeDelta/2*111}
      params[:filter] = @filter
      #SVProgressHUD.show
      if Auth.needlogin?
        #SVProgressHUD.dismiss
      else
        Api.get "listings/nearests", params do |result, error|
          #SVProgressHUD.dismiss
          if result
            @data = result.map do |listing|
              {
                listing: listing,
                longitude: listing[:lon], # REQUIRED
                latitude: listing[:lat], # REQUIRED
                title: get_money(listing[:price]), # REQUIRED
                subtitle: "#{listing[:bedrooms]} Beds #{listing[:baths]} Baths",
                # image: rmq.image.resource("icon"),
                left_accessory: left_image(listing[:photos]),
                # right_accessory: my_other_button,
                action: "open_detail_screen", # Overrides :right_accessory
                # action_button_type: UIButtonTypeDetailDisclosure # Defaults to UIButtonTypeDetailDisclosure
                pin_color: listing.fetch(:mls_num, nil).to_s.empty? ? (:green):(:red)
              }
            end
            if @data.length > 0
              self.navigationItem.rightBarButtonItem.enabled = true
            else
              self.navigationItem.rightBarButtonItem.enabled = false
            end
            update_annotation_data
          end
          # if error
          #   App.alert(error)
          # end
        end
      end
    end
  end

  def get_money(money)
    nf = NSNumberFormatter.alloc.init
    nf.setNumberStyle(NSNumberFormatterCurrencyStyle)
    nf.setMaximumFractionDigits(0)
    nf.stringFromNumber(money)
  end

  def init_tab_bar
    tab_bar_icon_selected   = FAKIonIcons.iosNavigateIconWithSize(30)
    tab_bar_icon_unselected = FAKIonIcons.iosNavigateOutlineIconWithSize(30)
    tab_bar_icon_selected.addAttribute(NSForegroundColorAttributeName, value:rmq.color.tint)
    tab_bar_icon_unselected.addAttribute(NSForegroundColorAttributeName, value:rmq.color.gray)
    set_tab_bar_item item: { selected: tab_bar_icon_selected.imageWithSize(CGSizeMake(30, 30)), unselected: tab_bar_icon_unselected.imageWithSize(CGSizeMake(30, 30)) }, title: "Nearby"
  end

  def init_nav
    toggleOutlineIcon = FAKIonIcons.iosToggleOutlineIconWithSize(25)
    naviconRoundIcon = FAKIonIcons.naviconRoundIconWithSize(25)
    set_nav_bar_button :left,  image: toggleOutlineIcon.imageWithSize(CGSizeMake(25, 25)), action: :nav_left_button
    set_nav_bar_button :right, image: naviconRoundIcon.imageWithSize(CGSizeMake(25, 25)),  action: :nav_right_button
    self.navigationItem.rightBarButtonItem.enabled = false
  end

  def nav_left_button
    app_delegate.menu.toggle_left
  end

  def nav_right_button
    open ListScreen.new(region: map.region)
  end

  def update_zip_code
    if Auth.zip_code == ""
      BW::UIAlertView.new(title: 'Update your zip code.', buttons: ['Later', 'Update'], cancel_button_index: 0,
        message: 'In order to use ProjectX, please input your zip code.', style: :plain_text_input ) do |alert|
        if alert.clicked_button.cancel?
          puts 'Canceled'
        else
          if alert.plain_text_field.text == ""
            App.alert("Please Input your zip code") do |alert|
              update_zip_code
            end
          else
            Api.post "users/#{Auth.id}/update_zip", {user: {zip_code: alert.plain_text_field.text}} do |result, error|
              if result
                Auth.update zip_code: result[:zip_code]
              end
              if error
                App.alert(error) do |alert|
                  update_zip_code
                end
              end
            end
          end
        end
      end.show
    end
  end

  def on_return(args={})
    @filter = args[:filter]
    load_async(map.region)
  end



  # You don't have to reapply styles to all UIViews, if you want to optimize,
  # another way to do it is tag the views you need to restyle in your stylesheet,
  # then only reapply the tagged views, like so:
  # def logo(st)
  #   st.frame = {t: 10, w: 200, h: 96}
  #   st.centered = :horizontal
  #   st.image = image.resource('logo')
  #   st.tag(:reapply_style)
  # end
  #
  # # Then in willAnimateRotationToInterfaceOrientation
  # find(:reapply_style).reapply_styles
  def willAnimateRotationToInterfaceOrientation(orientation, duration: duration)
    find.all.reapply_styles
  end
end
