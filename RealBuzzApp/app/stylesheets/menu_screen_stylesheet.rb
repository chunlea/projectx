class MenuScreenStylesheet < ApplicationStylesheet
  include CustomDetailFormCellStylesheet
    # Add your view stylesheets here. You can then override styles if needed,
  # example:
  #   # include FooStylesheet

  def setup
    # Add sytlesheet specific setup stuff here.
    # Add application specific setup stuff in application_stylesheet.rb
  end

  def root_view(st)
    st.background_color = color.from_hex('#FD8552')
  end

  def table_header(st)
    st.frame = {h: 200}
    st.background_color = color.clear
  end

  def username_label(st)
    st.frame          = {l: 10, top: 100, width: 160, height: 18}
    st.text_alignment = :center
    st.color          = color.white
    st.font           = font.medium
    st.text           = (Auth.name == "")? Auth.email : Auth.name
  end
  def user_profile_pic(st)
    st.frame = {l:50, t: 15, w: 70, h: 70}
    st.corner_radius = 35
  end
end
