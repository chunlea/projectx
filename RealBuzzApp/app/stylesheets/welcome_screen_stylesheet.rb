class WelcomeScreenStylesheet < ApplicationStylesheet
    # Add your view stylesheets here. You can then override styles if needed,
  # example:
  #   # include FooStylesheet

  def setup
    # Add sytlesheet specific setup stuff here.
    # Add application specific setup stuff in application_stylesheet.rb
  end

  def root_view(st)
    # st.background_color = color.white
  end

  def button_welcome_screen(st)
    standard_button st
    st.frame         = {w: (st.superview.size.width - 45)/2, fb:25, h: 40}
    st.corner_radius = 4
  end

  def sign_up_btn(st)
    button_welcome_screen st
    st.frame = {l:15}
    st.text  = "Sign Up"
    st.background_color = color.from_hex('#48A0DC')
  end

  def sign_in_btn(st)
    button_welcome_screen st
    st.frame = {fr:15}
    st.text  = "Sign In"
    st.background_color = color.from_hex('#88C057')
  end

  def fb_login_button(st)
    st.frame = { l: 15, fr: 15, fb: 80, centered: :both }
  end
end
