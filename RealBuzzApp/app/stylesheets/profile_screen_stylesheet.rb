class ProfileScreenStylesheet < ApplicationStylesheet
  # Add your view stylesheets here. You can then override styles if needed,
  # example: include FooStylesheet

  def setup
    # Add sytlesheet specific setup stuff here.
    # Add application specific setup stuff in application_stylesheet.rb
  end

  def root_view(st)
    st.background_color = color.white
  end

  def username_label(st)
    # standard_button st
    st.frame         = {l:15, w: st.superview.size.width - 30, h: 30, t: 20}
    st.corner_radius = 4
  end
  def info_label(st)
    st.frame         = {l:15, w: st.superview.size.width - 30, h: 30, t: 45}
    st.corner_radius = 4
    st.font = font.small
    st.color = color.gray
  end
  def user_role_label(st)
    st.frame         = {fr:15, w: 60, h: 40, t: 30}
    st.font = font.small
    st.color = color.tint
    st.corner_radius = 4
  end
  def message_btn(st)
    standard_button st
    st.frame         = {l: 15, w: (st.superview.size.width - 45)/2, h: 40, t: 80}
    st.corner_radius = 4
    st.text = "Message"
  end
  def follow_btn(st)
    standard_button st
    st.frame         = {fr: 15, w: (st.superview.size.width - 45)/2, h: 40, t: 80}
    st.corner_radius = 4
    st.text = "Follow"
  end
  def unfollow_btn(st)
    standard_button st
    st.frame         = {fr: 15, w: (st.superview.size.width - 45)/2, h: 40, t: 80}
    st.corner_radius = 4
    st.text = "Unfollow"
  end

  def estimates_label(st)
    st.frame         = {l: 0,fr: 0, h: 30, t: 130}
    st.text_alignment = :center
    st.color = color.white
    st.background_color = color.gray
    st.text = "Previous Estimates"
  end

  def listing_table(st)
    st.frame = {l:0, fr: 0, t: 170}
  end
end
