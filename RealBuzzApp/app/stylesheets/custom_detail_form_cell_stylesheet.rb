module CustomDetailFormCellStylesheet
  def clear_bg_cell(st)
    st.background_color    = color.clear
    st.view.selectionStyle = UITableViewCellSelectionStyleNone
  end
  def house_address_cell(st)
    st.frame               = {h:60, w: st.superview.size.width}
    st.view.selectionStyle = UITableViewCellSelectionStyleNone
  end
  def house_address(st)
    st.frame              = {w:(st.superview.size.width-30), h:60, t:5, l:15}
    st.text_alignment     = :center
    st.font               = UIFont.fontWithName("EuphemiaUCAS", size:26.0)
    st.color              = color.dark_gray
    st.view.lineBreakMode = UILineBreakModeWordWrap
    st.view.numberOfLines = 0
  end
  def house_price_cell(st)
    st.frame               = {h:70, w: st.superview.size.width}
    st.view.selectionStyle = UITableViewCellSelectionStyleNone
  end
  def house_price(st)
    st.frame              = {w:(st.superview.size.width-30), h:70, t: 5, l:15}
    st.text_alignment     = :center
    st.font               = UIFont.fontWithName("EuphemiaUCAS", size:26.0)
    st.color              = color.dark_gray
    st.view.lineBreakMode = UILineBreakModeWordWrap
    st.view.numberOfLines = 0
  end
  def house_info_cell(st)
    st.frame               = {h:80, w: st.superview.size.width}
    st.view.selectionStyle = UITableViewCellSelectionStyleNone
  end
  def house_info_cell_label(st)
    st.frame          = {w:(st.superview.size.width-30)/3, h:40, t:10}
    st.text_alignment = :center
    st.font           = UIFont.fontWithName("EuphemiaUCAS", size:16.0)
    st.color          = color.dark_gray
  end
  def house_info_beds(st)
    house_info_cell_label st
    st.frame = {l:20}
  end
  def house_info_baths(st)
    house_info_cell_label st
    st.frame = {l:20+st.frame.width}
  end
  def house_info_sqft(st)
    house_info_cell_label st
    st.frame = {l:20+st.frame.width*2}
  end
  def house_info_cell_title(st)
    st.frame          = {w:(st.superview.size.width-30)/3, h:20, t:50}
    st.text_alignment = :center
    st.font           = UIFont.fontWithName("EuphemiaUCAS", size:13.0)
    st.color          = color.light_gray
  end
  def house_info_beds_label(st)
    house_info_cell_title st
    st.frame = {l:20}
    st.text  = "Beds"
  end
  def house_info_baths_label(st)
    house_info_cell_title st
    st.frame = {l: 20 + st.frame.width}
    st.text  = "Baths"
  end
  def house_info_sqft_label(st)
    house_info_cell_title st
    st.frame = {l: 20 + st.frame.width*2}
    st.text  = "Acres"
  end
  def paged_images_cell(st)
    st.frame = {h:300}
  end
  def paged_images_cell_page_control(st)
    st.frame = {centered: :horizontal, t: 280}
    st.view.pageIndicatorTintColor        = color.light_gray
    st.view.currentPageIndicatorTintColor = color.gray
    st.view.currentPage                   = 0
  end
  def paged_images_cell_scroll_view(st)
    st.frame                               = {l: 0, t: 0, w: st.superview.size.width, h: 300}
    st.view.pagingEnabled                  = true
    st.view.delaysContentTouches           = true
    st.view.showsHorizontalScrollIndicator = false
    st.view.showsVerticalScrollIndicator   = false
  end
  # def facebook_login_button(st)
  #   clear_bg_cell st
  #   st.frame = {h:100}
  #   # st.view.separatorStyle = UITableViewCellSeparatorStyleNone
  #   # st.clips_to_bounds     = true
  #   # st.layer.cornerRadius  = 4
  #   # st.layer.masksToBounds = true
  # end

  # def fb_login_button(st)
  #   st.frame = { centered: :both }
  # end

  def user_info_cell(st)
    st.frame               = {h:165, w: st.superview.size.width}
    st.view.selectionStyle = UITableViewCellSelectionStyleNone
  end

  def user_info_cell_profile_pic(st)
    st.frame = {l:10, t: 10, w: 120, h: 120}
  end

  def user_info_cell_verfied(st)
    st.frame          = {w:120, h:30, t:135, l: 10}
    st.text_alignment = :center
    st.font           = UIFont.fontWithName("EuphemiaUCAS", size:16.0)
    st.color          = color.dark_gray
    # st.text           = "Hello"
  end
  def user_info_cell_checkins(st)
    st.frame          = {w:(st.superview.size.width-150), h:30, t:10, l:140}
    st.text_alignment = :left
    st.font           = UIFont.fontWithName("EuphemiaUCAS", size:16.0)
    # st.color          = color.dark_gray
    # st.text           = "Hello"
  end
  def user_info_cell_estimates(st)
    st.frame          = {w:(st.superview.size.width-150), h:30, t:40, l:140}
    st.text_alignment = :left
    st.font           = UIFont.fontWithName("EuphemiaUCAS", size:16.0)
    # st.color          = color.dark_gray
    # st.text           = "Hello"
  end
  def user_info_cell_accurate(st)
    st.frame          = {w:(st.superview.size.width-150), h:30, t:70, l:140}
    st.text_alignment = :left
    st.font           = UIFont.fontWithName("EuphemiaUCAS", size:16.0)
    # st.color          = color.dark_gray
    # st.text           = "Hello"
  end
  def user_info_cell_sold_listings(st)
    st.frame          = {w:(st.superview.size.width-150), h:30, t:100, l:140}
    st.text_alignment = :left
    st.font           = UIFont.fontWithName("EuphemiaUCAS", size:16.0)
    # st.color          = color.dark_gray
    # st.text           = "Hello"
  end
end
