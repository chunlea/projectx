class AppDelegate < PM::Delegate
  include CDQ
  include PM::DelegateNotifications

  attr_accessor :fb_loginview
  status_bar true, animation: :fade

  def on_load(app, options)
    cdq.setup
    Parse.setApplicationId("XXXXXX", clientKey:"XXXXXX")
    Api.init_server
    # URL Cache
    url_cache = NSURLCache.alloc.initWithMemoryCapacity(4 * 1024 * 1024, diskCapacity:20 * 1024 * 1024, diskPath:nil)
    NSURLCache.setSharedURLCache(url_cache)
    AFNetworkActivityIndicatorManager.sharedManager.enabled = true
    AFNetworkReachabilityManager.sharedManager.startMonitoring

    register_for_push_notifications :all
    PM.logger.info registered_push_notifications

    mixpanel_token = "XXXXXX"
    Mixpanel.sharedInstanceWithToken(mixpanel_token)
    mixpanel = Mixpanel.sharedInstance
    p mixpanel

    if Auth.needlogin?
      open WelcomeScreen.new
    else
      open_main_screens
    end
  end

  def fb_loginview
    @fb_loginview ||= FBLoginView.new
  end

  def on_unload
    unregister_for_push_notifications
  end

  def on_push_registration(token, error)
    NSLog("on_push_registration")
    PM.logger.info token.description
    App::Persistence['token'] = token
    NSLog("This is token %@", App::Persistence['token'])

    # App.alert(token)
  end

  def on_push_notification(notification, launched)
    # PM.logger.info notification.to_json
    # App.alert(notification.alert)
    case notification.custom["type"]
    when "listing"
      app_delegate.menu.center_controller = app_delegate.main_screens
      Api.get "listings/#{notification.custom["id"]}" do |result, error|
        if result
          app_delegate.menu.center_controller.selectedViewController.pushViewController DetailScreen.new(nav_bar: true, listing: result), animated: true
        end
        if error
          App.alert(error)
        end
      end
    when "request_listing"
      app_delegate.menu.center_controller = app_delegate.main_screens
      Api.get "request_listings/#{notification.custom["id"]}" do |result, error|
        if result
          app_delegate.menu.center_controller.selectedViewController.pushViewController RequestDetailScreen.new(nav_bar: true, listing: result), animated: true
        end
        if error
          App.alert(error)
        end
      end
    when "message"
      app_delegate.menu.center_controller = MessagesScreen.new(nav_bar: true)
      Api.get "messages/#{notification.custom["id"]}" do |result, error|
        if result
          Api.get "messages/#{result[:id]}/read" do |result, error|; end
          app_delegate.menu.center_controller.pushViewController MessageDetailScreen.new(nav_bar: true, message: result), animated: true
        end
        if error
          App.alert(error)
        end
      end
    else
      App.alert(notification.alert.to_s)
    end
  end

  def open_main_screens
    # PM::TabBarController.new HomeScreen.new(nav_bar: true), PreviousScreen.new(nav_bar: true), LeadersScreen.new(nav_bar: true), AddNewScreen.new(nav_bar: true)
    open_menu main_screens, left: MenuScreen.new(nav_bar: true, hide_nav_bar: true)
  end
  def main_screens
    PM::TabBarController.new HomeScreen.new(nav_bar: true), PreviousScreen.new(nav_bar: true), LeadersScreen.new(nav_bar: true), FriendsScreen.new(nav_bar: true), NewListingScreen.new(nav_bar: true)
  end

  def application(_, openURL: url, sourceApplication: sourceApplication, annotation: _)
    FBAppCall.handleOpenURL(url, sourceApplication: sourceApplication)
  end

  def applicationDidBecomeActive(application)
    FBSession.activeSession.handleDidBecomeActive
  end

  def applicationWillTerminate(application)
    FBSession.activeSession.close
  end

  def logout  #=> app_delegate.logout
    Auth.reset
    unregister_for_push_notifications
    open_root_screen WelcomeScreen.new
  end

  # Remove this if you are only supporting portrait
  def application(application, willChangeStatusBarOrientation: new_orientation, duration: duration)
    # Manually set RMQ's orientation before the device is actually oriented
    # So that we can do stuff like style views before the rotation begins
    rmq.device.orientation = new_orientation
  end
end
