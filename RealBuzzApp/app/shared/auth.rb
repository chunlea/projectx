# Try to use this to save auth to keychain
# Auth.set id: 1, email: "ichunlea@me.com", role: "user", token: "app-token"
class Auth
  class << self
    ATTRIBUTES = %w(id email token name zip_code role facebook_id confirmed).freeze
    # bio degree yr_graduated college other_school designations linked_in website profile_pic verified agent_id phone

    def set(arguments)
      reset
      arguments.each do |key, value|
        MotionKeychain.set(key.to_s, value.to_s)
      end
      Api.init_server
      if id
        NSLog("Set currentInstallation")
        currentInstallation = PFInstallation.currentInstallation
        currentInstallation.setDeviceTokenFromData(App::Persistence['token'])
        currentInstallation.setObject(Auth.id.to_i, forKey:"user_id")
        currentInstallation.saveInBackground
      end
    end

    def update(arguments)
      arguments.each do |key, value|
        MotionKeychain.set(key.to_s, value.to_s)
        Api.init_server if key.to_s == "token"
      end
    end

    def reset
      ATTRIBUTES.each do |attribute|
        MotionKeychain.remove(attribute)
      end
      if FBSession.activeSession.open?
        FBSession.activeSession.closeAndClearTokenInformation
      end
      Api.init_server
    end

    def user
      Hash[ATTRIBUTES.collect { |v| [v, MotionKeychain.get(v.to_s)] }]
    end

    def needlogin?
      return true if MotionKeychain.get('email').nil? or MotionKeychain.get('token').nil?
      false
    end

    ATTRIBUTES.each do |method|
      define_method method do |*args, &block|
        MotionKeychain.get(method)
      end
    end
  end
end
