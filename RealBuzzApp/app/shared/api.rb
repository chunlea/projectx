class Api
  class << self
    # Init API server, with the shared client
    def init_server
      if Auth.needlogin?
        AFMotion::SessionClient.build_shared(BASE_URL) do
          session_configuration :default
          header "Accept", "application/json"
          response_serializer :json
        end
      else
        AFMotion::SessionClient.build_shared(BASE_URL) do
          session_configuration :default
          header "Accept", "application/json"
          header "X-Api-Email",  Auth.email
          header "X-Api-Token",  Auth.token
          response_serializer :json
        end
      end
    end

    METHODS = %w(get post put delete patch).freeze
    # Api.get "apitest", {} do |result, error|
    #   if result; Do something here; end
    #   if error;  Do something here; end
    # end
    METHODS.each do |method|
      define_method method do |path, parameters=nil, *args, &block|
        # p "API:: #{method} #{path} #{AFNetworkReachabilityManager.sharedManager.reachable?}"
        if AFNetworkReachabilityManager.sharedManager.reachable?
          AFMotion::SessionClient.shared.send(method, path, parameters) do |result|
            if result.success?
              block.call(result.object, nil) if block
            else
              if result.task.response
                error = NSHTTPURLResponse.localizedStringForStatusCode(result.status_code).capitalize
                case result.status_code
                when 401
                  # Do some thing for this status code, like 401 to logout and clear auth token
                  App.alert("You session has expired, please login again.")
                  UIApplication.sharedApplication.delegate.logout
                # when 404
                #   # Do some thing for this status code, like 401 to logout and clear auth token
                #   error = "Not Found"
                when 400...500
                  if result.error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                    error_body = result.error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                    if error_body.to_s =~ /^\{/
                      error = (BW::JSON.parse error_body).fetch("errors", error)
                    end
                  end
                end
                block.call(nil, error) if block
              else
                block.call(nil, result.error.localizedDescription) if block
              end
            end
          end
        else
          SVProgressHUD.dismiss
          App.alert("Network not available right now, please try again later.")
        end
      end
    end

    def fblogin(parameters=nil, *args, &block)
      if AFNetworkReachabilityManager.sharedManager.reachable?
        AFMotion::SessionClient.shared.post("auth/fblogin", parameters) do |result|
          if result.success?
            block.call(result.object, nil) if block
          else
            if result.task.response
              error = NSHTTPURLResponse.localizedStringForStatusCode(result.status_code).capitalize
              case result.status_code
              # when 404
              #   # Do some thing for this status code, like 401 to logout and clear auth token
              #   error = "Not Found"
              when 400...500
                if result.error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                  error_body = result.error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                  if error_body.to_s =~ /^\{/
                    error = (BW::JSON.parse error_body).fetch("errors", error)
                  end
                end
              end
              block.call(nil, error) if block
            else
              block.call(nil, result.error.localizedDescription) if block
            end
          end
        end
      else
        SVProgressHUD.dismiss
        App.alert("Network not available right now, please try again later.")
      end
    end
  end
end
