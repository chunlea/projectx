schema "0001 initial" do

  # Examples:
  #
  # entity "Person" do
  #   string :name, optional: false
  #
  #   has_many :posts
  # end
  #
  # entity "Post" do
  #   string :title, optional: false
  #   string :body
  #
  #   datetime :created_at
  #   datetime :updated_at
  #
  #   has_many :replies, inverse: "Post.parent"
  #   belongs_to :parent, inverse: "Post.replies"
  #
  #   belongs_to :person
  # end

  entity "Profile" do
    string :bio
    string :degree
    integer32 :yr_graduated
    string :college
    string :other_school
    string :designations
    string :linked_in
    string :website
    string :profile_pic
    boolean :verified
    integer32 :user_id
    integer32 :agent_id
  end


end
