
  desc "Fetch data from glvar.com and update or create thos data."
  task fetch_glvar: :environment do
    @geocodio = Geocodio::Client.new("ProjectX")
    client = RETS::Client.login(:url => "https://rets-paragon.sandicor.com/xxxxxxx", :username => "ProjectX", :password => "ProjectX")
    ["1"].each do |property_class|
      client.search(:search_type => :Property, :class => property_class, :query => "(144=1-999999999)") do |data|
        listing = Listing.find_by_mls_num(data['163'])
        if listing
          @status = data['242']
          @sys_id = data['sysid']
          listing.update(
              mls_num:                   data['163'],
              listing_class:             1,
              listing_class_type:        1,
              address:                   "#{data['244']} #{data['243']} #{data['2386']} #{data['2909']} #{data['2963']}",
              zip:                       data['10'],
              bathshalf:                 data['62'].to_s.to_i,
              fireplaces:                data['113'].to_s.to_i,
              bedrooms:                  data['68'].to_s.to_i,
              baths:                     data['63'].to_s.to_i,
              acres:                     data['2'].to_s.to_d,
              price:                     data['144'].to_s.to_d,
              list_on_market:            data['138'],
              last_sold_price:           data['210'].to_s.to_d,
              year_built:                data['264'].to_s.to_i,
              remarks:                   "#{data['274']}",
              home_owner_fees:           data['39'].to_s.to_d,
              home_owner_total_fees:     data['39'].to_s.to_d,
              bathsfull:                 data['61'].to_s.to_d,
              estimated_square_feet:     data['237'].to_s.to_d,
              lot_sqft_approx:           data['154'].to_s.to_d,
              garage:                    data['122'].to_s.to_d,
              street:                    "#{data['244']} #{data['243']} #{data['2386']}",
              city:                      "#{data['2909']}",
              state:                     "#{data['2963']}",
              agent_id:                  "#{data['143']}",
              tour_link:                 "#{2139}",
              agent_phone:               "#{data['27']}", 
              mls_name:                  "glvar",
              photos: [], thumbnails: []
          )

            # status
            if @data == "Active-Exclusive Right"
              listing.update(status: 0, for_sale: true)
            elsif @data == "Closed"
              listing.update(status: 1, for_sale: false)
            elsif @data == "Contingent Offer" || @data == "Pending Offer"
              listing.update(status: 2)
            elsif @data == "Temporarily Off The Market" || @data == "Withdrawn Unconditional" || @data == "Withdrawn Conditional"
              listing.update(status: 4, for_sale: false)
            elsif @data == "Expired"
              listing.update(status: 3, for_sale: false)
            else
              listing.update(status: 4, for_sale: false)
            end

            if Profile.where(agent_id:listing.agent_id).any?
              profile = Profile.where(agent_id:listing.agent_id).last
              listing.update(user_id: profile.user.id)
            end


          if listing.save
            p "#{listing.mls_num}     [ UPDATED ]"
          else
            p "#{listing.mls_num}     [\e[31m UPDATED Failed \e[0m]"
          end
        else
          #if data['130'] == "Yes"
            @status = data['242']
            @sys_id = data['sysid']
            listing = Listing.create(
              sys_id:                    data['sysid'],
              mls_num:                   data['163'],
              listing_class:             1,
              listing_class_type:        1,
              address:                   "#{data['244']} #{data['243']} #{data['2386']} #{data['2909']} #{data['2963']}",
              zip:                       data['10'],
              bathshalf:                 data['62'].to_s.to_i,
              fireplaces:                data['113'].to_s.to_i,
              bedrooms:                  data['68'].to_s.to_i,
              baths:                     data['63'].to_s.to_i,
              acres:                     data['2'].to_s.to_d,
              price:                     data['144'].to_s.to_d,
              list_on_market:            data['138'],
              last_sold_price:           data['210'].to_s.to_d,
              year_built:                data['264'].to_s.to_i,
              remarks:                   "#{data['274']}",
              home_owner_fees:           data['39'].to_s.to_d,
              home_owner_total_fees:     data['39'].to_s.to_d,
              bathsfull:                 data['61'].to_s.to_d,
              estimated_square_feet:     data['237'].to_s.to_d,
              lot_sqft_approx:           data['154'].to_s.to_d,
              garage:                    data['122'].to_s.to_d,
              street:                    "#{data['244']} #{data['243']} #{data['2386']}",
              city:                      "#{data['2909']}",
              state:                     "#{data['2963']}",
              agent_id:                  "#{data['143']}",
              tour_link:                 "#{2139}",
              agent_phone:               "#{data['27']}", 
              mls_name:                  "glvar",
              photos: [], thumbnails: []
              )
            
            # status
            if @data == "Active-Exclusive Right"
              listing.update(status: 0, for_sale: true)
            elsif @data == "Closed"
              listing.update(status: 1, for_sale: false)
            elsif @data == "Contingent Offer" || @data == "Pending Offer"
              listing.update(status: 2)
            elsif @data == "Temporarily Off The Market" || @data == "Withdrawn Unconditional" || @data == "Withdrawn Conditional"
              listing.update(status: 4, for_sale: false)
            elsif @data == "Expired"
              listing.update(status: 3, for_sale: false)
            else
              listing.update(status: 4, for_sale: false)
            end

            # Claim listing for user
            if Profile.where(agent_id:listing.agent_id).any?
              profile = Profile.where(agent_id:listing.agent_id).last
              listing.update(user_id: profile.user.id)
            end

          #end
            begin 
              results = @geocodio.geocode(["#{listing.street}, #{listing.city}, #{listing.state}  #{listing.zip}"]).best
            raise "Geocode failure" if results.nil?
              lat =results.latitude # or address.lat
              lng= results.longitude 
              listing.update(lat: lat, lon: lng)
            rescue 
              p "could not goecode #{listing.street}"
            end
          # begin
          #   client.get_object(:resource => :Property, :type => "Photo", :location => false, :id => "#{@sys_id}") do |headers, content|
          #     fn = headers["content-id"]

          #     sio = StringIO.new(content)
          #     def sio.original_filename; "glvar_#{Time.now.to_i}.png"; end
          #     uploader = ListingImageUploader.new
          #     uploader.store!(sio)
          #     photo_url = uploader.url
          #     p photo_url
          #     listing.photos << photo_url
          #     listing.thumbnails << photo_url
          #     break
          #   end
          #     rescue RETS::APIError
          #        p "Could not find a picture for mls: #{listing.mls_num}, skipping this property"
          #       break
          #     end 
          
 

          if listing.save
            p "#{listing.mls_num}     [ CREATED ]"
          else
            p "#{listing.mls_num}     [\e[31m CREATED Failed \e[0m]"
          end
        end
        # sleep rand
      end
    end
  end


