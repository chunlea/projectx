desc "Fetch data from glvar.com and update or create thos data."
task get_glvar_pix: :environment do
  client = RETS::Client.login(:url => "https://rets-paragon.sandicor.com/xxxxxxx", :username => "ProjectX", :password => "ProjectX")

	Listing.where(mls_name:"glvar").each do |listing|
		if listing.photos.count == 0 && listing.created_at >= (Date.today -2)
			begin
            client.get_object(:resource => :Property, :type => "Photo", :location => false, :id => "#{listing.sys_id}") do |headers, content|
              fn = headers["content-id"]

              sio = StringIO.new(content)
              def sio.original_filename; "glvar_#{Time.now.to_i}.png"; end
              uploader = ListingImageUploader.new
              uploader.store!(sio)
              photo_url = uploader.url
              p photo_url
              listing.update({:photos => ["#{photo_url}"]})
              listing.update({:thumbnails => ["#{photo_url}"]})
              #listing.photos << photo_url
              #listing.thumbnails << photo_url
              break
            end
              rescue RETS::APIError
                 p "Could not find a picture for mls: #{listing.mls_num}, skipping this property"
                #break
              end 
        end
    end
end