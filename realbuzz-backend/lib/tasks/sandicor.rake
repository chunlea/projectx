namespace :sandicor do
  desc "Fetch data from sandicor.com and update or create thos data."
  task fetch: :environment do
    @geocodio = Geocodio::Client.new("ProjectX")
    client = RETS::Client.login(:url => "https://rets-paragon.sandicor.com/xxxxxxx", :username => "ProjectX", :password => "ProjectX")
    ["RE_1", "RI_2"].each do |property_class|
      client.search(:search_type => :Property, :class => property_class, :query => "(L_ListingID=0-9999999999)") do |data|
        listing = Listing.find_by_mls_num(data['L_ListingID'])
        if listing
          listing.update(
            listing_class:             data['L_Class'].parameterize.underscore.to_sym,
            listing_class_type:        data['L_Type_'].parameterize.underscore.to_sym,
            status:                    data['L_StatusCatID'].parameterize.underscore.to_sym,
            for_sale:                  data['L_SaleRent']=="For Sale",
            lat:                       data['LMD_MP_Latitude'].to_f,
            lon:                       data['LMD_MP_Longitude'].to_f,
            address:                   "#{data['L_Address']} #{data['L_Address2']} #{data['L_Area']} #{data['L_City']} #{data['L_State']}",
            zip:                       data['L_Zip'],
            bathshalf:                 data['LM_Int1_5'].to_s.to_i,
            fireplaces:                data['LM_Int1_8'].to_s.to_i,
            bedrooms:                  data['LM_Int1_1'].to_s.to_i,
            optional_bedrooms:         data['LM_Int1_2'].to_s.to_i,
            baths:                     data['LM_Int2_6'].to_s.to_i,
            acres:                     data['L_NumAcres'].to_s.to_d,
            price:                     data['L_AskingPrice'].to_s.to_d,
            list_on_market:            data['L_ListingDate'],
            last_sold_price:           data['L_SoldPrice'].to_s.to_d,
            year_built:                data['LM_Int2_1'].to_s.to_i,
            remarks:                   "#{data['LR_remarks11']}\n#{data['LR_remarks22']}\n#{data['LR_remarks33']}\n#{data['LR_remarks44']}\n#{data['LR_remarks55']}\n#{data['LR_remarks66']}\n#{data['LR_remarks1414']}\n#{data['LR_remarks1515']}\n#{data['LR_remarks1616']}\n",
            home_owner_fees:           data['LM_Dec_3'].to_s.to_d,
            home_owner_total_fees:     data['LM_Dec_4'].to_s.to_d,
            other_fees:                data['LM_Dec_5'].to_s.to_d,
            mello_roos_fee:            data['LM_Dec_6'].to_s.to_d,
            bathsfull:                 data['LM_Int2_3'].to_s.to_d,
            parkng_non_garaged_spaces: data['LM_Int2_4'].to_s.to_d,
            estimated_square_feet:     data['LM_Int4_1'].to_s.to_d,
            lot_sqft_approx:           data['LM_Int4_6'].to_s.to_d,
            garage:                    data['LM_Int4_7'].to_s.to_d,
            parking_spaces_total:      data['LM_Int4_8'].to_s.to_d,
            monthly_total_fees:        data['LM_Int4_16'].to_s.to_d,
            street:                    "#{data['L_AddressNumber']} #{data['L_AddressStreet']} #{data['L_AddressDirection']} #{data['L_Address2']}",
            city:                      "#{data['L_City']}",
            state:                     "#{data['L_State']}",
            agent_id:                  "#{data['LA1_AgentID']}",
            tour_link:                 "#{data['VT_VTourURL']}",
            agent_phone:               "#{data['LA1_PhoneNumber1']}", 
            mls_name:                  "sandicor",
            photos: [], thumbnails: []
          )

          client.get_object(:resource => :Property, :type => :Photo, :location => true, :id => "#{listing.mls_num}") do |headers, content|
            listing.photos << headers["location"]
            break
          end
          client.get_object(:resource => :Property, :type => :Photo, :location => true, :id => "#{listing.mls_num}") do |headers, content|
            listing.thumbnails << headers["location"]
            break
          end

          if listing.save
            p "#{listing.mls_num}     [ UPDATED ]"
          else
            p "#{listing.mls_num}     [\e[31m UPDATED Failed \e[0m]"
          end
        else
          listing = Listing.create(
            mls_num:                   data['L_ListingID'],
            listing_class:             data['L_Class'].parameterize.underscore.to_sym,
            listing_class_type:        data['L_Type_'].parameterize.underscore.to_sym,
            status:                    data['L_StatusCatID'].parameterize.underscore.to_sym,
            for_sale:                  data['L_SaleRent']=="For Sale",
            lat:                       data['LMD_MP_Latitude'].to_f,
            lon:                       data['LMD_MP_Longitude'].to_f,
            address:                   "#{data['L_Address']} #{data['L_Address2']} #{data['L_City']} #{data['L_State']}",
            zip:                       data['L_Zip'],
            bathshalf:                 data['LM_Int1_5'].to_s.to_i,
            fireplaces:                data['LM_Int1_8'].to_s.to_i,
            bedrooms:                  data['LM_Int1_1'].to_s.to_i,
            optional_bedrooms:         data['LM_Int1_2'].to_s.to_i,
            baths:                     data['LM_Int2_6'].to_s.to_i,
            acres:                     data['L_NumAcres'].to_s.to_d,
            price:                     data['L_AskingPrice'].to_s.to_d,
            list_on_market:            data['L_ListingDate'],
            last_sold_price:           data['L_SoldPrice'].to_s.to_d,
            year_built:                data['LM_Int2_1'].to_s.to_i,
            remarks:                   "#{data['LR_remarks11']}\n#{data['LR_remarks22']}\n#{data['LR_remarks33']}\n#{data['LR_remarks44']}\n#{data['LR_remarks55']}\n#{data['LR_remarks66']}\n#{data['LR_remarks1414']}\n#{data['LR_remarks1515']}\n#{data['LR_remarks1616']}\n",
            home_owner_fees:           data['LM_Dec_3'].to_s.to_d,
            home_owner_total_fees:     data['LM_Dec_4'].to_s.to_d,
            other_fees:                data['LM_Dec_5'].to_s.to_d,
            mello_roos_fee:            data['LM_Dec_6'].to_s.to_d,
            bathsfull:                 data['LM_Int2_3'].to_s.to_d,
            parkng_non_garaged_spaces: data['LM_Int2_4'].to_s.to_d,
            estimated_square_feet:     data['LM_Int4_1'].to_s.to_d,
            lot_sqft_approx:           data['LM_Int4_6'].to_s.to_d,
            garage:                    data['LM_Int4_7'].to_s.to_d,
            parking_spaces_total:      data['LM_Int4_8'].to_s.to_d,
            monthly_total_fees:        data['LM_Int4_16'].to_s.to_d,
            street:                    "#{data['L_AddressNumber']} #{data['L_AddressStreet']} #{data['L_AddressDirection']} #{data['L_Address2']}",
            city:                      "#{data['L_City']}",
            state:                     "#{data['L_State']}",
            agent_id:                  "#{data['LA1_AgentID']}",
            tour_link:                 "#{data['VT_VTourURL']}",
            agent_phone:               "#{data['LA1_PhoneNumber1']}",
            photos: [], thumbnails: []
          )

            begin 
              results = @geocodio.geocode(["#{listing.address}"]).best
            raise "Geocode failure" if results.nil?
              lat =results.latitude # or address.lat
              lng= results.longitude 
              listing.update(lat: lat, lon: lng)
            rescue 
              p "could not goecode #{listing.address}"
            end

          client.get_object(:resource => :Property, :type => :Photo, :location => true, :id => "#{listing.mls_num}") do |headers, content|
            listing.photos << headers["location"]
            break
          end
          client.get_object(:resource => :Property, :type => :Photo, :location => true, :id => "#{listing.mls_num}") do |headers, content|
            listing.thumbnails << headers["location"]
            break
          end
          if listing.save
            p "#{listing.mls_num}     [ CREATED ]"
          else
            p "#{listing.mls_num}     [\e[31m CREATED Failed \e[0m]"
          end
        end
        # sleep rand
      end
    end
  end
end
namespace :sandicor do
  desc "Fetch data from sandicor.com and update or create thos data."
  task count: :environment do
    sum = 0
    client = RETS::Client.login(:url => "https://rets-paragon.sandicor.com/rets/fnisrets.aspx/sandicor/login?rets-version=rets/1.7.2", :username => "500607", :password => "aMMrC9h=")
    ["RE_1", "RI_2", "LN_3", "RT_4", "TS_5", "RN_6", "HO_7", "MH_8", "OF_9", "BO_10", "LR_11"].each do |property_class|
      client.search(:search_type => :Property, :class => property_class, :query => "(L_ListingID=0-9999999999)") do |data|
        sum += 1
        p "#{data['L_ListingID']}     #{sum}  (#{data['LMD_MP_Latitude']}, #{data['LMD_MP_Longitude']})"
      end
    end
  end
end

desc "Update all listings agent's phone"
task update_agent_phone: :environment do
  client = RETS::Client.login(:url => "https://rets-paragon.sandicor.com/rets/fnisrets.aspx/sandicor/login?rets-version=rets/1.7.2", :username => "500607", :password => "aMMrC9h=")
  Listing.where(agent_phone: nil, user_id: nil).each do |listing|
    ["RE_1", "RI_2", "LN_3", "RT_4", "TS_5", "RN_6", "HO_7", "MH_8", "OF_9", "BO_10", "LR_11"].each do |property_class|
      client.search(:search_type => :Property, :class => property_class, :query => "(L_ListingID=#{listing.mls_num})") do |data|
        listing.update(agent_phone: "#{data['LA1_PhoneNumber1Area']}#{data['LA1_PhoneNumber1']}#{data['LA1_PhoneNumber1Ext']}")

        if listing.save
          p "#{listing.mls_num}     [ UPDATED ] #{listing.agent_phone}"
        else
          p "#{listing.mls_num}     [\e[31m UPDATED Failed \e[0m]"
        end
      end
    end
  end
end

