desc "Fetch more fields from Sandicor."
task add_city_to_sandicor: :environment do
	client = RETS::Client.login(:url => "https://rets-paragon.sandicor.com/xxxxxxx", :username => "ProjectX", :password => "ProjectX")
    ["RE_1", "RI_2"].each do |property_class|
      client.search(:search_type => :Property, :class => property_class, :query => "(L_ListingID=0-9999999999)") do |data|
        listing = Listing.find_by_mls_num(data['L_ListingID'])
        if listing
          listing.update(
	        street:                    "#{data['L_AddressNumber']} #{data['L_AddressStreet']} #{data['L_AddressDirection']} #{data['L_Address2']}",
	        city:                      "#{data['L_City']}",
	        state:                     "#{data['L_State']}",
	        agent_id:                  "#{data['LA1_AgentID']}",
	        tour_link:                 "#{data['VT_VTourURL']}",
	        agent_phone:               "#{data['LA1_PhoneNumber1']}", 
	        mls_name:                  "sandicor")
	      end
	    end
      end
end