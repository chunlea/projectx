ProjectX
========
ENV['S3_ACCESS_KEY'],
ENV['S3_SECRET_KEY'],
ENV['S3_REGION']
ENV['S3_BUCKET']
ENV['ASSET_HOST']

How to add In App Purchases
---------------------------
Go to domain.com/admin/products/new, add production id and extend_months.

The default admin user and password is:
User: admin@example.com
Password: password

<!-- You need to create the Product in the rails backend. Use this: `heroku run rails c` to open the rails console.
And use some thing like this to add Product to the backend database:
```ruby
Product.create(product_id: "zip_one_month", extend_months: 1)
```
 -->