Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  apipie
  devise_for :users
  post 'profiles' => 'api/v1/profiles#create'
  scope module: :api, path: '/api', defaults: { format: 'json' } do
    namespace :v1 do
      post 'auth/login'    => 'auth#authenticate'
      post 'auth/fblogin'  => 'auth#authenticate_facebook'
      post 'auth/register' => 'auth#register'

      get 'products' => 'base#products'

      resources :own_listings
      resources :offers

      #resources :profiles
      post 'profiles' => 'profiles#create'
      #post 'profile/:id/upload' => 'profiles#upload'

      resources :profiles do
        post 'upload'
      end

      resources :estimates, only: [] do
        get :previous, on: :collection, action: :index_current_user
        get :leaders,  on: :collection, action: :index_leaders
        get :leaders_by_amounts,  on: :collection, action: :index_leaders_by_amounts
      end
      resources :listings do
        get 'nearests',  on: :collection
        get 'favorites', on: :collection
        get 'favorite',  on: :member
        get 'owned'#, on: :member
        post 'upload',   on: :member
        post 'feature_pic1',   on: :member
        post 'feature_pic2',   on: :member
        post 'feature_pic3',   on: :member
        post 'feature_pic4',   on: :member
        post 'green_pic1',   on: :member
        post 'green_pic2',   on: :member
        post 'green_pic3',   on: :member
        post 'green_pic4',   on: :member

        resources :estimates #, shallow: true
        # get  'estimate' => 'listings#current_user_estimate', on: :member
        # post 'estimate' => 'listings#estimate', on: :member
        # put  'estimate' => 'listings#estimate', on: :member
        # get 'search',    on: :collection
        # get 'favorites', on: :collection
        # get 'favorite',  on: :member

        # post 'estimate', on: :member

        # resources :comments
      end
      resources :request_listings do
        post 'upload',   on: :member
      end
      # resources :estimates do
      #   get 'previous', on: :collection
      # end
      resources :messages do
        get :unread,   on: :collection, action: :index_unread
        get :archived, on: :collection, action: :index_archived
        get :read,     on: :member,     action: :mark_message_read
        get :archived, on: :member,     action: :mark_message_archived
      end

      resources :extra_zips, only: [:index, :create, :update]

      resources :users do
        post 'update_zip', on: :member
        get  'toggle_follow', on: :member

        get  'estimates', on: :member

        get 'followers', on: :collection
        get 'following', on: :collection
        get 'myself', on: :collection
      end
      root 'base#index'
    end
    # scope module: :v1, constraints: ApiConstraint.new(version: 1) do
    #   # match 'auth/login' => "auth#authenticate", :via => :post
    #   # namespace :events do
    #   #   resources :nearests, only: [:index]
    #   # end

    #   # resources :events, only: [:create, :show, :update] do
    #   #   resources :photos, only: [:create, :index, :show]
    #   # end
    #   # resources :users#, only: [:create, :delete]
    #   # resources :attendances, only: [:create]
    # end
  end

  root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
