class ZipCodeValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors.add attribute, (options[:message] || "#{ value.to_s } is not an US zip code") unless GoingPostal.postcode?(value.to_s, "US")
  end
end