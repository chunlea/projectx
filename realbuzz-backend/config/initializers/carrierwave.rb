require 'carrierwave'
require 'carrierwave/orm/activerecord'

CarrierWave.configure do |config|
  if Rails.env.development? || Rails.env.test?
    config.storage    = :file
    config.asset_host = ENV['ASSET_HOST']
  else
    config.storage = :fog

    config.fog_credentials = {
      :provider               => 'AWS',                        # required
      :aws_access_key_id      => ENV['S3_ACCESS_KEY'],     # your aws access key id
      :aws_secret_access_key  => ENV['S3_SECRET_KEY'], # your aws secret access key
      :region                 => ENV['S3_REGION']
      # :region               => ENV['S3_REGION']              # your bucket's region in S3, defaults to 'us-east-1'
    }
    # your S3 bucket name
    config.fog_directory    = ENV['S3_BUCKET']
    # custome your domain on aws S3, defaults to nil
    # config.fog_host       = 'https://s3-us-west-1.amazonaws.com/bucket_name'
    config.fog_public       = true                                     # optional, defaults to true
    # config.fog_attributes = {'Cache-Control'=>'max-age=315576000'}  # optional, defaults to {}
    # config.asset_host     = "https://dzd0dkfaaqyhs.cloudfront.net"
  end
end
