Apipie.configure do |config|
  config.app_name = "ProjectX API"
  config.copyright = "&copy; 2014 Developed by Chunlea Ju (ichunlea@me.com)"
  config.default_version = "v1"
  config.doc_base_url = "/api/doc"
  config.api_base_url = "/api/"

  # set to true to turn on/off the cache. To generate the cache use:
  #
  #     rake apipie:cache
  #
  config.use_cache = Rails.env.production?
  # config.cache_dir = File.join(Rails.root, "public", "apipie-cache") # optional

  # for reloading to work properly you need to specify where your api controllers are (like in Dir.glob):
  config.api_controllers_matcher = File.join(Rails.root, "app", "controllers", "api", "**","*.rb")
  config.markup = Apipie::Markup::Markdown.new
  config.app_info = <<-EOF
    # Hello, this is ProjectX API documentation
  EOF

  # show debug informations
  config.debug = false

  # set all parameters as required by default
  # if enabled, use param :name, val, :required => false for optional params
  config.required_by_default = false

  # use custom layout
  # use Apipie.include_stylesheets and Apipie.include_javascripts
  # to include apipies css and js

  # specify disqus site shortname to show discusion on each page
  # to show it in custom layout, use `render 'disqus' if Apipie.configuration.use_disqus?`
  # config.disqus_shortname = 'paveltest'

  # Hide '.html' extensions.  Note that '.html' extensions are forced
  # when generating static documentation
  # config.link_extension = ""
end
