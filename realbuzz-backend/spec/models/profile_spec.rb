# == Schema Information
#
# Table name: profiles
#
#  id           :integer          not null, primary key
#  bio          :text
#  degree       :string
#  yr_graduated :integer
#  college      :string
#  other_school :string
#  designations :string
#  linked_in    :string
#  website      :string
#  profile_pic  :string
#  verified     :boolean
#  agent_id     :integer
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  phone        :string
#

require 'rails_helper'

RSpec.describe Profile, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
