# == Schema Information
#
# Table name: device_tokens
#
#  id               :integer          not null, primary key
#  auth_token       :string           not null
#  user_id          :integer
#  device_id        :string
#  device_model     :string
#  device_name      :string
#  device_osname    :string
#  device_osversion :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_device_tokens_on_auth_token  (auth_token) UNIQUE
#  index_device_tokens_on_device_id   (device_id) UNIQUE
#  index_device_tokens_on_user_id     (user_id)
#

require 'rails_helper'

RSpec.describe DeviceToken, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
