# == Schema Information
#
# Table name: request_listings
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  zip          :string(20)
#  address      :string(4096)
#  type_of_home :string
#  photos       :text             default("{}"), is an Array
#  thumbnails   :text             default("{}"), is an Array
#  square       :decimal(, )
#  bedrooms     :integer          default("0")
#  bathrooms    :integer          default("0")
#  condition    :text
#  notes        :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_request_listings_on_user_id  (user_id)
#

require 'rails_helper'

RSpec.describe RequestListing, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
