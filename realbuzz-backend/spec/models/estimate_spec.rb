# == Schema Information
#
# Table name: estimates
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  listing_id       :integer
#  location_rating  :float
#  condition_rating :float
#  price            :decimal(, )
#  reason           :text
#  accurate         :float
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_estimates_on_listing_id  (listing_id)
#  index_estimates_on_user_id     (user_id)
#

require 'rails_helper'

RSpec.describe Estimate, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
