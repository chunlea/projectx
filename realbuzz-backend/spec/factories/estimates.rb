# == Schema Information
#
# Table name: estimates
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  listing_id       :integer
#  location_rating  :float
#  condition_rating :float
#  price            :decimal(, )
#  reason           :text
#  accurate         :float
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_estimates_on_listing_id  (listing_id)
#  index_estimates_on_user_id     (user_id)
#

FactoryGirl.define do
  factory :estimate do
    user nil
listing nil
location_rating 1.5
condition_rating 1.5
price "9.99"
reason "MyText"
accurate 1.5
  end

end
