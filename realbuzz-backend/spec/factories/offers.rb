# == Schema Information
#
# Table name: offers
#
#  id            :integer          not null, primary key
#  listing_id    :integer
#  user_id       :integer
#  price         :integer
#  escrow_period :integer
#  due_dill      :integer
#  as_is         :boolean
#  cont_finance  :boolean
#  cont_inspect  :boolean
#  prequalified  :boolean
#  notes         :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryGirl.define do
  factory :offer do
    listing_id 1
user_id 1
price 1
escrow_period 1
due_dill 1
as_is false
cont_finance false
cont ""
prequalified false
notes "MyText"
  end

end
