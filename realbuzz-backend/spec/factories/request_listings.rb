# == Schema Information
#
# Table name: request_listings
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  zip          :string(20)
#  address      :string(4096)
#  type_of_home :string
#  photos       :text             default("{}"), is an Array
#  thumbnails   :text             default("{}"), is an Array
#  square       :decimal(, )
#  bedrooms     :integer          default("0")
#  bathrooms    :integer          default("0")
#  condition    :text
#  notes        :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_request_listings_on_user_id  (user_id)
#

FactoryGirl.define do
  factory :request_listing do
    user nil
address ""
address ""
address ""
address ""
type_of_home "MyString"
photos "MyText"
thumbnails "MyText"
square "9.99"
bedrooms 1
bathrooms 1
condition "MyText"
notes "MyText"
  end

end
