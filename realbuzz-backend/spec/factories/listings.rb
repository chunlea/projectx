# == Schema Information
#
# Table name: listings
#
#  id                        :integer          not null, primary key
#  user_id                   :integer
#  mls_num                   :string
#  listing_class             :integer
#  listing_class_type        :integer
#  status                    :integer
#  for_sale                  :boolean
#  lat                       :float
#  lon                       :float
#  address                   :string(4096)
#  zip                       :string(20)
#  photos                    :text             default("{}"), is an Array
#  thumbnails                :text             default("{}"), is an Array
#  location_rating           :float            default("0.0")
#  condition_rating          :float            default("0.0")
#  bathshalf                 :integer
#  fireplaces                :integer
#  bedrooms                  :integer
#  optional_bedrooms         :integer
#  baths                     :integer
#  acres                     :decimal(, )
#  price                     :decimal(, )
#  list_on_market            :date
#  last_sold_price           :decimal(, )
#  year_built                :integer
#  recorded_mortgage         :decimal(, )      default("0")
#  assesed_value             :decimal(, )      default("0")
#  current_taxes             :decimal(, )      default("0")
#  remarks                   :text
#  home_owner_fees           :decimal(, )
#  home_owner_total_fees     :decimal(, )
#  other_fees                :decimal(, )
#  mello_roos_fee            :decimal(, )
#  bathsfull                 :decimal(, )
#  parkng_non_garaged_spaces :decimal(, )
#  estimated_square_feet     :decimal(, )
#  lot_sqft_approx           :decimal(, )
#  garage                    :decimal(, )
#  parking_spaces_total      :decimal(, )
#  monthly_total_fees        :decimal(, )
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  agent_phone               :string
#  street                    :string
#  city                      :string
#  state                     :string
#  agent_id                  :string
#  tour_link                 :string
#  mls_name                  :string
#  sys_id                    :string
#  upgrade1                  :string
#  upgrade2                  :string
#  upgrade3                  :string
#  upgrade4                  :string
#  green_feature1            :string
#  green_feature2            :string
#  green_feature3            :string
#  green_feature4            :string
#  upgrade1_pic              :string
#  upgrade2_pic              :string
#  upgrade3_pic              :string
#  upgrade4_pic              :string
#  green_feature1_pic        :string
#  green_feature2_pic        :string
#  green_feature3_pic        :string
#  green_feature4_pic        :string
#
# Indexes
#
#  index_listings_on_mls_num     (mls_num)
#  index_listings_on_photos      (photos)
#  index_listings_on_thumbnails  (thumbnails)
#  index_listings_on_user_id     (user_id)
#

FactoryGirl.define do
  factory :listing do
    user nil
mls_num "MyString"
listing_class 1
listing_class_type 1
status 1
for_sale false
lat 1.5
lon 1.5
address "MyString"
zip "MyString"
photos "MyText"
thumbnails "MyText"
location_rating 1.5
condition_rating 1.5
bathshalf 1
fireplaces 1
bedrooms 1
optional_bedrooms 1
baths 1
acres "9.99"
price "9.99"
list_on_market "2014-12-05"
last_sold_price "9.99"
year_built 1
recorded_mortgage "9.99"
assesed_value "9.99"
current_taxes "9.99"
remarks "MyText"
home_owner_fees "9.99"
home_owner_total_fees "9.99"
other_fees "9.99"
mello_roos_fee "9.99"
bathsfull "9.99"
parkng_non_garaged_spaces "9.99"
estimated_square_feet "9.99"
lot_sqft_approx "9.99"
garage "9.99"
parking_spaces_total "9.99"
monthly_total_fees "9.99"
  end

end
