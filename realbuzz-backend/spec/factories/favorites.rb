# == Schema Information
#
# Table name: favorites
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  listing_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_favorites_on_listing_id              (listing_id)
#  index_favorites_on_user_id                 (user_id)
#  index_favorites_on_user_id_and_listing_id  (user_id,listing_id) UNIQUE
#

FactoryGirl.define do
  factory :favorite do
    user nil
listing nil
  end

end
