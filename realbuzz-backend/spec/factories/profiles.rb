# == Schema Information
#
# Table name: profiles
#
#  id           :integer          not null, primary key
#  bio          :text
#  degree       :string
#  yr_graduated :integer
#  college      :string
#  other_school :string
#  designations :string
#  linked_in    :string
#  website      :string
#  profile_pic  :string
#  verified     :boolean
#  agent_id     :integer
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  phone        :string
#

FactoryGirl.define do
  factory :profile do
    bio "MyText"
degree "MyString"
yr_graduated 1
college "MyString"
other_school "MyString"
designations "MyString"
linked_in "MyString"
website "MyString"
profile_pic "MyString"
verified false
  end

end
