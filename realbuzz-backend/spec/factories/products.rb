# == Schema Information
#
# Table name: products
#
#  id            :integer          not null, primary key
#  product_id    :string
#  extend_months :integer          default("0")
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryGirl.define do
  factory :product do
    product_id "MyString"
extend_months 1
  end

end
