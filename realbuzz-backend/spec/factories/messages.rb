# == Schema Information
#
# Table name: messages
#
#  id           :integer          not null, primary key
#  body         :text
#  sender_id    :integer
#  recipient_id :integer
#  status       :integer          default("0")
#  reply_id     :integer
#  listing_id   :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_messages_on_listing_id    (listing_id)
#  index_messages_on_recipient_id  (recipient_id)
#  index_messages_on_reply_id      (reply_id)
#  index_messages_on_sender_id     (sender_id)
#

FactoryGirl.define do
  factory :message do
    body "MyText"
sender nil
recipient nil
status 1
reply nil
listing nil
  end

end
