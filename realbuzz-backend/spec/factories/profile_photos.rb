# == Schema Information
#
# Table name: profile_photos
#
#  id         :integer          not null, primary key
#  profile_id :integer
#  image      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_profile_photos_on_profile_id  (profile_id)
#

FactoryGirl.define do
  factory :profile_photo do
    profile_id 1
image "MyString"
  end

end
