# == Schema Information
#
# Table name: extra_zips
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  zip_code   :string
#  expired_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_extra_zips_on_user_id  (user_id)
#

FactoryGirl.define do
  factory :extra_zip do
    user nil
zip_code "MyString"
  end

end
