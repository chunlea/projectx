class CreateRequestListings < ActiveRecord::Migration
  def change
    create_table :request_listings do |t|
      t.references  :user,           index: true
      t.string      :zip,            limit: 20
      t.string      :address,        limit: 4096
      t.string      :type_of_home
      t.text        :photos,         array: true, default: []
      t.text        :thumbnails,     array: true, default: []
      t.decimal     :square
      t.integer     :bedrooms,       default: 0
      t.integer     :bathrooms,      default: 0
      t.text        :condition
      t.text        :notes

      t.timestamps null: false
    end
    add_foreign_key :request_listings, :users
  end
end
