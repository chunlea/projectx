class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :product_id
      t.integer :extend_months, default: 0

      t.timestamps null: false
    end
  end
end
