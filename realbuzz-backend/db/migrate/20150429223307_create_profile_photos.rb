class CreateProfilePhotos < ActiveRecord::Migration
  def change
    create_table :profile_photos do |t|
      t.integer :profile_id, index: true
      t.string :image

      t.timestamps null: false
    end
  end
end
