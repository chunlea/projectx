class CreateExtraZips < ActiveRecord::Migration
  def change
    create_table :extra_zips do |t|
      t.references :user, index: true
      t.string     :zip_code
      t.datetime   :expired_at

      t.timestamps null: false
    end
    add_foreign_key :extra_zips, :users
  end
end
