class AddUpgradesToListings < ActiveRecord::Migration
  def change
    add_column :listings, :upgrade1, :string
    add_column :listings, :upgrade2, :string
    add_column :listings, :upgrade3, :string
    add_column :listings, :upgrade4, :string
    add_column :listings, :green_feature1, :string
    add_column :listings, :green_feature2, :string
    add_column :listings, :green_feature3, :string
    add_column :listings, :green_feature4, :string
    add_column :listings, :upgrade1_pic, :string
    add_column :listings, :upgrade2_pic, :string
    add_column :listings, :upgrade3_pic, :string
    add_column :listings, :upgrade4_pic, :string
    add_column :listings, :green_feature1_pic, :string
    add_column :listings, :green_feature2_pic, :string
    add_column :listings, :green_feature3_pic, :string
    add_column :listings, :green_feature4_pic, :string
  end
end
