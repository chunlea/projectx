class AddMlsNameToListings < ActiveRecord::Migration
  def change
    add_column :listings, :mls_name, :string
    add_column :listings, :sys_id, :string
  end
end
