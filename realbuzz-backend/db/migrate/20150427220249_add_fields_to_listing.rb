class AddFieldsToListing < ActiveRecord::Migration
  def change
    add_column :listings, :street, :string
    add_column :listings, :city, :string
    add_column :listings, :state, :string
    add_column :listings, :agent_id, :string
    add_column :listings, :tour_link, :string
  end
end
