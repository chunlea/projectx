class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text       :body
      t.references :sender,    index: true
      t.references :recipient, index: true
      t.integer    :status,    default: 0
      t.references :reply,     index: true
      t.references :listing,   index: true

      t.timestamps null: false
    end
  end
end
