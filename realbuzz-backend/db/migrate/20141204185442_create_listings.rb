class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
      t.references  :user,       index: true
      t.string      :mls_num
      t.integer     :listing_class
      t.integer     :listing_class_type
      t.integer     :status
      t.boolean     :for_sale
      t.float       :lat
      t.float       :lon
      t.string      :address,    limit: 4096
      t.string      :zip,        limit: 20
      t.text        :photos,     array: true, default: []
      t.text        :thumbnails, array: true, default: []
      t.float       :location_rating,         default: 0
      t.float       :condition_rating,        default: 0
      t.integer     :bathshalf
      t.integer     :fireplaces
      t.integer     :bedrooms
      t.integer     :optional_bedrooms
      t.integer     :baths
      t.decimal     :acres
      t.decimal     :price
      t.date        :list_on_market
      t.decimal     :last_sold_price
      t.integer     :year_built
      t.decimal     :recorded_mortgage, default: 0
      t.decimal     :assesed_value,     default: 0
      t.decimal     :current_taxes,     default: 0
      t.text        :remarks
      t.decimal     :home_owner_fees
      t.decimal     :home_owner_total_fees
      t.decimal     :other_fees
      t.decimal     :mello_roos_fee
      t.decimal     :bathsfull
      t.decimal     :parkng_non_garaged_spaces
      t.decimal     :estimated_square_feet
      t.decimal     :lot_sqft_approx
      t.decimal     :garage
      t.decimal     :parking_spaces_total
      t.decimal     :monthly_total_fees

      t.timestamps null: false
    end
    add_index        :listings, :mls_num
    add_index        :listings, :photos,     using: 'gin'
    add_index        :listings, :thumbnails, using: 'gin'
    add_foreign_key  :listings, :users
  end
end
