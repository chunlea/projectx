class CreateUpgrade1Pics < ActiveRecord::Migration
  def change
    create_table :upgrade1_pics do |t|
      t.integer :listing_id
      t.string :image

      t.timestamps null: false
    end
  end
end
