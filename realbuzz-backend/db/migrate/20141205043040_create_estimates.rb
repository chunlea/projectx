class CreateEstimates < ActiveRecord::Migration
  def change
    create_table :estimates do |t|
      t.references :user,    index: true
      t.references :listing, index: true
      t.float      :location_rating
      t.float      :condition_rating
      t.decimal    :price
      t.text       :reason
      t.float      :accurate

      t.timestamps null: false
    end
    add_foreign_key :estimates, :users
    add_foreign_key :estimates, :listings
  end
end
