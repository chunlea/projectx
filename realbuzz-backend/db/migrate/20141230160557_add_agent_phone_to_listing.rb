class AddAgentPhoneToListing < ActiveRecord::Migration
  def change
    add_column :listings, :agent_phone, :string
  end
end
