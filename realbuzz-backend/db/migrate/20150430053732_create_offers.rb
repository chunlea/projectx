class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.integer :listing_id
      t.integer :user_id
      t.integer :price
      t.integer :escrow_period
      t.integer :due_dill
      t.boolean :as_is
      t.boolean :cont_finance
      t.boolean :cont_inspect
      t.boolean :prequalified
      t.text :notes

      t.timestamps null: false
    end
  end
end
