class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.references :user,    index: true
      t.references :listing, index: true

      t.timestamps null: false

      t.index [:user_id, :listing_id], unique: true
    end
    add_foreign_key :favorites, :users
    add_foreign_key :favorites, :listings
  end
end
