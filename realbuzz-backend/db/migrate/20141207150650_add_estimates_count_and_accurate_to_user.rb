class AddEstimatesCountAndAccurateToUser < ActiveRecord::Migration
  def change
    add_column :users, :estimates_count, :integer, default: 0
    add_column :users, :accurate,        :float,   default: 0
  end
end
