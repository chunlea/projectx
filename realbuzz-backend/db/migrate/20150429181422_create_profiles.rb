class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.text :bio
      t.string :degree
      t.integer :yr_graduated
      t.string :college
      t.string :other_school
      t.string :designations
      t.string :linked_in
      t.string :website
      t.string :profile_pic
      t.boolean :verified
      t.integer :agent_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
