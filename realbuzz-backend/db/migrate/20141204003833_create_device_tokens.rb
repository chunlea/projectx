class CreateDeviceTokens < ActiveRecord::Migration
  def change
    create_table :device_tokens do |t|
      t.string     :auth_token, null: false
      t.references :user,       index: true
      t.string     :device_id
      t.string     :device_model
      t.string     :device_name
      t.string     :device_osname
      t.string     :device_osversion

      t.timestamps null: false
    end
    add_index :device_tokens, :auth_token, unique: true
    add_index :device_tokens, :device_id, unique: true
    add_foreign_key :device_tokens, :users
  end
end
