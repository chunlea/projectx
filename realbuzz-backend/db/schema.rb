# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150504120509) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "badges_sashes", force: :cascade do |t|
    t.integer  "badge_id"
    t.integer  "sash_id"
    t.boolean  "notified_user", default: false
    t.datetime "created_at"
  end

  add_index "badges_sashes", ["badge_id", "sash_id"], name: "index_badges_sashes_on_badge_id_and_sash_id", using: :btree
  add_index "badges_sashes", ["badge_id"], name: "index_badges_sashes_on_badge_id", using: :btree
  add_index "badges_sashes", ["sash_id"], name: "index_badges_sashes_on_sash_id", using: :btree

  create_table "device_tokens", force: :cascade do |t|
    t.string   "auth_token",       null: false
    t.integer  "user_id"
    t.string   "device_id"
    t.string   "device_model"
    t.string   "device_name"
    t.string   "device_osname"
    t.string   "device_osversion"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "device_tokens", ["auth_token"], name: "index_device_tokens_on_auth_token", unique: true, using: :btree
  add_index "device_tokens", ["device_id"], name: "index_device_tokens_on_device_id", unique: true, using: :btree
  add_index "device_tokens", ["user_id"], name: "index_device_tokens_on_user_id", using: :btree

  create_table "estimates", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "listing_id"
    t.float    "location_rating"
    t.float    "condition_rating"
    t.decimal  "price"
    t.text     "reason"
    t.float    "accurate"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "estimates", ["listing_id"], name: "index_estimates_on_listing_id", using: :btree
  add_index "estimates", ["user_id"], name: "index_estimates_on_user_id", using: :btree

  create_table "extra_zips", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "zip_code"
    t.datetime "expired_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "extra_zips", ["user_id"], name: "index_extra_zips_on_user_id", using: :btree

  create_table "favorites", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "listing_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "favorites", ["listing_id"], name: "index_favorites_on_listing_id", using: :btree
  add_index "favorites", ["user_id", "listing_id"], name: "index_favorites_on_user_id_and_listing_id", unique: true, using: :btree
  add_index "favorites", ["user_id"], name: "index_favorites_on_user_id", using: :btree

  create_table "follows", force: :cascade do |t|
    t.string   "follower_type"
    t.integer  "follower_id"
    t.string   "followable_type"
    t.integer  "followable_id"
    t.datetime "created_at"
  end

  add_index "follows", ["followable_id", "followable_type"], name: "fk_followables", using: :btree
  add_index "follows", ["follower_id", "follower_type"], name: "fk_follows", using: :btree

  create_table "green1_pics", force: :cascade do |t|
    t.integer  "listing_id"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "green2_pics", force: :cascade do |t|
    t.integer  "listing_id"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "green3_pics", force: :cascade do |t|
    t.integer  "listing_id"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "green4_pics", force: :cascade do |t|
    t.integer  "listing_id"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "likes", force: :cascade do |t|
    t.string   "liker_type"
    t.integer  "liker_id"
    t.string   "likeable_type"
    t.integer  "likeable_id"
    t.datetime "created_at"
  end

  add_index "likes", ["likeable_id", "likeable_type"], name: "fk_likeables", using: :btree
  add_index "likes", ["liker_id", "liker_type"], name: "fk_likes", using: :btree

  create_table "listings", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "mls_num"
    t.integer  "listing_class"
    t.integer  "listing_class_type"
    t.integer  "status"
    t.boolean  "for_sale"
    t.float    "lat"
    t.float    "lon"
    t.string   "address",                   limit: 4096
    t.string   "zip",                       limit: 20
    t.text     "photos",                                 default: [],               array: true
    t.text     "thumbnails",                             default: [],               array: true
    t.float    "location_rating",                        default: 0.0
    t.float    "condition_rating",                       default: 0.0
    t.integer  "bathshalf"
    t.integer  "fireplaces"
    t.integer  "bedrooms"
    t.integer  "optional_bedrooms"
    t.integer  "baths"
    t.decimal  "acres"
    t.decimal  "price"
    t.date     "list_on_market"
    t.decimal  "last_sold_price"
    t.integer  "year_built"
    t.decimal  "recorded_mortgage",                      default: 0.0
    t.decimal  "assesed_value",                          default: 0.0
    t.decimal  "current_taxes",                          default: 0.0
    t.text     "remarks"
    t.decimal  "home_owner_fees"
    t.decimal  "home_owner_total_fees"
    t.decimal  "other_fees"
    t.decimal  "mello_roos_fee"
    t.decimal  "bathsfull"
    t.decimal  "parkng_non_garaged_spaces"
    t.decimal  "estimated_square_feet"
    t.decimal  "lot_sqft_approx"
    t.decimal  "garage"
    t.decimal  "parking_spaces_total"
    t.decimal  "monthly_total_fees"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.string   "agent_phone"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "agent_id"
    t.string   "tour_link"
    t.string   "mls_name"
    t.string   "sys_id"
    t.string   "upgrade1"
    t.string   "upgrade2"
    t.string   "upgrade3"
    t.string   "upgrade4"
    t.string   "green_feature1"
    t.string   "green_feature2"
    t.string   "green_feature3"
    t.string   "green_feature4"
    t.string   "upgrade1_pic"
    t.string   "upgrade2_pic"
    t.string   "upgrade3_pic"
    t.string   "upgrade4_pic"
    t.string   "green_feature1_pic"
    t.string   "green_feature2_pic"
    t.string   "green_feature3_pic"
    t.string   "green_feature4_pic"
  end

  add_index "listings", ["mls_num"], name: "index_listings_on_mls_num", using: :btree
  add_index "listings", ["photos"], name: "index_listings_on_photos", using: :gin
  add_index "listings", ["thumbnails"], name: "index_listings_on_thumbnails", using: :gin
  add_index "listings", ["user_id"], name: "index_listings_on_user_id", using: :btree

  create_table "mentions", force: :cascade do |t|
    t.string   "mentioner_type"
    t.integer  "mentioner_id"
    t.string   "mentionable_type"
    t.integer  "mentionable_id"
    t.datetime "created_at"
  end

  add_index "mentions", ["mentionable_id", "mentionable_type"], name: "fk_mentionables", using: :btree
  add_index "mentions", ["mentioner_id", "mentioner_type"], name: "fk_mentions", using: :btree

  create_table "merit_actions", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "action_method"
    t.integer  "action_value"
    t.boolean  "had_errors",    default: false
    t.string   "target_model"
    t.integer  "target_id"
    t.text     "target_data"
    t.boolean  "processed",     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "merit_activity_logs", force: :cascade do |t|
    t.integer  "action_id"
    t.string   "related_change_type"
    t.integer  "related_change_id"
    t.string   "description"
    t.datetime "created_at"
  end

  create_table "merit_score_points", force: :cascade do |t|
    t.integer  "score_id"
    t.integer  "num_points", default: 0
    t.string   "log"
    t.datetime "created_at"
  end

  create_table "merit_scores", force: :cascade do |t|
    t.integer "sash_id"
    t.string  "category", default: "default"
  end

  create_table "messages", force: :cascade do |t|
    t.text     "body"
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.integer  "status",       default: 0
    t.integer  "reply_id"
    t.integer  "listing_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "messages", ["listing_id"], name: "index_messages_on_listing_id", using: :btree
  add_index "messages", ["recipient_id"], name: "index_messages_on_recipient_id", using: :btree
  add_index "messages", ["reply_id"], name: "index_messages_on_reply_id", using: :btree
  add_index "messages", ["sender_id"], name: "index_messages_on_sender_id", using: :btree

  create_table "offers", force: :cascade do |t|
    t.integer  "listing_id"
    t.integer  "user_id"
    t.integer  "price"
    t.integer  "escrow_period"
    t.integer  "due_dill"
    t.boolean  "as_is"
    t.boolean  "cont_finance"
    t.boolean  "cont_inspect"
    t.boolean  "prequalified"
    t.text     "notes"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "photos", force: :cascade do |t|
    t.integer  "listing_id"
    t.string   "image",      limit: 4096
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "photos", ["listing_id"], name: "index_photos_on_listing_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "product_id"
    t.integer  "extend_months", default: 0
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "profile_photos", force: :cascade do |t|
    t.integer  "profile_id"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "profile_photos", ["profile_id"], name: "index_profile_photos_on_profile_id", using: :btree

  create_table "profiles", force: :cascade do |t|
    t.text     "bio"
    t.string   "degree"
    t.integer  "yr_graduated"
    t.string   "college"
    t.string   "other_school"
    t.string   "designations"
    t.string   "linked_in"
    t.string   "website"
    t.string   "profile_pic"
    t.boolean  "verified"
    t.integer  "agent_id"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "phone"
  end

  create_table "request_listings", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "zip",          limit: 20
    t.string   "address",      limit: 4096
    t.string   "type_of_home"
    t.text     "photos",                    default: [],              array: true
    t.text     "thumbnails",                default: [],              array: true
    t.decimal  "square"
    t.integer  "bedrooms",                  default: 0
    t.integer  "bathrooms",                 default: 0
    t.text     "condition"
    t.text     "notes"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "request_listings", ["user_id"], name: "index_request_listings_on_user_id", using: :btree

  create_table "sashes", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "upgrade1_pics", force: :cascade do |t|
    t.integer  "listing_id"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "upgrade2_pics", force: :cascade do |t|
    t.integer  "listing_id"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "upgrade3_pics", force: :cascade do |t|
    t.integer  "listing_id"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "upgrade4_pics", force: :cascade do |t|
    t.integer  "listing_id"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",  null: false
    t.string   "encrypted_password",     default: "",  null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,   null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,   null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "name"
    t.string   "zip_code"
    t.integer  "role",                   default: 0,   null: false
    t.string   "facebook_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "estimates_count",        default: 0
    t.float    "accurate",               default: 0.0
    t.integer  "sash_id"
    t.integer  "level",                  default: 0
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["facebook_id"], name: "index_users_on_facebook_id", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  add_index "users", ["zip_code"], name: "index_users_on_zip_code", using: :btree

  add_foreign_key "device_tokens", "users"
  add_foreign_key "estimates", "listings"
  add_foreign_key "estimates", "users"
  add_foreign_key "extra_zips", "users"
  add_foreign_key "favorites", "listings"
  add_foreign_key "favorites", "users"
  add_foreign_key "listings", "users"
  add_foreign_key "photos", "listings"
  add_foreign_key "request_listings", "users"
end
