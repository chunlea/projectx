# == Schema Information
#
# Table name: messages
#
#  id           :integer          not null, primary key
#  body         :text
#  sender_id    :integer
#  recipient_id :integer
#  status       :integer          default("0")
#  reply_id     :integer
#  listing_id   :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_messages_on_listing_id    (listing_id)
#  index_messages_on_recipient_id  (recipient_id)
#  index_messages_on_reply_id      (reply_id)
#  index_messages_on_sender_id     (sender_id)
#

class Message < ActiveRecord::Base
  default_scope { order(created_at: :desc) }

  belongs_to :sender, class_name: "User", foreign_key: "sender_id"
  belongs_to :recipient, class_name: "User", foreign_key: "recipient_id"
  belongs_to :reply, class_name: "Message", foreign_key: "reply_id"
  belongs_to :listing

  has_many :replys, class_name: "Message", foreign_key: "reply_id"

  enum status: [:unread, :read, :archived]

  after_create :send_notification

  def send_notification
    SendNotificationJob.perform self.recipient_id, "You got a new messsage from #{self.sender.name}.", nil, {type: "message", id: self.id}
  end
end
