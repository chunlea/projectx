# == Schema Information
#
# Table name: listings
#
#  id                        :integer          not null, primary key
#  user_id                   :integer
#  mls_num                   :string
#  listing_class             :integer
#  listing_class_type        :integer
#  status                    :integer
#  for_sale                  :boolean
#  lat                       :float
#  lon                       :float
#  address                   :string(4096)
#  zip                       :string(20)
#  photos                    :text             default("{}"), is an Array
#  thumbnails                :text             default("{}"), is an Array
#  location_rating           :float            default("0.0")
#  condition_rating          :float            default("0.0")
#  bathshalf                 :integer
#  fireplaces                :integer
#  bedrooms                  :integer
#  optional_bedrooms         :integer
#  baths                     :integer
#  acres                     :decimal(, )
#  price                     :decimal(, )
#  list_on_market            :date
#  last_sold_price           :decimal(, )
#  year_built                :integer
#  recorded_mortgage         :decimal(, )      default("0")
#  assesed_value             :decimal(, )      default("0")
#  current_taxes             :decimal(, )      default("0")
#  remarks                   :text
#  home_owner_fees           :decimal(, )
#  home_owner_total_fees     :decimal(, )
#  other_fees                :decimal(, )
#  mello_roos_fee            :decimal(, )
#  bathsfull                 :decimal(, )
#  parkng_non_garaged_spaces :decimal(, )
#  estimated_square_feet     :decimal(, )
#  lot_sqft_approx           :decimal(, )
#  garage                    :decimal(, )
#  parking_spaces_total      :decimal(, )
#  monthly_total_fees        :decimal(, )
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  agent_phone               :string
#  street                    :string
#  city                      :string
#  state                     :string
#  agent_id                  :string
#  tour_link                 :string
#  mls_name                  :string
#  sys_id                    :string
#  upgrade1                  :string
#  upgrade2                  :string
#  upgrade3                  :string
#  upgrade4                  :string
#  green_feature1            :string
#  green_feature2            :string
#  green_feature3            :string
#  green_feature4            :string
#  upgrade1_pic              :string
#  upgrade2_pic              :string
#  upgrade3_pic              :string
#  upgrade4_pic              :string
#  green_feature1_pic        :string
#  green_feature2_pic        :string
#  green_feature3_pic        :string
#  green_feature4_pic        :string
#
# Indexes
#
#  index_listings_on_mls_num     (mls_num)
#  index_listings_on_photos      (photos)
#  index_listings_on_thumbnails  (thumbnails)
#  index_listings_on_user_id     (user_id)
#

class Listing < ActiveRecord::Base
  scope :unsold, -> { where("status <> ?", Listing::statuses[:sold]) }
  belongs_to :user
  geocoded_by :address
  reverse_geocoded_by :lat, :lon
  #after_validation :geocode, if: ->(obj){ obj.address.present? and obj.address_changed? and obj.lat.nil?}

  validates :address, :zip, :remarks, presence: true
  validates :bathshalf, :fireplaces, :bedrooms, :baths, :year_built, numericality: { only_integer: true }
  validates :acres, :price, numericality: true
  validates :zip, zip_code: true, if: Proc.new { |listing| listing.zip.present? }

  has_many :estimates, dependent: :destroy
  has_many :favorites, dependent: :destroy
  has_many :favorite_users, through: :favorites, source: :user
  has_many :messages
  has_many :green_1_pics
  has_many :green_2_pics
  has_many :green_3_pics
  has_many :green_4_pics
  has_many :upgrade1_pics
  has_many :upgrade2_pics
  has_many :upgrade3_pics
  has_many :upgrade4_pics

  enum listing_class: [:residential, :"2_4_units", :lot_land, :residential_rental, :fractional_ownership, :commercial_res_income, :commercial_hotel_motel, :commercial_mobhmpark, :commercial_off_rtl_ind, :commercial_busop, :commercial_lnd_rn_grv]
  enum listing_class_type: [:all_other_attached, :detached, :manufactured_home, :modular_home, :mobile_home, :rowhome, :townhome, :twinhome, :lots_land, :com_lnd_rn_grv]
  enum status: [:active, :sold, :pending, :expired, :off_market, :rented]

  after_save :update_sold_estimates,
    if: Proc.new { |listing| listing.status_changed? and listing.sold? }

  after_create :notify_users

  def favorite_by?(user)
    self.favorite_users.include?(user)
  end

  protected
  def update_sold_estimates
    self.estimates.pendding.find_each do |estimate|
      estimate.update_accurate(self.last_sold_price)
    end
  end
  def notify_users
    user_ids = (User.agent.where(zip_code: self.zip).ids).concat(ExtraZip.includes(:user).where(zip_code: self.zip, users: {role: User::roles[:agent]}).map(&:user).map(&:id)).uniq
    SendNotificationJob.perform user_ids, "There is new listing available in your aera.", nil, {type: "listing", id: self.id}
  end
end
