# == Schema Information
#
# Table name: profile_photos
#
#  id         :integer          not null, primary key
#  profile_id :integer
#  image      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_profile_photos_on_profile_id  (profile_id)
#

class ProfilePhoto < ActiveRecord::Base
  belongs_to :profile
  mount_uploader :image, ImageUploader

  after_save :update_profile_photos

  protected
  def update_profile_photos
    @profile = Profile.find(self.profile_id)
    if @profile
      @profile.update(profile_pic: self.image_url)
      #@profile.thumbnails << self.image.thumb.url
      @profile.save!
    end
  end
end
