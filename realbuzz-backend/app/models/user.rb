# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  failed_attempts        :integer          default("0"), not null
#  unlock_token           :string
#  locked_at              :datetime
#  name                   :string
#  zip_code               :string
#  role                   :integer          default("0"), not null
#  facebook_id            :string
#  created_at             :datetime
#  updated_at             :datetime
#  estimates_count        :integer          default("0")
#  accurate               :float            default("0.0")
#  sash_id                :integer
#  level                  :integer          default("0")
#
# Indexes
#
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_facebook_id           (facebook_id) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#  index_users_on_zip_code              (zip_code)
#

class User < ActiveRecord::Base
  has_merit

  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
  enum role: [:buyer, :agent]

  acts_as_follower
  acts_as_followable

  validates :zip_code, zip_code: true, if: Proc.new { |user| user.zip_code.present? }

  has_many :devices, class_name: "DeviceToken", dependent: :destroy
  has_many :extra_zips, dependent: :destroy
  has_many :estimates, dependent: :destroy
  has_many :own_listings, class_name: "Listing", dependent: :destroy, source: :listing
  has_many :request_listings, dependent: :destroy
  has_many :estimated_listings, through: :estimates, source: :listing
  has_many :favorites, dependent: :destroy
  has_many :favorite_listings, through: :favorites, source: :listing
  has_many :sent_messages, class_name: "Message", foreign_key: "sender_id"
  has_many :received_messages, class_name: "Message", foreign_key: "recipient_id"
  has_one  :profile

  protected
  # Allow unconfirmed user to login
  def confirmation_required?
    false
  end
end
