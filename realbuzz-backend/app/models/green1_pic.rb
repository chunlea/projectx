# == Schema Information
#
# Table name: green1_pics
#
#  id         :integer          not null, primary key
#  listing_id :integer
#  image      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Green1Pic < ActiveRecord::Base
  belongs_to :listing
  mount_uploader :image, ImageUploader

  after_save :update_listing_photos

  protected
  def update_listing_photos
    @listing = Listing.find(self.listing_id)
    if @listing
      @listing.update(green_feature1_pic: self.image_url)
      @listing.save!
    end
  end
end
