# == Schema Information
#
# Table name: request_listings
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  zip          :string(20)
#  address      :string(4096)
#  type_of_home :string
#  photos       :text             default("{}"), is an Array
#  thumbnails   :text             default("{}"), is an Array
#  square       :decimal(, )
#  bedrooms     :integer          default("0")
#  bathrooms    :integer          default("0")
#  condition    :text
#  notes        :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_request_listings_on_user_id  (user_id)
#

class RequestListing < ActiveRecord::Base
  belongs_to :user

  validates :address, :zip, presence: true
  validates :bedrooms, :bathrooms, numericality: { only_integer: true }
  validates :square, numericality: true
  validates :zip, zip_code: true, if: Proc.new { |listing| listing.zip.present? }

  after_create :notify_users

  protected
  def notify_users
    user_ids = (User.agent.where(zip_code: self.zip).ids).concat(ExtraZip.includes(:user).where(zip_code: self.zip, users: {role: User::roles[:agent]}).map(&:user).map(&:id)).uniq
    SendNotificationJob.perform user_ids, "There is new listing requested estimate in your aera.", nil, {type: "request_listing", id: self.id}
  end
end
