# == Schema Information
#
# Table name: estimates
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  listing_id       :integer
#  location_rating  :float
#  condition_rating :float
#  price            :decimal(, )
#  reason           :text
#  accurate         :float
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_estimates_on_listing_id  (listing_id)
#  index_estimates_on_user_id     (user_id)
#

class Estimate < ActiveRecord::Base
  scope :by_user, ->(user) { where(user: user) }
  scope :resulted, ->{ where.not(accurate: nil) }
  scope :pendding, ->{ where(accurate: nil) }

  belongs_to :user, counter_cache: true
  belongs_to :listing

  before_update :only_allowed_update_pendding
  after_save :update_listing_rating,
    if: Proc.new { |estimate| estimate.location_rating_changed? or estimate.condition_rating_changed? }

  def update_accurate(sold_price)
    self.update_columns(accurate: calculate_accurate(sold_price.to_f, self.price.to_f))
    if self.user
      self.user.update!(accurate: self.user.accurate + (self.accurate - self.user.accurate)/self.user.estimates.resulted.count )
      SendNotificationJob.perform self.user.id, "Listing at #{self.listing.address} has sold. Click to view the selling price.", nil, {type: "listing", id: self.listing_id}
    end
  end

  def is_pendding?
    self.accurate.nil?
  end

  protected
  def only_allowed_update_pendding
    return true if self.is_pendding?
    self.errors.add :base, "Cannot update this #{ self.class.to_s }, because the listing had been sold."
    return false
  end
  def update_listing_rating
    self.listing.update(
      location_rating: self.listing.location_rating + (self.location_rating-self.listing.location_rating)/self.listing.estimates.count,
      condition_rating: self.listing.condition_rating + (self.condition_rating-self.listing.condition_rating)/self.listing.estimates.count,
    )
  end
  def calculate_accurate(a, b)
    1.0-(a - b)/[a, b].max
  end
end
