# == Schema Information
#
# Table name: photos
#
#  id         :integer          not null, primary key
#  listing_id :integer
#  image      :string(4096)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_photos_on_listing_id  (listing_id)
#

class Photo < ActiveRecord::Base
  belongs_to :listing
  mount_uploader :image, ImageUploader

  after_save :update_listing_photos

  protected
  def update_listing_photos
    @listing = Listing.find(self.listing_id)
    if @listing
      @listing.photos << self.image_url
      @listing.thumbnails << self.image.thumb.url
      @listing.save!
    end
  end
end
