# == Schema Information
#
# Table name: device_tokens
#
#  id               :integer          not null, primary key
#  auth_token       :string           not null
#  user_id          :integer
#  device_id        :string
#  device_model     :string
#  device_name      :string
#  device_osname    :string
#  device_osversion :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_device_tokens_on_auth_token  (auth_token) UNIQUE
#  index_device_tokens_on_device_id   (device_id) UNIQUE
#  index_device_tokens_on_user_id     (user_id)
#

class DeviceToken < ActiveRecord::Base
  belongs_to :user

  before_create :ensure_token!
  before_save   :remove_same_device_id
  def ensure_token!
    return if auth_token.present?
    self.auth_token = generate_auth_token
  end

  def remove_same_device_id
    old_token = self.class.find_by_device_id(self.device_id)
    if old_token
      old_token.destroy!
    end
  end

  private
  def generate_secure_token_string
    SecureRandom.uuid
  end

  def generate_auth_token
    loop do
      auth_token = generate_secure_token_string
      break auth_token unless self.class.exists?(auth_token: auth_token)
    end
  end

end
