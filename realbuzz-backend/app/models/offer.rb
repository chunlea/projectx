# == Schema Information
#
# Table name: offers
#
#  id            :integer          not null, primary key
#  listing_id    :integer
#  user_id       :integer
#  price         :integer
#  escrow_period :integer
#  due_dill      :integer
#  as_is         :boolean
#  cont_finance  :boolean
#  cont_inspect  :boolean
#  prequalified  :boolean
#  notes         :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Offer < ActiveRecord::Base
end
