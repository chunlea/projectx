# == Schema Information
#
# Table name: extra_zips
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  zip_code   :string
#  expired_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_extra_zips_on_user_id  (user_id)
#

class ExtraZip < ActiveRecord::Base
  belongs_to :user

  validates :zip_code, zip_code: true

  after_initialize :defaults, unless: :persisted?
  def defaults
    self.expired_at ||= Time.new.next_month
  end

  def expired?
    self.expired_at < Time.now
  end
end
