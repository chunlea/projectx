module ApplicationHelper
  def render_page_title(page_title=nil)
    @page_title = page_title if page_title
    title = @page_title ? "#{@page_title} | ProjectX" : "ProjectX - Real Estate Everywhere"
    content_tag(:title, title)
  end
end
