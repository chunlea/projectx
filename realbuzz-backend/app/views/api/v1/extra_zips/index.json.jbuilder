json.array! @zips do |zip|
  # next if comment.marked_as_spam_by?(current_user)
  json.id zip.id
  json.user_id zip.user_id
  json.zip_code zip.zip_code
  json.expired zip.expired?
  json.expired_at zip.expired_at.strftime("%Y-%m-%d")
end
