json.app_name    "ProjectX"
json.api_version "1.0"
json.time_now    Time.now
json.developer   "Chunlea Ju"

if user_signed_in?
  json.current_user do
    json.partial! 'api/v1/users/user', user: current_user
  end
  json.users do
    # json.partial! 'api/v1/users/user', collection: [current_user], as: :user
    json.partial! partial: 'api/v1/users/user', collection: [current_user], as: :user
  end
  # json.users [current_user], partial: 'api/v1/users/user', as: :user
end

