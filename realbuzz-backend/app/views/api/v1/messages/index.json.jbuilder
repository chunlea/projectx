json.array!(@messages) do |message|
  json.extract! message, :id, :body, :listing_id, :status, :created_at, :updated_at
  json.sender do
    json.extract! message.sender, :id, :email, :name
  end
  json.recipient do
    json.extract! message.recipient, :id, :email, :name
  end
  # if message.reply
  #   json.recipient do
  #     json.extract! message.reply, :id, :body, :status, :created_at, :updated_at
  #   end
  # end
end
