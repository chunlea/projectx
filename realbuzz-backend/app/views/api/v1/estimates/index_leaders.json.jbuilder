json.array! @leaders do |user|
  json.extract! user, :id, :email,
                :name, :zip_code, :role,
                :estimates_count, :accurate
  json.followed current_user.follows?(user)
end
