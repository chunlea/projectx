json.extract! listing, :id, :user, :mls_num, :listing_class,
                       :listing_class_type, :status, :for_sale,
                       :lat, :lon, :address, :zip, :photos,
                       :thumbnails, :location_rating, :condition_rating,
                       :bathshalf, :fireplaces, :bedrooms, :optional_bedrooms,
                       :baths, :acres, :price, :list_on_market, :last_sold_price,
                       :year_built, :recorded_mortgage, :assesed_value, :current_taxes,
                       :remarks, :home_owner_fees, :home_owner_total_fees, :other_fees,
                       :mello_roos_fee, :bathsfull, :parkng_non_garaged_spaces,
                       :estimated_square_feet, :lot_sqft_approx, :garage, :agent_phone,
                       :parking_spaces_total, :monthly_total_fees, :created_at, :updated_at,
                       :street, :city, :state, :agent_id, :tour_link, :mls_name, :sys_id, 
                       :upgrade1, :upgrade2, :upgrade3, :upgrade4, :upgrade1_pic, :upgrade2_pic, 
                       :upgrade3_pic, :upgrade4_pic, :green_feature1, :green_feature2, :green_feature3, 
                       :green_feature4, :green_feature1_pic, :green_feature2_pic, :green_feature3_pic, 
                       :green_feature4_pic
json.favorited listing.favorite_by?(current_user)
json.previous_reviews listing.estimates.by_user(current_user)
json.estimates listing.estimates