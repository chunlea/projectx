json.extract! user, :id, :email,
              :sign_in_count, :current_sign_in_at,
              :last_sign_in_at, :current_sign_in_ip,
              :last_sign_in_ip, :confirmed_at,
              :name, :zip_code, :role, :facebook_id,
              :estimates_count, :accurate
json.confirmed user.confirmed?
json.followed current_user.follows?(user)
