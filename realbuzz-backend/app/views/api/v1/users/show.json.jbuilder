json.extract! @user, :id, :email,
              :sign_in_count, :current_sign_in_at,
              :last_sign_in_at, :current_sign_in_ip,
              :last_sign_in_ip, :confirmed_at,
              :name, :zip_code, :role, :facebook_id,
              :estimates_count, :accurate, :points
json.confirmed @user.confirmed?
json.followed current_user.follows?(@user)

json.sold_listings @user.own_listings.sold.count

json.followers @user.followers(User).count
json.followings @user.followees(User).count

if @user.profile
  json.extract! @user.profile, :bio, :degree, :yr_graduated, :college,
              :other_school, :designations, :linked_in, :website,
              :profile_pic, :verified, :agent_id, :phone
  json.educations "#{@user.profile.degree} #{@user.profile.yr_graduated} #{@user.profile.college} \n#{@user.profile.other_school}"
end