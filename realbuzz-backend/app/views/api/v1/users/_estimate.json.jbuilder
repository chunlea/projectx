json.extract! estimate, :id, :user_id, :listing_id, :location_rating, :condition_rating,
                        :price, :reason, :accurate, :created_at, :updated_at
json.user do
  json.partial! 'api/v1/users/user', user: estimate.user
end
json.listing do
  json.partial! 'api/v1/listings/listing', listing: estimate.listing
end
