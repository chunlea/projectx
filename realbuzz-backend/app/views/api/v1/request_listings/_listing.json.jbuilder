json.extract! listing, :id, :user, :zip, :address, :type_of_home, :photos,
                       :thumbnails, :square, :bedrooms, :bathrooms,
                       :condition, :notes, :created_at, :updated_at
