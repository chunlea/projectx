class Api::V1::ProfilesController < ApiController
  before_action :authenticate!
  skip_before_filter :verify_authenticity_token
  # before_action :set_profile, only: [:show, :edit, :update, :destroy, :mark_message_read]


  def index
    user = User.find(params[:user_id])
    @profile = user.profile
    render json: @profile
  end

  def create
    @profile = Profile.new(profile_params)
    if @profile.save
      render json: @profile, status: :ok
      listings = Listing.where(agent_id: @profile.agent_id)
      if listings.any?
        listings.each do |listing|
          listing.update(user_id: @profile.user.id)
        end
      end
    else
      render status: :unprocessable_entity, json: {errors: @profile.errors.full_messages.join("; \n")}
    end
  end

  def update
    @profile = Profile.find(params[:id])
    if @profile.update(profile_params)
      render :show, status: :ok
    else
      render status: :unprocessable_entity, json: {errors: @profile.errors.full_messages.join("; \n")}
    end
  end

  def upload
    @photo = ProfilePhoto.new(image: params[:photo], profile_id: params[:profile_id])
    if @photo.save
      render status: :ok, json: {}
    else
      render status: :unprocessable_entity, json: {errors: @photo.errors.full_messages.join("; \n")}
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def profile_params
    params.require(:profile).permit(:bio, :degree, :yr_graduated, :college, :other_school, :designations, :linked_in, :website, :profile_pic, :user_id, :phone, :agent_id)
  end
end