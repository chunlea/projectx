class Api::V1::MessagesController < ApiController
  before_action :authenticate!
  before_action :set_message, only: [:show, :edit, :update, :destroy, :mark_message_read]

  def index
    @messages = current_user.received_messages.page params[:page]
  end
  def index_unread
    @messages = current_user.received_messages.unread.page params[:page]
    render :index
  end
  def index_archived
    @messages = current_user.received_messages.archived.page params[:page]
    render :index
  end

  def mark_message_read
    @message.read!
    render json: {}
  end
  def mark_message_archived
    @message.archived!
    render json: {}
  end

  def show
  end

  def create
    if params[:reply_id].present?
      message = Message.find(params[:reply_id])
      @message = current_user.sent_messages.build(body: params[:body], recipient: message.sender, reply_id: params[:reply_id])
      if @message.save
        current_user.add_points(50)
        render :show, status: :created, location: v1_message_url(@message)
      else
        render status: :unprocessable_entity, json: {errors: @message.errors.full_messages}
      end
    elsif params[:user_id].present?
      recipient = User.find(params[:user_id])
      if recipient
        @message = current_user.sent_messages.build(body: params[:body], listing: nil, recipient: recipient)
        if @message.save
          render :show, status: :created, location: v1_message_url(@message)
        else
          render status: :unprocessable_entity, json: {errors: @message.errors.full_messages}
        end
      else
        render status: :unprocessable_entity, json: {errors: "Can't send message for now."}
      end

    else
      unless params[:listing_id].present?
        render status: :unprocessable_entity, json: {errors: "Can't find this listing's seller or agent."}
      end

      listing = Listing.find(params[:listing_id])
      @message = current_user.sent_messages.build(body: params[:body], listing: listing, recipient: listing.user)
      if @message.save
        render :show, status: :created, location: v1_message_url(@message)
      else
        render status: :unprocessable_entity, json: {errors: @message.errors.full_messages}
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_message
    @message = Message.find(params[:id])
  end

end
