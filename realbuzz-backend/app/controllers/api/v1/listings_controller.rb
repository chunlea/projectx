class Api::V1::ListingsController < ApiController
  before_action :authenticate!
  before_action :set_listing, only: [:show, :edit, :update, :destroy, :favorite, :upload]

  def nearests
    if params[:filter]
      filter = params[:filter]
      if params[:filter][:price_end].to_f == 0
        filter[:price_end] = Float::MAX
      end
      p filter
      @listings = Listing.unsold.near(
        [params[:lat], params[:lon]],
        params[:radius],
        units: :km
      ).where("price >= :price_begin AND price <= :price_end AND bedrooms >= :min_beds AND baths >= :min_baths", filter).page params[:page]
    else
      @listings = Listing.unsold.near(
        [params[:lat], params[:lon]],
        params[:radius],
        units: :km
      ).page params[:page]
    end
  end

  def favorite
    if @listing.favorite_by?(current_user)
      if @listing.favorites.find_by(user: current_user).destroy
        render stauts: :ok, json: {status: false}
      else
        render stauts: :unprocessable_entity, json: {errors: @listing.errors.full_messages}
      end
    else
      if @listing.favorites.create(user: current_user)
        current_user.add_points(50)
        render stauts: :ok, json: {status: true}
      else
        render stauts: :unprocessable_entity, json: {errors: @listing.errors.full_messages}
      end
    end
  end

  def favorites
    @listings = current_user.favorite_listings.page params[:page]
    render :nearests
  end

  def upload
    @photo = Photo.new(image: params[:photo], listing_id: params[:id])
    if @photo.save
      render status: :ok, json: {}
    else
      render status: :unprocessable_entity, json: {errors: @photo.errors.full_messages.join("; \n")}
    end
  end

  def feature_pic1
    @photo = Upgrade1Pic.new(image: params[:upgrade1_pic], listing_id: params[:id])
    if @photo.save
      render status: :ok, json: {}
    else
      render status: :unprocessable_entity, json: {errors: @photo.errors.full_messages.join("; \n")}
    end
  end
  def feature_pic2
    @photo = Upgrade2Pic.new(image: params[:upgrade2_pic], listing_id: params[:id])
    if @photo.save
      render status: :ok, json: {}
    else
      render status: :unprocessable_entity, json: {errors: @photo.errors.full_messages.join("; \n")}
    end
  end

  def feature_pic3
    @photo = Upgrade3Pic.new(image: params[:upgrade3_pic], listing_id: params[:id])
    if @photo.save
      render status: :ok, json: {}
    else
      render status: :unprocessable_entity, json: {errors: @photo.errors.full_messages.join("; \n")}
    end
  end

  def feature_pic4
    @photo = Upgrade4Pic.new(image: params[:upgrade4_pic], listing_id: params[:id])
    if @photo.save
      render status: :ok, json: {}
    else
      render status: :unprocessable_entity, json: {errors: @photo.errors.full_messages.join("; \n")}
    end
  end

  def green_pic1
    @photo = Green1Pic.new(image: params[:gree_feature1_pic], listing_id: params[:id])
    if @photo.save
      render status: :ok, json: {}
    else
      render status: :unprocessable_entity, json: {errors: @photo.errors.full_messages.join("; \n")}
    end
  end

  def green_pic2
    @photo = Green2Pic.new(image: params[:gree_feature2_pic], listing_id: params[:id])
    if @photo.save
      render status: :ok, json: {}
    else
      render status: :unprocessable_entity, json: {errors: @photo.errors.full_messages.join("; \n")}
    end
  end

  def green_pic3
    @photo = Green3Pic.new(image: params[:gree_feature3_pic], listing_id: params[:id])
    if @photo.save
      render status: :ok, json: {}
    else
      render status: :unprocessable_entity, json: {errors: @photo.errors.full_messages.join("; \n")}
    end
  end

  def green_pic4
    @photo = Green4Pic.new(image: params[:gree_feature4_pic], listing_id: params[:id])
    if @photo.save
      render status: :ok, json: {}
    else
      render status: :unprocessable_entity, json: {errors: @photo.errors.full_messages.join("; \n")}
    end
  end

  def create
    if params[:listing][:listing_id]
      @listing = Listing.find(params[:listing][:listing_id])
      @listing.update(new_listing_params)
      render json: @listing
    else
      @listing = current_user.own_listings.build(new_listing_params)
      @listing.list_on_market = Date.today
      if @listing.save
        current_user.add_points(50)
        render :show, status: :ok, location: v1_listing_path(@listing)
      else
        render status: :unprocessable_entity, json: {errors: @listing.errors.full_messages.join("; \n")}
      end
    end
  end

  def owned
    @listings = User.find(params[:user_id]).listings
  end

  private
  def set_listing
    @listing = Listing.find(params[:id])
  end
  # Never trust parameters from the scary internet, only allow the white list through.
  def new_listing_params
    params.require(:listing).permit(:acres, :address, :assesed_value, :baths, :bathsfull, :bathshalf, :bedrooms, :current_taxes, :estimated_square_feet, :fireplaces, :for_sale, :garage, :home_owner_fees, :home_owner_total_fees, :last_sold_price, :listing_class, :listing_class_type, :lot_sqft_approx, :mello_roos_fee, :monthly_total_fees, :optional_bedrooms, :other_fees, :parking_spaces_total, :parkng_non_garaged_spaces, :price, :recorded_mortgage, :remarks, :status, :year_built, :zip, :lat, :lon, :street, :city, :state, :agent_id, :tour_link, :mls_name, :sys_id, :upgrade1, :upgrade2, :upgrade3, :upgrade4, :upgrade1_pic, :upgrade2_pic, :upgrade3_pic, :upgrade4_pic, :green_feature1, :green_feature2, :green_feature3, :green_feature4, :green_feature1_pic, :green_feature2_pic, :green_feature3_pic, :green_feature4_pic)
  end
end
