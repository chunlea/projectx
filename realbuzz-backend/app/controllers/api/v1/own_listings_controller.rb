class Api::V1::OwnListingsController < ApiController
  before_action :authenticate!
  skip_before_filter :verify_authenticity_token

	def index
		@listings = Array.new
		Listing.unsold.where(agent_id: User.find(params[:user_id]).profiles.last.agent_id).each do |listing|
			@listings << listing
		end
		User.find(params[:user_id]).own_listings.each do |own_listings|
			@listings << own_listings
		end
		render json: @listings
	end
end