class Api::V1::RequestListingsController < ApiController
  before_action :authenticate!
  before_action :set_listing, only: [:show, :edit, :update, :destroy, :favorite, :upload]

  def upload
    @photo = Photo.new(image: params[:photo], listing_id: params[:id])
    if @photo.save
      render status: :ok, json: {}
    else
      render status: :unprocessable_entity, json: {errors: @photo.errors.full_messages.join("; \n")}
    end
  end

  def create
    if params[:listing][:listing_id]
      @listing = RequestListing.find(params[:listing][:listing_id])
      @listing.update(new_listing_params)
      render json: @listing
    else
      @listing = current_user.request_listings.build(new_listing_params)
      if @listing.save
        current_user.add_points(50)
        render :show, status: :ok, location: v1_request_listing_path(@listing)
      else
        render status: :unprocessable_entity, json: {errors: @listing.errors.full_messages.join("; \n")}
      end
    end
  end

  private
  def set_listing
    @listing = RequestListing.find(params[:id])
  end
  # Never trust parameters from the scary internet, only allow the white list through.
  def new_listing_params
    params.require(:listing).permit(:zip, :address, :type_of_home, :photos, :thumbnails, :square, :bedrooms, :bathrooms, :condition, :notes)
  end
end
