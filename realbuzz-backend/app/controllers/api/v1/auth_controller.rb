class Api::V1::AuthController < ApiController
  def authenticate
    if params[:email].nil? or params[:password].nil?
      render status: :unprocessable_entity,
             json: {errors: "Email & password parameters are required."}
      return false
    end

    @user=User.find_by_email(params[:email].to_s.downcase)

    if @user.nil? or not @user.valid_password?(params[:password].to_s)
      render status: :unprocessable_entity,
             json: {errors: "Invalid email or passoword."}
      return false
    else
      @device = @user.devices.build(device_id: params[:device_id], device_model: params[:model], device_name: params[:name], device_osname: params[:os_name], device_osversion: params[:os_version])
      if @device.save
        sign_in @user, store: false
        render status: :created,
               json: {
                user: current_user,
                confirmed: current_user.confirmed?,
                token: @device.auth_token,
               }
        return true
      else
        render status: :unprocessable_entity,
               json: {errors: @device.errors.full_messages.join("; \n")}
        return false
      end
    end
  end

  def authenticate_facebook
    # params => {"link"=>"https://www.facebook.com/app_scoped_user_id/561054630692187/", "id"=>"561054630692187", "first_name"=>"Chunlea", "name"=>"Chunlea Ju", "gender"=>"male", "last_name"=>"Ju", "email"=>"ichunlea@me.com", "locale"=>"zh_CN", "timezone"=>8, "updated_time"=>"2014-08-10T06:28:12+0000", "verified"=>true}

    @user=User.find_by_facebook_id(params[:id].downcase)
    if @user.nil?
      if params[:email].empty?
        params[:email] = "#{params[:id]}@facebook.com"
      end
      password = Devise.friendly_token

      @user=User.find_by_email(params[:email].downcase)
      if @user.nil?
        @user = User.new(email: params[:email].downcase,
                         password: Devise.friendly_token,
                         name: "#{params[:first_name]} #{params[:last_name]}",
                         facebook_id: params[:id])
      else
        @user.facebook_id = params[:id]
      end
    end

    if @user.save
      @device = @user.devices.build(device_id: params[:device_id], device_model: params[:model], device_name: params[:name], device_osname: params[:os_name], device_osversion: params[:os_version])
      if @device.save
        sign_in @user, store: false
        render status: :created,
               json: {
                user: current_user,
                confirmed: current_user.confirmed?,
                token: @device.auth_token,
               }
        return true
      else
        render status: :unprocessable_entity,
               json: {errors: @device.errors.full_messages.join("; \n")}
        return false
      end
    else
      render status: :unprocessable_entity,
             json: {errors: @user.errors.full_messages.join("; \n")}
      return false
    end
  end

  def register
    @user = User.new(email: params[:email].downcase,
                     password: params[:password],
                     name: params[:name],
                     role: params[:role],
                     zip_code: params[:zip_code])
    if @user.save
      @device = @user.devices.build(device_id: params[:device_id], device_model: params[:model], device_name: params[:name], device_osname: params[:os_name], device_osversion: params[:os_version])
      if @device.save
        sign_in @user, store: false
        render status: :created,
               json: {
                user: current_user,
                confirmed: current_user.confirmed?,
                token: @device.auth_token,
               }
        return true
      else
        render status: :unprocessable_entity,
               json: {errors: @device.errors.full_messages.join("; \n")}
        return false
      end
    else
      render status: :unprocessable_entity,
             json: {errors: @user.errors.full_messages.join("; \n")}
      return false
    end
  end
end