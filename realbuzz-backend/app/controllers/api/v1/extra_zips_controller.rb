class Api::V1::ExtraZipsController < ApiController
  before_action :authenticate!
  before_action :set_extra_zip, only: [:update]
  before_action :find_product, only: [:create, :update]

  def index
    @zips = current_user.extra_zips.page params[:page]
  end
  def create
    @zip = current_user.extra_zips.find_by(zip_code: params[:zip_code])
    if @zip
      if @zip.expired?
        @zip.expired_at =  Time.now + @product.extend_months.months
      else
        @zip.expired_at += @product.extend_months.months
      end
    else
      @zip = current_user.extra_zips.build(zip_code: params[:zip_code])
      @zip.expired_at =  Time.now + @product.extend_months.months
      current_user.add_points(50)
    end
    if @zip.save
      render :show, status: :ok, json: {}
    else
      render status: :unprocessable_entity, json: {errors: @zip.errors.full_messages.join("; \n")}
    end
  end
  def update
    if @zip.expired?
      @zip.expired_at =  Time.now + @product.extend_months.months
    else
      @zip.expired_at += @product.extend_months.months
    end
    if @zip.save
      render :show, status: :ok, json: {}
    else
      render status: :unprocessable_entity, json: {errors: @zip.errors.full_messages.join("; \n")}
    end
  end

  private
  def set_extra_zip
    @zip = ExtraZip.find(params[:id])
  end
  def find_product
    @product = Product.find_by(product_id: params[:product])
    if @product.nil?
      render status: :unprocessable_entity, json: {errors: "The Product you buy dosen't exist any more."}
      return
    end
  end
end
