class Api::V1::EstimatesController < ApiController
  before_action :authenticate!
  before_action :set_listing, except: [:index_current_user, :index_leaders, :index_leaders_by_amounts]
  before_action :set_estimate, only: [:show, :edit, :update, :destroy]

  def index
    @estimates = @listing.estimates.by_user(current_user)
  end
  def index_current_user
    @estimates = current_user.estimates.page params[:page]
    @show_listing = true
    render :index
  end
  def index_leaders
    @leaders = User.order(accurate: :desc).page params[:page]
  end
  def index_leaders_by_amounts
    @leaders = User.order(estimates_count: :desc).page params[:page]
    render :index_leaders
  end

  def create
    @estimate = @listing.estimates.build(estimate_params)
    @estimate.user = current_user
    if @estimate.save
      current_user.add_points(50)
      render :show, status: :ok, location: v1_listing_estimates_path(@estimate)
    else
      render status: :unprocessable_entity, json: {errors: @estimate.errors.full_messages.join("; \n")}
    end
  end

  def update
    if @estimate.update(estimate_params)
      render :show, status: :ok, location: v1_listing_estimates_path(@estimate)
    else
      render status: :unprocessable_entity, json: {errors: @estimate.errors.full_messages.join("; \n")}
    end
  end

  private
  def set_listing
    @listing = Listing.find(params[:listing_id])
  end
  def set_estimate
    @estimate = Estimate.find(params[:id])
  end
  # Never trust parameters from the scary internet, only allow the white list through.
  def estimate_params
    params.require(:estimate).permit(:location_rating, :condition_rating, :price, :reason)
  end
end
