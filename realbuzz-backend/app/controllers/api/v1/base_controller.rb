class Api::V1::BaseController < ApiController
  before_action :authenticate!, only: [:products]#, execpt: [:any], only: [:some]
  api_version "v1"

  # Description all api Endpoints here
  api :GET, 'v1', "API version 1's root path"
  def index
  end

  def products
    render status: :ok, json: {products: Product.all.map(&:product_id)}
  end
end
