class Api::V1::UsersController < ApiController
  before_action :authenticate!
  before_action :set_user, only: [:show, :toggle_follow, :estimates, :extra_zips]

  def update
    if current_user.update_with_password(user_params)
        render :show, status: :ok, location: v1_users_url(current_user)
    else
      render status: :unprocessable_entity, json: {errors: current_user.errors.full_messages.join("; \n")}
    end
  end

  def update_zip
    if current_user.update(params.require(:user).permit(:zip_code))
        render :show, status: :ok, location: v1_users_url(current_user)
    else
      render status: :unprocessable_entity, json: {errors: current_user.errors.full_messages.join("; \n")}
    end
  end

  def toggle_follow
    current_user.toggle_follow!(@user)
    current_user.add_points(50)
    render json: {followed: current_user.follows?(@user)}
  end

  def estimates
    @estimates = @user.estimates.page params[:page]
    @show_listing = true
  end

  def followers
    @users = current_user.followers(User)
    render :index
  end
  def following
    @users = current_user.followees(User)
    render :index
  end

  def myself
    @users = current_user
    render :index
  end

  def show

  end

  private
  def set_user
    @user = User.find(params[:id])
  end
  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:email, :name, :zip_code, :role, :current_password, :password, :password_confirmation)
  end
end
