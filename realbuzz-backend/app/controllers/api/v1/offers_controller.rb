class Api::V1::OffersController < ApiController
  before_action :authenticate!
  before_action :set_message, only: [:show, :edit, :update, :destroy, :mark_message_read]


  def index
    user = User.find(params[:user_id])
    listing = Listing.find(params[:listing_id])
    @offer = Offer.where("user_id = ? AND listing_id = ?", user.id, listing.id).last
    render json: @offer
  end

  def create
    @offer = Offer.new(offer_params)
    if @offer.save
      render json: @offer, status: :ok
      	listing = Listing.find(@offer.listing_id)
      	user = User.find(listing.user_id)
      	@phone = Profile.where(user_id:user.id).last.phone
		account_sid = 'ProjectX'  
		auth_token = 'ProjectX' 
		 
		# set up a client to talk to the Twilio REST API 
		@client = Twilio::REST::Client.new account_sid, auth_token 
		 
		@client.account.messages.create({
			:from => '+18587035520', 
			:to => "#{@phone}", 
			:body => 'New offer received on ProjectX!',  
		})      
    else
      render status: :unprocessable_entity, json: {errors: @offer.errors.full_messages.join("; \n")}
    end
  end

  def update
    if @offer.update(offer_params)
      render :show, status: :ok
    else
      render status: :unprocessable_entity, json: {errors: @offer.errors.full_messages.join("; \n")}
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  # Never trust parameters from the scary internet, only allow the white list through.
  def offer_params
    params.require(:offer).permit(:listing_id, :user_id, :price, :escrow_period, :due_dill, :as_is, :cont_finance, :cont_inspect, :prequalified, :notes)
  end
end