ActiveAdmin.register Listing do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  index do
    selectable_column
    id_column
    column :user_id                  
    column :mls_num                  
    column :listing_class            
    column :listing_class_type       
    column :status                   
    column :for_sale                 
    column :lat                      
    column :lon                      
    column :address                  
    column :zip                      
    column :location_rating          
    column :condition_rating         
    column :acres                    
    column :price                    
    column :list_on_market           
    column :last_sold_price          
    column :year_built               
    column :recorded_mortgage        
    column :assesed_value            
    column :current_taxes            
    column :monthly_total_fees       
    column :created_at              
    actions
  end

end
