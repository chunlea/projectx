ActiveAdmin.register Product do
  permit_params :product_id, :extend_months

  index do
    selectable_column
    id_column
    column :product_id
    column :extend_months
    column :created_at
    actions
  end

  filter :product_id
  filter :extend_months
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :product_id, label: "Product ID"
      f.input :extend_months
    end
    f.actions
  end


end
