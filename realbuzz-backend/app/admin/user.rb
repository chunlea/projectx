ActiveAdmin.register User do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  
  index do
    selectable_column
    id_column
    column :email                 
    column :sign_in_count         
    column :current_sign_in_at    
    column :last_sign_in_at       
    column :current_sign_in_ip    
    column :last_sign_in_ip       
    column :name                  
    column :zip_code              
    column :role                  
    column :facebook_id           
    column :created_at            
    column :updated_at            
    column :estimates_count       
    column :accurate              
    actions
  end 


end
