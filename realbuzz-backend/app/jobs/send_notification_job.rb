class SendNotificationJob
  class << self
    def perform(user_id, message, badge = nil, custom = {})
      data          = { :alert => message }
      data[:badge]  = badge.to_i if badge
      data[:custom] = custom
      push          = Parse::Push.new(data)
      if user_id.kind_of?(Array)
        # Send mulit push notification with only one request, save time.
        if !user_id.empty?
          query = Parse::Query.new(Parse::Protocol::CLASS_INSTALLATION).value_in('user_id', user_id)
          push.where = query.where
          push.save
        end
      else
        # Send push notification for specail user.
        query = Parse::Query.new(Parse::Protocol::CLASS_INSTALLATION).eq('user_id', user_id)
        push.where = query.where
        push.save
      end
    end
  end
end
